<?php
$lang['Welcome to CodeIgniter!'] = "Добро пожаловать в CodeIgniter!";
$lang['Enter you password...'] = "Введите ваш пароль...";
$lang['Username'] = "Логин";
$lang['Password'] = "Пароль";
$lang['Remember me'] = "Запомнить меня";
$lang['Login'] = "Войти";
$lang['Send'] = "Отправить";
$lang['Forgot Password?'] = "Забыли пароль?";
$lang["Please send us your email and we'll reset your password."] = "Пожалуйста укажите ваш email и мы отправим вам ссылку для сброса пароля.";
$lang['Email address:'] = "Ваш Email:";
$lang['Back to login'] = "Вернуться";

?>