<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "main";
$route['404_override'] = '';

// User profile routes
$route['profile/article/add'] = 'profile/articles_add';
$route['profile/article/edit/(:num)'] = 'profile/articles_edit/$1';

$route['profile/firm/add'] = 'profile/firms_add';
$route['profile/firm/request/(:num)'] = 'profile/request_firm/$1';
$route['profile/adverts/add'] = 'profile/adverts_add';
$route['adverts/add'] = 'profile/adverts_add';
$route['profile/adverts/request/(:num)'] = 'profile/request_adverts/$1';
$route['profile/article/request/(:num)'] = 'profile/request_articles/$1';
$route['firm/add'] = 'profile/firms_add';

// User profile routes
$route['profile/wedding/add'] = 'profile/wedding_add';

// FeedBck routes
$route['feedback'] = 'show_feedback/index';

// Articles routes
$route['article/(:any)'] = 'article/index/$1';

// Articles groups routes
$route['group/(:any)'] = 'article_group/index/$1';

// Firms routes
$route['firm/(:any)'] = 'firm/index/$1';

// Firms cats routes
$route['cat/(:any)'] = 'firms_cats/index/$1';
$route['cat'] = 'firms_cats/all';

// Adverts routes
$route['adverts'] = 'adverts_show/index';
$route['adverts/(:any)/(:any)'] = 'adverts_show/advert/$1/$2';
$route['adverts/(:any)'] = 'adverts_show/cat/$1';

// Admin routes
$route['login'] = 'auth_controller/login';
$route['logout'] = 'auth_controller/logout';
$route['admin'] = 'admin/main';
// Admin firms
$route['admin/firms'] = 'admin/a_firms';
$route['admin/firms/add'] = 'admin/a_firms/add';
$route['admin/firms/edit/(:num)'] = 'admin/a_firms/edit/$1';
$route['admin/firms/delete/(:num)'] = 'admin/a_firms/delete/$1';
$route['admin/firms/cat_add'] = 'admin/a_firms/cat_add';
$route['admin/firms/cat_edit/(:num)'] = 'admin/a_firms/cat_edit/$1';
$route['admin/firms/cat_delete/(:num)'] = 'admin/a_firms/cat_delete/$1';
$route['admin/firms/cat'] = 'admin/a_firms/cat';
$route['admin/firms/cat/(:any)'] = 'admin/a_firms/cat/$1';
$route['admin/firms/image_add/(:num)'] = 'admin/a_firms/image_add/$1';
$route['admin/firms/image_del'] = 'admin/a_firms/image_del';

// Banners redirect system
$route['bgo/(:any)'] = 'bgo/index/$1';

// Admin adverts
$route['admin/adverts'] = 'admin/a_adverts';
$route['admin/socbuttons'] = 'admin/a_social';
$route['admin/adverts/add'] = 'admin/a_adverts/add';
$route['admin/adverts/edit/(:num)'] = 'admin/a_adverts/edit/$1';
$route['admin/adverts/delete/(:num)'] = 'admin/a_adverts/delete/$1';
$route['admin/adverts/cat_add'] = 'admin/a_adverts/cat_add';
$route['admin/adverts/cat_edit/(:num)'] = 'admin/a_adverts/cat_edit/$1';
$route['admin/adverts/cat_delete/(:num)'] = 'admin/a_adverts/cat_delete/$1';
$route['admin/adverts/cat'] = 'admin/a_adverts/cat';
$route['admin/adverts/cat/(:any)'] = 'admin/a_adverts/cat/$1';
$route['admin/adverts/types_add'] = 'admin/a_adverts/types_add';
$route['admin/adverts/types_edit/(:num)'] = 'admin/a_adverts/types_edit/$1';
$route['admin/adverts/types_delete/(:num)'] = 'admin/a_adverts/types_delete/$1';
$route['admin/adverts/types'] = 'admin/a_adverts/types';
$route['admin/adverts/types/(:any)'] = 'admin/a_adverts/types/$1';

// Admin gallery
$route['admin/gallery'] = 'admin/a_gallery';
$route['admin/gallery/add'] = 'admin/a_gallery/add';
$route['admin/gallery/edit/(:num)'] = 'admin/a_gallery/edit/$1';
$route['admin/gallery/delete/(:num)'] = 'admin/a_gallery/delete/$1';
$route['admin/gallery/photoUpload/(:num)'] = 'admin/a_gallery/photoUpload/$1';
$route['admin/gallery/photothumbsgen/(:num)'] = 'admin/a_gallery/photothumbsgen/$1';

// Admin news
$route['admin/news'] = 'admin/a_news';
$route['admin/news/add'] = 'admin/a_news/add';
$route['admin/news/edit/(:num)'] = 'admin/a_news/edit/$1';
$route['admin/news/delete/(:num)'] = 'admin/a_news/delete/$1';


// Admin articles
$route['admin/articles/(:num)'] = 'admin/a_articles/index/$1';
$route['admin/articles'] = 'admin/a_articles';
$route['admin/articles/add'] = 'admin/a_articles/add';
$route['admin/articles/edit/(:num)'] = 'admin/a_articles/edit/$1';
$route['admin/articles/show/(:num)'] = 'admin/a_articles/show/$1';
$route['admin/articles/hide/(:num)'] = 'admin/a_articles/hide/$1';
$route['admin/articles/delete/(:num)'] = 'admin/a_articles/delete/$1';
$route['admin/ajax_ruslat'] = 'admin/ajax_ruslat';
$route['admin/templates'] = 'admin/a_templates';

// Admin articles groups
$route['admin/articles/groups'] = 'admin/a_articles/groups';
$route['admin/articles/groups_add'] = 'admin/a_articles/groups_add';
$route['admin/articles/groups_edit/(:num)'] = 'admin/a_articles/groups_edit/$1';
$route['admin/articles/groups_delete/(:num)'] = 'admin/a_articles/groups_delete/$1';

// Admin banners
$route['admin/banners'] = 'admin/a_banners';
$route['admin/banners/add'] = 'admin/a_banners/add';
$route['admin/banners/edit/(:num)'] = 'admin/a_banners/edit/$1';
$route['admin/banners/delete/(:num)'] = 'admin/a_banners/delete/$1';
$route['admin/banners/image_add/(:num)'] = 'admin/a_banners/image_add/$1';
$route['admin/banners/image_del'] = 'admin/a_banners/image_del';



// Admin users
$route['admin/users'] = 'admin/a_users';
$route['admin/users/add'] = 'admin/a_users/add';
$route['admin/users/edit/(:num)'] = 'admin/a_users/edit/$1';
$route['admin/users/delete/(:num)'] = 'admin/a_users/delete/$1';
$route['admin/users/user/(:num)'] = 'admin/a_users/user/$1';

// Admin options
$route['admin/options'] = 'admin/a_options/log';
$route['admin/options/log'] = 'admin/a_options/log';
$route['admin/options/log/delete/(:num)'] = 'admin/a_options/log_del/$1';



/* End of file routes.php */
/* Location: ./application/config/routes.php */