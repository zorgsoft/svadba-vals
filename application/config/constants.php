<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
    HardKor CMS constants, you can change it for you needs
*/

define('HC_SYS_RES_PATH', 'resources/sys/');
define('HC_RES_PATH', 'resources/');

// Name of access rights for global super admin
define('HC_SUPER_ADMIN_NAME', 'superadmin');
// Default registred user role ID for new users (check in you base)
define('HC_DEF_USER_ROLE_ID', 2);
// Default registred user rule name for add when register new user
define('HC_DEF_USER_RULE_NAME', 'registred');

// Auth errors
define('E_USER_REG_EXISTS', 'Пользователь с таким именем или email уже существует.');
define('E_USER_REG_DONE', TRUE);
define('E_USER_REG_ERROR', 'Ошибка при регистрации пользователя. Обратитесь к администратору.');


// Users roles constants and names for access roles
// Прользовательские константы и их имена для прав доступа
define('R_USER_ADMIN', 'admin');                // Основное правило для администратора
define('R_USER_ARTICLES', R_USER_ADMIN);        // Для дуступа к статьям, только просмотр и список
define('R_USER_ARTICLES_ADD', R_USER_ADMIN);    // Для добавления статей
define('R_USER_ARTICLES_EDIT', R_USER_ADMIN);   // Для редактирования статей
define('R_USER_ARTICLES_DELETE', R_USER_ADMIN); // Для удаления статей
define('R_USER_ARTICLES_GROUPS', R_USER_ADMIN); // Для дуступа к групам статей, только просмотр и список
define('R_USER_ARTICLES_GROUPS_ADD', R_USER_ADMIN);    // Для груп статей
define('R_USER_ARTICLES_GROUPS_EDIT', R_USER_ADMIN);   // Для редактирования груп
define('R_USER_ARTICLES_GROUPS_DELETE', R_USER_ADMIN); // Для удаления груп
define('R_USER_TEMPLATES', R_USER_ADMIN);    // Для просмотра шаблонов
define('R_USER_TEMPLATES_ADD', R_USER_ADMIN);    // Для добавления шаблонов
define('R_USER_TEMPLATES_EDIT', R_USER_ADMIN);    // Для редактирования шаблонов
define('R_USER_TEMPLATES_DELETE', R_USER_ADMIN);    // Для удаления шаблонов
define('R_USER_FIRMS_GROUPS', R_USER_ADMIN);    // Для работы с разделами фирм
define('R_USER_FIRMS_GROUPS_ADD', R_USER_ADMIN);    // Для работы с разделами фирм
define('R_USER_FIRMS_GROUPS_EDIT', R_USER_ADMIN);    // Для работы с разделами фирм
define('R_USER_FIRMS_GROUPS_DELETE', R_USER_ADMIN);    // Для работы с разделами фирм
define('R_USER_FIRMS', R_USER_ADMIN);    // Для работы с фирмами
define('R_USER_FIRMS_ADD', R_USER_ADMIN);    // Для работы с фирмами
define('R_USER_FIRMS_EDIT', R_USER_ADMIN);    // Для работы с фирмами
define('R_USER_FIRMS_DELETE', R_USER_ADMIN);    // Для работы с фирмами
define('R_USER_ADVERTS_GROUPS', R_USER_ADMIN);    // Для работы с разделами объявлений
define('R_USER_ADVERTS_GROUPS_ADD', R_USER_ADMIN);    // Для работы с разделами объявлений
define('R_USER_ADVERTS_GROUPS_EDIT', R_USER_ADMIN);    // Для работы с разделами объявлений
define('R_USER_ADVERTS_GROUPS_DELETE', R_USER_ADMIN);    // Для работы с разделами объявлений
define('R_USER_ADVERTS_TYPES', R_USER_ADMIN);    // Для работы с типами объявлений
define('R_USER_ADVERTS_TYPES_ADD', R_USER_ADMIN);    // Для работы с типами объявлений
define('R_USER_ADVERTS_TYPES_EDIT', R_USER_ADMIN);    // Для работы с типами объявлений
define('R_USER_ADVERTS_TYPES_DELETE', R_USER_ADMIN);    // Для работы с типами объявлений
define('R_USER_ADVERTS', R_USER_ADMIN);    // Для работы с объявлениями
define('R_USER_ADVERTS_ADD', R_USER_ADMIN);    // Для работы с объявлениями
define('R_USER_ADVERTS_EDIT', R_USER_ADMIN);    // Для работы с объявлениями
define('R_USER_ADVERTS_DELETE', R_USER_ADMIN);    // Для работы с объявлениями

define('R_USER_NEWS', R_USER_ADMIN);    // Для работы с новостями
define('R_USER_NEWS_ADD', R_USER_ADMIN);    // Для работы с новостями
define('R_USER_NEWS_EDIT', R_USER_ADMIN);    // Для работы с новостями
define('R_USER_NEWS_DELETE', R_USER_ADMIN);    // Для работы с новостями

define('R_USER_BANNERS', R_USER_ADMIN);    // Для работы с банерами
define('R_USER_BANNERS_ADD', R_USER_ADMIN);    // Для работы с банерами
define('R_USER_BANNERS_EDIT', R_USER_ADMIN);    // Для работы с банерами
define('R_USER_BANNERS_DELETE', R_USER_ADMIN);    // Для работы с банерами


/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */