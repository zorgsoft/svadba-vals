<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function adminMenu($menu_title, $menu_url, $menu_access_rule, $menu_selected_name, $sub_menu_selected_name, $menu_items = array()){
    // Generate admin menu with selected rights
    $menu_links = '';
    if(Auth::canAccess($menu_access_rule) == TRUE){
        $this_menu_class = 'select';
        $this_menu_sub_class = 'select_sub';
        if($menu_selected_name == $menu_title){
            $this_menu_class = 'current';
            $this_menu_sub_class = 'select_sub show';
        }
        $menu_links = '<ul class="'.$this_menu_class.'"><li><a href="'.$menu_url.'"><b>'.$menu_title.'</b><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class="'.$this_menu_sub_class.'">
			<ul class="sub">';
        foreach($menu_items as $menu_sub_item){
            $sub_menu_array = explode('|', $menu_sub_item);
            if(Auth::canAccess($sub_menu_array[2]) == TRUE){
                $this_sub_class = '';
                if($sub_menu_array[0] == $sub_menu_selected_name)
                    $this_sub_class = 'class="sub_show"';
                $menu_links .= '<li '.$this_sub_class.'><a href="'.$sub_menu_array[1].'">'.$sub_menu_array[0].'</a></li>';
            }
        }
	$menu_links .= '		</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		</ul>';
    }
    return $menu_links;
}

?>