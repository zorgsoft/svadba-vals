<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Actions {
    public function Add($userId, $actionName = 'Без названия', $actionUrl = 'Нет ссылки'){
        $newAction = new Actions_list();
        $newAction->name = $actionName;
        $newAction->url = $actionUrl;
        $newAction->user_id = $userId;
        $newAction->save();
    }
}
?>