<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Capctcha helper librarie
*/
class Libcap
{
	public function createCap($imgWidth = '150', $imgHeight = '30'){
		$capConfig = array(
			'img_path'	 => 'resources/captcha/',
    		'img_url'	 => base_url(). 'resources/captcha/',
    		'img_width'	 => $imgWidth,
    		'img_height' => $imgHeight,
    		'expiration' => 600
			);
		$cap = create_captcha($capConfig);
		$this->session->set_userdata('cap_code', $cap['word']);
		$this->session->set_userdata('cap_image', $cap['image']);
		return $cap['image'];
	}

	public function removeCap(){
		$this->session->unset_userdata('cap_code');
		$this->session->unset_userdata('cap_image');
	}
}
?>