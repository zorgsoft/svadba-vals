<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Editor {
    public function CKEditor($textAreaName, $width = '100%', $height = 300, $uiColor = 'rgb(206, 206, 206)', $skinName = 'kama') {
        $returnScriptText = '
        <script type="text/javascript">
        var ckeditor = CKEDITOR.replace(\''.$textAreaName.'\', {
            width: \''.$width.'\',
            height: \''.$height.'\',
            uiColor : \''.$uiColor.'\',
            baseHref: \''.base_url().'\',
            defaultLanguage: \'ru\',
            emailProtection: \'encode\',
            skin: \''.$skinName.'\'
        });

        AjexFileManager.init({
        	returnTo: \'ckeditor\',
        	editor: ckeditor,
        	skin: \'light\'
    });
    </script>';
        return $returnScriptText;
    }
}
?>