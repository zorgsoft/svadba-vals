<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Weddings Lib
 */
class libWeddings {
    public function showCalendar ($forMonth) {
        $weddingsList = Doctrine_Query::create()
                ->select('*')
                ->from('weddings')
                ->where('MONTH(wedding_date) = ?', $forMonth)
                ->andWhere('visible = ?', 1)
                ->execute();
        
        $datesArray =  array();
        $dayInMonth = days_in_month($forMonth);
        if($forMonth == 1){
            $dayInPrevMonth = days_in_month(12);
        } else {
            $dayInPrevMonth = days_in_month($forMonth - 1);
        }
        
        $firstDayOfWeek = date('N', strtotime(date('Y').'-'.$forMonth.'-01'));
        //$lastDayOfWeek = date('N', strtotime(date('Y').'-'.$forMonth.'-01'));
            
        for($i = 1; $i < ($firstDayOfWeek) ; $i++){
            $theDate = (($dayInPrevMonth - $firstDayOfWeek) + $i +1);
            $datesArray[] = array('itemUrl' => '#', 'itemText' => $theDate, 'itemClass' => 'calendar_lm');
        }
        for($i = 1; $i <= $dayInMonth; $i++){
            $url = '#';
            foreach($weddingsList as $weddingItem){
                if(date('d', strtotime($weddingItem->wedding_date)) == $i){
                    $url = base_url().'wedding/'.$weddingItem->url;
                }
            }
            if(date('d') == $i and date('m') == $forMonth){
                if($url != '#'){
                    $newClass = 'calendar_ot';
                } else {
                    $newClass = 'calendar_v';
                }
                $datesArray[] = array('itemUrl' => $url, 'itemText' => $i, 'itemClass' => $newClass);
            } else {
                if($url != '#'){
                    $newClass = 'calendar_ot';
                } else {
                    $newClass = 'calendar';
                }
                $datesArray[] = array('itemUrl' => $url, 'itemText' => $i, 'itemClass' => $newClass);
            }
        }
        for($i = 1;$i <= (42 - $dayInMonth - $firstDayOfWeek +1); $i++){
            $datesArray[] = array('itemUrl' => '#', 'itemText' => $i, 'itemClass' => 'calendar_lm');
        }

        $data['datesArray'] = $datesArray;
        $return_data = $this->parser->parse('tpl/tpl_calendar', $data, TRUE);
        return $return_data;
    }
}
?>
