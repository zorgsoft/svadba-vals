<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu{
public function adminMenu($menu_title, $menu_url, $menu_access_rule, $menu_selected_name, $sub_menu_selected_name, $menu_items = array()){
    // Generate admin menu with selected rights
    $menu_links = '';
    if(Auth::canAccess($menu_access_rule) == TRUE){
        if($menu_selected_name == $menu_title){
            $this_menu_class = 'active';
            $this_menu_sub_class = 'active';
        }
        if(count($menu_items) > 0){
	    $menu_links = '<ul class="nav"><li class="dropdown"><a class="dropdown-toggle" href="'.$menu_url.'">'.$menu_title.'</a>';
	} else {
	    $menu_links = '<ul class="nav"><li><a href="'.$menu_url.'">'.$menu_title.'</a>';
	}

	if(count($menu_items) > 0)
	    $menu_links .= '<ul class="dropdown-menu">';	
        foreach($menu_items as $menu_sub_item){
            $sub_menu_array = explode('|', $menu_sub_item);
            if(Auth::canAccess($sub_menu_array[2]) == TRUE){
                $this_sub_class = '';
                $menu_links .= '<li><a href="'.$sub_menu_array[1].'">'.$sub_menu_array[0].'</a></li>';
            }
	    
        }
	if(count($menu_items) > 0)
	    $menu_links .= '</ul>';
	$menu_links .= '</li></ul>';
    }
    return $menu_links;
}

public function visibleImage($visibleNum = 0) {
    // show image visible or not
    if($visibleNum == 0){
        return '<img style="vertical-align:middle;" title="Не видно/Не одобренно" src="'.base_url().'resources/sys/images/16x16/future-projects.png">';
    } else {
        return '<img style="vertical-align:middle;" title="Видно/Одобренно" src="'.base_url().'resources/sys/images/16x16/finished-work.png">';
    }
}
}

?>