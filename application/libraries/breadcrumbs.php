<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Breadcrumbs {
    public function Article($articleId, $showHome = FALSE, $templateFile = "tpl/crumbs_article") {
        // Create breadcrumbs for Article
        $articleData = Doctrine::getTable('articles')->findOneBy('id', $articleId);
        if($articleData != NULL) {
            $crumbsArray = array();
            if($articleData->group_id > 0) {
                $crumbsArray[] = array('url' => 'group/'.$articleData->Group->url, 'name' => $articleData->Group->name);
            }
            
            if($articleData->parent_id > 0) {
                $crumbsArray[] = array('url' => 'article/'.$articleData->Parent->url, 'name' => $articleData->Parent->name);
                $parenArticle = $articleData->Parent;
                while($parenArticle->parent_id > 0){
                    $crumbsArray[] = array('url' => 'article/'.$parenArticle->url, 'name' => $parenArticle->name);
                    $parenArticle = $parenArticle->Parent;
                }
            }
            if($articleData->group_id > 0 or $articleData->parent_id > 0) {
                $data =  array();
                $data['crumbsList'] = $crumbsArray;
                $data['showHome'] = $showHome;
                $return_data = $this->parser->parse($templateFile, $data, TRUE);
                return $return_data;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
    
    public function Cat($catId, $showHome = TRUE, $templateFile = "tpl/crumbs_cat") {
        // Create breadcrumbs for Cat
        $catData = Doctrine::getTable('firms_cat')->findOneBy('id', $catId);
        if($catData != NULL and $catData->parent_id > 0) {
            $crumbsArray = array();
            $catParent = $catData;
            while($catParent->parent_id > 0){
                $crumbsArray[] = array('url' => 'cat/'.$catParent->Parent->url, 'name' => $catParent->Parent->name);
                $catParent = $catParent->Parent;
            }
            
            $data =  array();
            $data['crumbsList'] = $crumbsArray;
            $data['showHome'] = $showHome;
            $return_data = $this->parser->parse($templateFile, $data, TRUE);
            return $return_data;
    
        } else {
            return '<a class="kroshki_link" href="'.base_url().'">Главная</a> \ <a class="kroshki_link" href="'.base_url().'cat">Каталог фирм</a>';
        }
    }
    
    public function Firm($firmId, $showHome = TRUE, $templateFile = "tpl/crumbs_firm") {
        // Crate breadcrumbs for Firm
        $firmData = Doctrine::getTable('firms')->findOneBy('id', $firmId);
        if($firmData != NULL){
            $data =  array();
            $data['crumbsCatParent'] = $firmData->Firms_cats[0]->Firms_cat;
            $data['crumbsCatTop'] = $firmData->Firms_cats[0]->Firms_cat->Parent;
            $data['showHome'] = $showHome;
            $return_data = $this->parser->parse($templateFile, $data, TRUE);
            return $return_data;
        } else {
            return '';
        }
    }
    
    public function AdvertsCat($catId, $showHome = TRUE, $templateFile = "tpl/crumbs_adverts_cat") {
        // Show breadcrumbs for adverts cat
        $catData = Doctrine::getTable('adverts_groups')->findOneBy('id', $catId);
        if($catData != NULL) {
            $crumbsArray = array();
            $catParent = $catData;
            while($catParent->parent_id > 0){
                $crumbsArray[] = array('url' => 'adverts/'.$catParent->Parent->url, 'name' => $catParent->Parent->name);
                $catParent = $catParent->Parent;
            }
            
            $data =  array();
            $data['crumbsList'] = $crumbsArray;
            $data['showHome'] = $showHome;
            $return_data = $this->parser->parse($templateFile, $data, TRUE);
            return $return_data;
        } else {
            return '';
        }
    }
    
    public function Adverts($advertId, $showHome = TRUE, $templateFile = "tpl/crumbs_adverts_cat") {
        // Show adverts breadcrumbs
        $advertsData = Doctrine::getTable('adverts')->findOneBy('id', $advertId);
        if($advertsData != NULL){
            $catData = Doctrine::getTable('adverts_groups')->findOneBy('id', $advertsData->group_id);
            $crumbsArray = array();
            $catParent = $catData;
            while($catParent->parent_id > 0){
                $crumbsArray[] = array('url' => 'adverts/'.$catParent->Parent->url, 'name' => $catParent->Parent->name);
                $catParent = $catParent->Parent;
            }
            $crumbsArray[] = array('url' => 'adverts/'.$catData->url, 'name' => $catData->name);
            $data =  array();
            $data['crumbsList'] = $crumbsArray;
            $data['showHome'] = $showHome;
            $return_data = $this->parser->parse($templateFile, $data, TRUE);
            return $return_data;
        } else {
            return '';
        }
        
    }
}
?>