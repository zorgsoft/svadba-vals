<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth {
    
    public function login($login, $password){
        // Login user
        $login_user = Doctrine::getTable('users')->findOneBy('login', $login);
        if($login_user != NULL and $login_user->password == md5($password) ){
            $this->session->set_userdata('isAuthorized', TRUE);
            $this->session->set_userdata($login_user);
            return TRUE;
        } else {
            $this->session->set_userdata('isAuthorized', FALSE);
            return FALSE;
        }
    }
    
    public function isAuthorized(){
        return $this->session->userdata('isAuthorized');
    }
    
    public function logout($redirect_url = NULL){
        $this->session->sess_destroy();
        if($redirect_url!=NULL)
            redirect($redirect_url);
    }
    
    public function user(){
        // Return user object
        if(self::isAuthorized()){
            $obj_user = (object) array(
                                       'name' => $this->session->userdata('name'),
                                       'login' => $this->session->userdata('login'),
                                       'email' => $this->session->userdata('email'),
                                       'phone' => $this->session->userdata('phone'),
                                       'comment' => $this->session->userdata('comment'),
                                       'created' => $this->session->userdata('created_at'),
                                       'updated' => $this->session->userdata('updated_at')
                                       );
            return $obj_user;
        } else {
            return NULL;
        }
    }
    
    public function getUserId() {
       if(Auth::isAuthorized()) 
           return $this->session->userdata('id');
       else
          return 0;
        
    }


    public function canAccess($rule_name){
        // Return TRUE or FALSE for loggedin user by rule name
        
        $canAccess = FALSE;
        if(self::isAuthorized()){
            $user_data = Doctrine::getTable('users')->findOneBy('id', $this->session->userdata('id'));
            $rule_data = Doctrine::getTable('access_rights')->findOneBy('name', $rule_name);
            if($user_data != NULL and $rule_data != NULL and $user_data->roles_id>0){
                $roles_rights_q = Doctrine_Query::create()
                    ->select('*')
                    ->from('roles_rights')
                    ->where('roles_id = ?', $user_data->roles_id)
                    ->andWhere('access_rights_id = ?', $rule_data->id)
                    ->limit(1);
                $roles_rights = $roles_rights_q->execute();
                if ($roles_rights->count()>0)
                    $canAccess = TRUE;
                
            }
        }
        
        if(self::isSuperAdmin())
            $canAccess = TRUE;
       
        return $canAccess;
    }
    
    public function isSuperAdmin(){
        // Return TRUE if userhas super admin right
        // HC_SUPER_ADMIN_NAME
        $isSuperAdmin = FALSE;
        if(self::isAuthorized()){
            $user_data = Doctrine::getTable('users')->findOneBy('id', $this->session->userdata('id'));
            $rule_data = Doctrine::getTable('access_rights')->findOneBy('name', HC_SUPER_ADMIN_NAME);            
            if($user_data != NULL and $rule_data != NULL and $user_data->roles_id>0){
                $roles_rights_q = Doctrine_Query::create()
                    ->select('*')
                    ->from('roles_rights')
                    ->where('roles_id = ?', $user_data->roles_id)
                    ->andWhere('access_rights_id = ?', $rule_data->id)
                    ->limit(1);
                $roles_rights = $roles_rights_q->execute();
                if ($roles_rights->count()>0)
                    $isSuperAdmin = TRUE;
            }
        }
        return $isSuperAdmin;
    }
    
    public function register($reg_user_data = object){
        // Function for register new users, get data from object and return true is done
        // or false if error
        $user_data_q = Doctrine_Query::create()
            ->select('*')
            ->from('users')
            ->where('login = ?', $reg_user_data->login)
            ->orWhere('email = ?', $reg_user_data->email)
            ->limit(1);
        $user_data = $user_data_q->execute();
        if($user_data->count()>0){
            return E_USER_REG_EXISTS;
        } else {
            
            if(($def_user_rule = Doctrine::getTable('access_rights')->findOneBy('name', HC_DEF_USER_RULE_NAME)) == NULL){
                $def_user_rule = new Access_rights();
                $def_user_rule->name = HC_DEF_USER_RULE_NAME;
                $def_user_rule->description = 'Обычный зарегистрированный пользователь с минимальными правами';
                $def_user_rule->save();
                
                $user_role_data = Dctrine::getTable('access_rights')->findOneBy('id', HC_DEF_USER_ROLE_ID);
                if($user_role_data != null){
                    $roles_rights_data = new Rolse_rights();
                    $roles_rights_data->roles_id = HC_DEF_USER_ROLE_ID;
                    $roles_rights_data->access_rights_id = $def_user_rule->id;
                    $roles_rights_data->save();
                }
            }
            $user_data = new Users();
            $user_data->login = $reg_user_data->login;
            $user_data->email = $reg_user_data->email;
            $user_data->password = $reg_user_data->password;
            $user_data->name = $reg_user_data->name;
            $user_data->phone = $reg_user_data->phone;
            $user_data->comment = $reg_user_data->comment;
            // TODO:: Change roles_id to get ID from configuraton, from base
            $user_data->roles_id = HC_DEF_USER_ROLE_ID;
            
            $user_data->save();
            return E_USER_REG_DONE;
        }
    }
}

?>