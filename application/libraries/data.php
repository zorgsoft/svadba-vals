<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Data {
    public function getCatListForMenu($template_file = 'tpl/tpl_cat_menu_list'){
        // Create and return firms cats list
        $catList = Doctrine_Query::create()
            ->select('*')
            ->from('firms_cat')
            ->where('visible =?', 1)
            ->andWhere('parent_id =?', 0)
            ->orderBy('zindex ASC')
            ->execute();
        
        if($catList != NULL and $catList->count() >0){
            $data =  array();
            $data['catList'] = $catList;
            $return_data = $this->parser->parse($template_file, $data, TRUE);
            return $return_data;
        } else {
            return 'Разделов нет.';
        }
    }
    
    public function getSubListForCat($cat_id){
        // Return subcats for selected cat id
        $catList = Doctrine_Query::create()
            ->select('*')
            ->from('firms_cat')
            ->where('visible =?', 1)
            ->andWhere('parent_id =?', $cat_id)
            ->orderBy('zindex ASC')
            ->execute();
        
        if($catList != NULL and $catList->count() >0){
            return $catList;
        } else {
            return NULL;
        }
    }
    
    public function getNewFirmsForMenu($template_file = 'tpl/tpl_firms_new_menu_list'){
        // Create and retuen new firms list for menu
        $firmsList = Doctrine_Query::create()
            ->select('*')
            ->from('firms')
            ->where('visible =?', 1)
            ->orderBy('paid DESC, updated_at DESC, zindex ASC')
            ->execute();
            
        if($firmsList != NULL and $firmsList->count() >0){
            $data =  array();
            $data['newFirms'] = $firmsList;
            $return_data = $this->parser->parse($template_file, $data, TRUE);
            return $return_data;
        } else {
            return 'Новых фирм нет, добавте!';
        }
    }
    
    public function getArticlesForGroup($group_url, $articles_count = 4, $template_file = 'tpl/tpl_article_list'){
        // Create and return articles list (only visible), can set count and template
        $groupData = Doctrine::getTable('articles_groups')->findOneBy('url', $group_url);
        if($groupData != NULL){
            $articlesList = Doctrine_Query::create()
                ->select('*')
                ->from('articles')
                ->where('visible =?', 1)
                ->andWhere('group_id =?', $groupData->id)
                ->limit($articles_count)
                ->orderBy('zindex ASC')
                ->execute();
                
            if($articlesList != NULL and $articlesList->count() >0){
                $data =  array();
                $data['articlesList'] = $articlesList;
                $return_data = $this->parser->parse($template_file, $data, TRUE);
                return $return_data;
            } else {
                return 'Нет статей';
            }
        } else {
            return 'Неверно указанная група сатей.';
        }
    }
    
    public function getCatTopParent($catId){
        // Return parent cat object
        $catData =  Doctrine::getTable('firms_cat')->findOneBy('id', $catId);
        if($catData != NULL and $catData->parent_id > 0) {
            return $catData->Parent;
        } else {
            return NULL;
        }
    }
    
    
    public function bannerShowById($bannerId, $bannerTemplate = 'banners/tpl_banners_top'){
        // show banner by id
        if($bannerId == NULL)
            $bannerId = 0;
        $bannerData = Doctrine::getTable('banners')->findOneBy('id', $bannerId);
        if($bannerData != NULL){
            $data =  array();
            $data['banner_image'] = $bannerData->file_name;
            $data['banner_short_url'] = base_url().'bgo/'.$bannerData->url;
            $data['banner_url'] = $bannerData->site_url;
            if(trim($data['banner_short_url']) == '')
                $data['banner_short_url'] = $data['banner_url'];
            $data['banner_title'] = htmlspecialchars ($bannerData->title);
            $data['banner_name'] = htmlspecialchars ($bannerData->name);
            if(trim($data['banner_title']) == '')
                $data['banner_title'] = $data['banner_name'];
            $return_data = $this->parser->parse($bannerTemplate, $data, TRUE);
            return $return_data;
        } else {
            return '';
        }
    }
    
    public function bannerShowOurRandom($bannerTemplate = 'banners/tpl_banners_left'){
        // Show our random banner
        $bannersList = Doctrine_Query::create()
            ->select('*')
            ->from('banners')
            ->where('visible =?', 1)
            ->andWhere('paid_type =?', 0)
            ->execute();
        if($bannersList->count() > 0){
            $rnd_seed = mt_rand(0, $bannersList->count()-1);
            return Data::bannerShowById($bannersList[$rnd_seed]->id, $bannerTemplate);
        } else {
            return '';
        }
    }
    
    public function bannersRandomShow($bannersMax = 1, $ourOnly = FALSE, $paidOnly = FALSE, $bannerTemplate = 'banners/tpl_banners_top_random') {
        
        $todayDatePlusOne = date("Y-m-d", strtotime ("+1 day"));
        
        $bannersList = Doctrine_Query::create()
            ->select('*')
            ->from('banners')
            ->where('visible =?', 1);
            
        if($ourOnly == TRUE AND $paidOnly == FALSE)
            $bannersList->andWhere('paid_type =?', 0);
        if($paidOnly == TRUE AND $ourOnly == FALSE){
            $bannersList->andWhere('paid_to >= ?', $todayDatePlusOne);
            $bannersList->andWhere('paid =?', 1);
        }
        $bannersList = $bannersList->execute();
        
        if($bannersList->count() > 0){
            $tmpItem = '';
            foreach($bannersList as $bannerAllItem){
                $bannerArrayItem =  array();
                $bannerArrayItem['banner_image'] = $bannerAllItem->file_name;
                $bannerArrayItem['banner_short_url'] = base_url().'bgo/'.$bannerAllItem->url;
                $bannerArrayItem['banner_url'] = $bannerAllItem->site_url;
                if(trim($bannerArrayItem['banner_short_url']) == '')
                    $bannerArrayItem['banner_short_url'] = $bannerArrayItem['banner_url'];
                $bannerArrayItem['banner_title'] = htmlspecialchars($bannerAllItem->title);
                $bannerArrayItem['banner_name'] = htmlspecialchars($bannerAllItem->name);
                if(trim($bannerArrayItem['banner_title']) == '')
                    $bannerArrayItem['banner_title'] = $bannerArrayItem['banner_name'];
                
                $allBannersList[] = $bannerArrayItem;
                unset($bannerArrayItem);
            }
            
            
            if($bannersMax > count($allBannersList))
                $bannersMax = count($allBannersList);
            srand();
            shuffle($allBannersList);
            $randomKeys = array_rand($allBannersList, $bannersMax);
            
            $randomBannersList = array();
            
            if(count($randomKeys) > 1 ){
                foreach($randomKeys as $randomKeyId){
                  $randomBannersList[] = $allBannersList[$randomKeyId];
                }
            } else {
                $randomBannersList = $allBannersList;
            }
            
            
            $data =  array();
            $data['bannersList'] = $randomBannersList;
            $return_data = $this->parser->parse($bannerTemplate, $data, TRUE);
            return $return_data;
        } else {
            return '';
        }
    }

}
?>