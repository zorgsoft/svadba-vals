<?php


class libGallery {
   
   
    
    
    
    public function save($p, $gal = null) {
      if ($gal)
          $gallery = $gal;
      else
        $gallery = new Gallery();
        
      
      $gallery->name = $p->post('name');
      $gallery->meta_title = $p->post('meta_title');
      $gallery->meta_description = $p->post('meta_description');
      $gallery->meta_keywords = $p->post('meta_keywords');
      $gallery->description = $p->post('description');
      $gallery->content = $p->post('content');
      $gallery->url = $p->post('url');
      $gallery->zindex = $p->post('zindex');
      $gallery->visible = str_replace('on', '1', $p->post('visible'));
      $gallery->user_id =  Auth::getUserId();
        
      $gallery->save();
      return $gallery;
    }
    
}

?>
