<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>{meta_title}</title>
<meta name=Keywords content="{meta_keywords}">
<meta name=Description content="{meta_description}">
<meta http-equiv="Content-Type" content="text_object/html; charset=utf-8">
<link rel=stylesheet type="text/css" href="<?=base_url()?>resources/inc/style.css">
<script type="text/javascript" src="<?=base_url()?>resources/inc/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>resources/inc/select.js"></script>
<script language="JavaScript" type="Text/Javascript" src="<?=base_url()?>resources/inc/lib.js" ></script>
<script type="text/javascript">$(document).ready(function(){$('.selectBlock').sSelect();});</script>


<script type="text/javascript" src="<?=base_url()?>resources/miniedit/jquery.cleditor.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>resources/miniedit/jquery.cleditor.css" />

</head>

<body>
<div id="container">

<!--Header Start-->
<div id="header">
<img src="<?=base_url()?>resources/images/top_pol.jpg" border="0" usemap="#Map">

<!--logo start-->
<div class="logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>resources/images/logo.png" border="0" alt=""></a></div>
<div class="logo_text"><strong>Севастопольский свадебный портал</strong></div>
<!--logo end-->

<!--Tel start-->
<div class="tel">
<span class="tel_text"><strong>Свадьба в Севастополе</strong></span><br><br>
<strong>Контактный телефон</strong><br>
<img src="<?=base_url()?>resources/images/tel.png" alt="" vspace="5">
</div>
<!--Tel end-->

<!--avtorizacia start-->
<div class="avtoroz_count">
<?php if(Auth::isAuthorized() == FALSE): ?>
<span class="avtoriz_text">Авторизация</span>
<?=form_open(base_url().'profile/login')?>

<input type="text" name="login" id="login" value="Ваш логин" class="avtoriz_login">
<input type="password" name="password" id="password" value="" class="avtoriz_pass">
<input type="image" src="<?=base_url()?>resources/images/vhod.png" align="right" class="avtoriz_buttom">
<?=form_close()?>

<span class="avtoriz_dop"><a href="<?=base_url()?>profile/resetpassword" class="avtoriz">Забыли пароль?</a></span>
<span class="avtoriz_dop2"><a href="<?=base_url()?>profile/register" class="avtoriz">Регистрация</a></span>
<?php else: ?>
<span class="avtoriz_text">Здравствуйте:</span><br><br>
<?php $userDataLoggedIn = Auth::user(); ?>
&nbsp;&nbsp;&nbsp;<strong><?=$userDataLoggedIn->name?></strong>
<strong><span class="avtoriz_dop2"><a href="<?=base_url()?>profile" class="avtoriz">Личный кабинет</a></span></strong>
<span class="avtoriz_dop2"><a href="<?=base_url()?>profile/logout" class="avtoriz">Выход</a></span>
<?php endif; ?>
</div>
<!--avtorizacia end-->

<!--top_menu start-->
<div id="top_menu">
<a href="<?=base_url()?>cat" class="top_menu">Каталог фирм</a>
<a href="<?=base_url()?>article/adverts" class="top_menu">Реклама на сайте</a>
<a href="<?=base_url()?>adverts" class="top_menu">Доска объявлений</a>
<a href="<?=base_url()?>firm/add" class="top_menu">Добавить фирму</a>
<a href="<?=base_url()?>article/contacts" class="top_menu">Контакты</a>

<div id="poisk">
<input type="text" value="Самый быстрый поиск" class="poisk_input">
<a href=""><img src="<?=base_url()?>resources/images/poisk.png" align="right" hspace="2"></a></div>
<br class="clear">

</div>
<!--top_menu start-->

</div>
<!--Header End-->
