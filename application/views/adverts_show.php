{tpl_header}
{tpl_left}
{tpl_right}



<!--kroshki start-->
<div class="kroshki">
<?=Breadcrumbs::Adverts($advertsData->id)?>
</div>
<!--kroshki end-->

<div class="line"></div>

<h1><?=$advertsData->name?></h1>
<small>
<?php if($advertsData->type_id > 0): ?>
<strong>Тип:</strong> <a href="<?=base_url()?>adverts/type/<?=$advertsData->Type->url?>"><?=$advertsData->Type->name?></a><br>
<?php endif; ?>
<strong>Город:</strong> <?php if($advertsData->city_id > 0): ?>
<?=$advertsData->City->name?>
<?php elseif(trim($advertsData->author_city) != ''): ?>
<?=$advertsData->author_city?>
<?php else: ?>
Нет
<?php endif; ?>
<br>
<?php if(trim($advertsData->site_url) != ''): ?>
<strong>Сайт:</strong> <?=$advertsData->site_url?><br>
<?php endif; ?>
<br><strong>Данные автора:</strong><br>
<?php if($advertsData->user_id > 0): ?>
<strong>Фамилия И.О.:</strong> <?=$advertsData->User->name?><br>
<?php else: ?>
<strong>Фамилия И.О.:</strong> <?=$advertsData->author_name?><br>
<?php endif; ?>
<strong>E-mail:</strong> <?=$advertsData->author_email?><br>
<strong>Телефон:</strong> <?=$advertsData->author_phone?><br>
<strong>ICQ:</strong> <?=$advertsData->author_icq?>
</small>
<hr>
    
<h2>Цена: <?=$advertsData->price?></h2>

<p><?=$advertsData->content?></p>

<br><br>

<!--main end-->

{tpl_futter}