{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">

<h1>Добавление объявления:</h1>
<?=validation_errors('<div style="color: red;">','</div>')?>
<br/>

<script type="text/javascript" >
   $(document).ready(function() {
        $("#description").cleditor({
            width: "100%",
            height: 150
            });
        $("#a_content").cleditor({
            width: "100%",
            height: 250
            });
   });
</script>
<?=form_open(base_url().'profile/adverts/add')?>
<label for="name">Название объявления:</label>
<input type="text" name="name" id="name" value="<?=set_value('name')?>" /><br/><br/>
<label for="author_name">Фамилия И.О.:</label>
<input type="text" name="author_name" id="author_name" value="<?=set_value('author_name')?>" /><br/><br/>
<label for="author_phone">Телефон:</label>
<input type="text" name="author_phone" id="author_phone" value="<?=set_value('author_phone')?>" /><br/><br/>
<label for="author_icq">ICQ:</label>
<input type="text" name="author_icq" id="author_icq" value="<?=set_value('author_icq')?>" /><br/><br/>
<label for="author_city">Город:</label>
<input type="text" name="author_city" id="author_city" value="<?=set_value('author_city')?>" /><br/><br/>
<label for="site_url">Ссылка на сайт:</label>
<input type="text" name="site_url" id="site_url" value="<?=set_value('site_url')?>" /><br/><br/>
<label for="price">Цена: <small>(если 0 то будет не видна)</small></label>
<input type="text" name="price" id="price" value="<?=set_value('price', '0.00')?>" /><br/><br/>
<label for="group_id">Раздел: </label>
<select name="group_id" id="group_id">
<?php foreach($advertsGroupsList as $advertGroupItem): ?>
<option value="<?=$advertGroupItem->id?>"><?=$advertGroupItem->name?></option>
<?php endforeach; ?>
</select>

<label for="type_id">Тип: </label>
<select name="type_id" id="type_id">
<?php foreach($advertsTypesList as $advertTypeItem): ?>
<option value="<?=$advertTypeItem->id?>"><?=$advertTypeItem->name?></option>
<?php endforeach; ?>
</select>
<br/><br/>
<label for="a_content">Текст объявления:</label><br/>
<textarea name="a_content" id="a_content"><?=set_value('a_content')?></textarea><br/>
<br/>
<input type="submit" value="Добавить"/>

<?=form_close()?>
</div>

<!--main text end-->

{tpl_futter}