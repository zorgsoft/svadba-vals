
                <!-- CONTENT PANEL START -->
                <script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>
                <h2>Редактирование галереи</h2>
                <div class="notification information png_bg">
                    
                    <div>
                        <h4>Галерея:</h4>
                        При добавлении или изменении галереи, обязательно укзаывайте <strong>URL</strong> галереи.<br/>Не забывайте указывать zindex, это указывает на порядок вывода галереи, чем выше номер zindex, тем дальше она будет при выводе.
                    </div>
                </div>
                <div class="clear"></div>
                <div class="content-box">
                    <div class="content-box-header">
                        <h3>Осноыные параметры галереи <i>(Нажмите на заголовок что бы свернуть)</i></h3>
                    </div>
                    <div class="content-box-content">
                        
                        <?=form_open_multipart('admin/gallery/edit/'. $gallery_data->id)?>
                        <fieldset>
                        <table>
                            <tbody>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="name">Название галереи</label>
                                    </td>
                                    <td>
                                        <input name="name" id="name" type="text" class="text-input medium-input" value="<?=set_value('name', $gallery_data->name)?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="meta_title">Мета Title</label>
                                    </td>
                                    <td>
                                        <input name="meta_title" id="meta_title" type="text" class="text-input medium-input" value="<?=set_value('meta_title', $gallery_data->meta_title)?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="meta_description">Мета Description</label>
                                    </td>
                                    <td>
                                        <input name="meta_description" id="meta_description" type="text" class="text-input medium-input" value="<?=set_value('meta_description', $gallery_data->meta_description)?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="meta_keywords">Мета Keywords</label>
                                    </td>
                                    <td>
                                        <input name="meta_keywords" id="meta_keywords" type="text" class="text-input medium-input" value="<?=set_value('meta_keywords', $gallery_data->meta_keywords)?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="description">Краткое описание</label>
                                    </td>
                                    <td>
                                        <textarea name="description" id="description" class="text-input textarea wysiwyg" cols="64" rows="15"><?=set_value('description', $gallery_data->description)?></textarea>
                                        <?//=ckeditor('description', 200);?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="content">Текст галереи</label>
                                    </td>
                                    <td>
                                        <textarea name="content" id="content" class="text-input textarea wysiwyg" cols="64" rows="15"><?=set_value('content', $gallery_data->content)?></textarea>
                                        <?//=ckeditor('content', 300);?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="url">URL ссылка этой галереи</label>
                                    </td>
                                    <td>
                                        <input name="url" id="url" type="text" class="text-input medium-input" value="<?=set_value('url', $gallery_data->url)?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="zindex">Позиция галереи (zindex)</label>
                                    </td>
                                    <td>
                                        <input name="zindex" id="zindex" type="text" class="text-input small-input" value="<?=set_value('zindex', $gallery_data->zindex)?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="visible">Видимая</label>
                                    </td>
                                    <td>
                                        <input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', $gallery_data->visible)?>/>
                                    </td>
                                </tr>
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td rowspan="2">
                                        <input class="button" type="submit" value="Сохранить галерею"/>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        </fieldset>
                        <?=form_close()?>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="content-box">
                    <div class="content-box-header">
                        <h3>Фотографии</h3>
                    </div>
                    <div class="content-box-content">
                      
                        
                        <script type="text/javascript" src="<?=sysRes()?>js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
                        <script type="text/javascript" src="<?=sysRes()?>js/uploadify/swfobject.js"></script>
                        <script type="text/javascript" language="javascript">
						
		$(document).ready(function(){
		    
		    $("#upload").uploadify({
			
			uploader: '<?php echo sysRes();?>js/uploadify/uploadify.swf',
			script: '<?php echo sysRes();?>js/uploadify/uploadify.php',
			cancelImg: '<?php echo sysRes();?>js/uploadify/cancel.png',
			
			scriptAccess: 'always',
			multi: true,
			'onError' : function (a, b, c, d) {
			    if (d.status == 404)
			    alert('Could not find upload script.');
			    else if (d.type === "HTTP")
			    alert('error '+d.type+": "+d.status);
			    else if (d.type ==="File Size")
			    alert(c.name+' '+d.type+' Limit: '+Math.round(d.sizeLimit/1024)+'KB');
			    else
			    alert('error '+d.type+": "+d.text);
			    },
			'onComplete'   : function (event, queueID, fileObj, response, data) {
			    var cct = $("input[name=ci_csrf_token]").val();
			    //Post response back to controller
				//alert(response);
			    $.post('<?=base_url()?>admin/gallery/photoUpload/<?=$gallery_data->id?>',{filearray: response, 'ci_csrf_token': cct},function(info){
				$("#target").append(info);  //Add response returned by controller
				});								 			
			    },
			'onAllComplete' : function(event,data) {

				var cct = $("input[name=ci_csrf_token]").val();
			    //Post response back to controller
				//alert(response);
			    //$.post('<?=base_url()?>admin/gallery/photothumbsgen/<?=$gallery_data->id?>',{filearray: response, 'ci_csrf_token': cct},function(info){
				//;  //Add response returned by controller
				//});
				
				var html = $.ajax({
						type: "POST",
						url: "<?=base_url()?>admin/gallery/photothumbsgen/<?=$gallery_data->id?>",
						//data: ({string_text : a_name, 'ci_csrf_token': cct}),
						dataType: "html",
						async: false
					}).responseText;
				
				
                },  				
			});				
		});
	</script>
                        
                        
                       <p>
    	<label for="Filedata">Choose a File</label><br/>
        <?php echo form_upload(array('name' => 'Filedata', 'id' => 'upload'));?>
        <a href="javascript:$('#upload').uploadifyUpload();">Upload File(s)</a>
    </p>
                        <p><a href="javascript:jQuery('#uploadify').uploadifyClearQueue()">Отменить загрузку</a></p>
                    </div>
                </div>
				<div class="content-box-content">
				<? foreach ($gallery_data->Photos as $photo):?>
				  <img width=110 height=110 src="<?php echo base_url();?>resources/gallery/thumbs/<?=$photo->thumb_name?>" />
				<? endforeach;?>
				
				</div>
                <div class="clear"></div>
                <!-- CONTENT PANEL END -->

                