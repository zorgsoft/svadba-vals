<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    
        Date.format = 'dd-mm-yyyy';
        $.dpText = {
            TEXT_PREV_YEAR		:	'Предыдущий год',
            TEXT_PREV_MONTH		:	'Предыдущий месяц',
            TEXT_NEXT_YEAR		:	'Следующий год',
            TEXT_NEXT_MONTH		:	'Следующий месяц',
            TEXT_CLOSE			:	'Закрыть',
            TEXT_CHOOSE_DATE	:	'Выбор даты',
            HEADER_FORMAT		:	'mmmm yyyy'
        };
        $('#paid_to').datePicker({startDate:'01-01-2000', clickInput:true});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open('admin/firms/edit/'.$firm_data->id)?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные фирмы:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px"><label for="name">Название фирмы:</label></th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name', $firm_data->name)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
    <tr>
        <th><label for="url">Ссылка:</label></th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url', $firm_data->url)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(2);'>Мета данные:</h3>
<div id="page_2" style="display: none;"> <!-- Meta данные -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th><label for="meta_title">Meta Title:</label></th>
        <td><input name="meta_title" id="meta_title" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_title', $firm_data->meta_title)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_description">Meta Description:</label></th>
        <td><input name="meta_description" id="meta_description" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_description', $firm_data->meta_description)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_keywords">Meta Keywords:</label></th>
        <td><input name="meta_keywords" id="meta_keywords" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_keywords', $firm_data->meta_keywords)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="tags">Тэги (метки):</label></th>
        <td><input name="tags" id="tags" type="text" class="span11" style="width: 450px;" value="<?=set_value('tags', $firm_data->tags)?>" /></td>
        <td><span class="label info">Метки разделяются запятой (Пример: <strong>свадьба, вальс, платья</strong>)</span></td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(12);'>Изображения:</h3>
<div id="page_12"> <!-- Изображения -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th width="200px"><label for="image_logo_load">Логотип:</label></th>
        <td>
	    <!-- AAA -->
	    <div id="image_div">
		<?php if(trim($firm_data->img_logo) != ''): ?>
		<ul class="media-grid">
		    <li>
			 <a>
			    <img class="thumbnail" src="<?=base_url()?>resources/firmlogos/<?=$firm_data->img_logo?>">
			 </a>
		    </li>
		</ul>
		<? endif; ?>
	    </div>
	    
	    <div id="upl_but_div"><input type="button" name="upload_button" id="upload_button" value="Загрузить" class="btn small info"></div>
	    <?php if(trim($firm_data->img_logo) != ''): ?>
	    <div id="del_btn" onclick="javascript: img_del();" class="btn small danger" style="cursor: pointer;">Удалить</div>
	    <script type="text/javascript">
		$('#upl_but_div').hide();
	    </script>
	    <?php else: ?>
	    <span id="del_btn" onclick="javascript: img_del();" class="btn small danger" style="cursor: pointer; display: none;">Удалить</span>
	    <?php endif; ?>
	    
	    <script type="text/javascript">
		function img_del(){
		    $('#upl_but_div').show();
		    $('#del_btn').hide();
		    var cct = $("input[name=ci_csrf_token]").val();
		    var html = $.ajax({
			type: "POST",
			url: "<?= base_url(); ?>admin/firms/image_del",
			data: ({banner_id : '<?=$firm_data->id?>', 'ci_csrf_token': cct}),
			dataType: "html",
			async: false
		    }).responseText;
		    $('#image_div').empty();
		};

		new AjaxUpload('upload_button', {
		    action: '<?= base_url(); ?>admin/firms/image_add/<?=$firm_data->id?>',
		    name: 'image_file',
		    autoSubmit: true,
		    onComplete: function(file, response) {
			    $('#upl_but_div').hide();
			    $('#del_btn').show();
			    $('#image_div').empty().append(response);
                    }
                });
	    </script>
	    <!-- BBB -->
	</td>
        <td>
	    &nbsp;
	</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(3);'>Тексты фирмы:</h3>
<div id="page_3" style="display: none;"> <!-- Текст -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th valign="top"><label for="description">Краткое содержание:</label></th>
	<td>
	    <textarea name="description" id="description" rows="6" cols="120" class="xxlarge"><?=set_value('description', $firm_data->description)?></textarea>
	    <?=Editor::CKEditor('description', '100%', '150')?>
	</td>
	<td></td>
    </tr>
    <tr>
        <th valign="top"><label for="a_content">Полный текст:</label></th>
	<td>
	    <textarea name="a_content" id="a_content" rows="12" cols="120" class="xxlarge"><?=set_value('a_content', $firm_data->content)?></textarea>
	    <?=Editor::CKEditor('a_content')?>
	</td>
	<td></td>
    </tr>
    <tr>
        <th valign="top"><label for="contacts">Контактные данные:</label></th>
	<td>
	    <textarea name="contacts" id="contacts" rows="12" cols="120" class="xxlarge"><?=set_value('contacts', $firm_data->contacts)?></textarea>
	    <?=Editor::CKEditor('contacts', '100%', '150')?>
	</td>
	<td></td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(6);'>Оплата:</h3>
<div id="page_6"> <!-- Оплата -->
<table id="id-form_2" width="100%" class="z_form">
    <tr>
        <th><label for="paid">Оплачено:</label></th>
        <td><input name="paid" id="paid" type="checkbox" <?=set_checkbox('paid', 'checked', $firm_data->paid)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="paid_sum">Сумма:</label></th>
        <td><input name="paid_sum" id="paid_sum" type="text" class="span11" style="width: 450px;" value="<?=set_value('paid_sum', $firm_data->paid_sum)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="paid_to">Оплачено до:</label></th>
        <td><input name="paid_to" id="paid_to" type="text" class="span11" style="width: 150px;" value="<?=set_value('paid_to', date('d-m-Y', strtotime($firm_data->paid_to)))?>" /></td>
        <script>
            $('.date-pick').datePicker({startDate:'<?=date('d-m-Y', strtotime($firm_data->paid_to))?>', clickInput:true});
        </script>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="user_id">Хозяин (пользователь):</label></th>
        <td>
	    <select name="user_id" id="user_id">
		<option value="0">Нет</option>
		<?php if($users_list->count()>0): ?>
		<?php foreach($users_list as $user_item): ?>
		<option value="<?=$user_item->id?>" <?=set_select('user_id', $user_item->id, $firm_data->user_id==$user_item->id ? TRUE : FALSE)?>><?=$user_item->name?> (<?=$user_item->login?>)</option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(7);'>Адреса:</h3>
<div id="page_7"> <!-- Адреса -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th valign="top"><label>Адрса:</label></th>
	<td>
            <input type="button" name="adds_add" id="adds_add" class="btn" value="Добавить новый" /><br/>
            <div id="adds_list">
            </div>
            <input type="button" name="adds_add" id="adds_add" class="btn" value="Добавить новый" />
            <script type="text/javascript">
                function adds_list(){
                    var n_firms_id = '<?=$firm_data->id?>';
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_update_firms_adds_list",
                        data: ({firms_id : n_firms_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    $("#adds_list").empty().append(html);
                }
                
                $('#adds_save').live('click', (function(){
                    var n_adds_id = $(this).parent().find("#adds_id").val();
                    var n_adds_cities_id = $(this).parent().parent().parent().find("#adds_cities_id").val();
                    var n_adds_emails = $(this).parent().parent().parent().find("#adds_emails").val();
                    var n_adds_url = $(this).parent().parent().parent().find("#adds_url").val();
                    var n_adds_phones = $(this).parent().parent().parent().find("#adds_phones").val();
                    var n_adds_address = $(this).parent().parent().parent().find("#adds_address").val();
                    var n_adds_can_edit = $(this).parent().parent().parent().find("#adds_can_edit").val();
                    var cct = $("input[name=ci_csrf_token]").val();
                    
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_firms_adds_save",
                        data: ({
                            adds_id : n_adds_id,
                            adds_cities_id : n_adds_cities_id,
                            adds_emails : n_adds_emails,
                            adds_url : n_adds_url,
                            adds_phones : n_adds_phones,
                            adds_address : n_adds_address,
                            adds_can_edit : n_adds_can_edit,
                            'ci_csrf_token': cct
                        }),
                        dataType: "html",
                        async: false
                    }).responseText;
                    
                    alert(html);
                    adds_list();
                }));
                
                $('#adds_add').live('click', (function(){
                    var n_firms_id = '<?=$firm_data->id?>';
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_firms_adds_add",
                        data: ({firms_id : n_firms_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    adds_list();
                }));
                
                $('#adds_del').live('click', (function(){
                    var n_adds_id = $(this).parent().find("#adds_id").val();
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_firms_adds_del",
                        data: ({adds_id : n_adds_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    adds_list();
                }));
                
                adds_list();
            </script>
        </td>
	<td></td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(4);'>Связи:</h3>
<div id="page_4"> <!-- Связи -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th><label for="firms_cat_id">Раздел родитель:</label></th>
        <td>
            <select name="firms_cat_id" id="firms_cat_id">
                <?php if($firms_cat_list !=NULL and $firms_cat_list->count() > 0): ?>
                <?php foreach($firms_cat_list as $firms_cat_item): ?>
                <option value="<?=$firms_cat_item->id?>"><?=$firms_cat_item->name?></option>
                <?php endforeach; ?>
                <?php endif; ?>
            </select>
            <input type="button" class="btn" name="add_cat" id="add_cat" value="Добавить раздел"/>
            <br/>
	    <select name="firms_to_cat" id="firms_to_cat" size="6" style="height: 90px;">
	    </select>
            <br/>
            <input type="button" class="btn danger" name="del_cat" id="del_cat" value="Удалить раздел"/><br/>
            
            <script type="text/javascript">
                function update_firms_to_cat(){
                    var n_firms_id = '<?=$firm_data->id?>';
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_update_firms_to_cat_list",
                        data: ({firms_id : n_firms_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    $("#firms_to_cat").empty().append(html);
                }
                
                $('#add_cat').live('click', (function(){
                    var n_firms_id = '<?=$firm_data->id?>';
                    var n_cat_id = $('#firms_cat_id').val();
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_add_cat",
                        data: ({firms_id : n_firms_id, firms_cat_id : n_cat_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    update_firms_to_cat();
                }));
                
                $('#del_cat').live('click', (function(){
                    var n_cat_id = $('#firms_to_cat').val();
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_del_cat",
                        data: ({firms_cat_id : n_cat_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    update_firms_to_cat();
                }));
                
                update_firms_to_cat();
            </script>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="city_id">Города:</label></th>
        <td>
            <select name="city_id" id="city_id">
                <?php if($cities_list != NULL and $cities_list->count() > 0): ?>
                <?php foreach($cities_list as $city_item): ?>
                <option value="<?=$city_item->id?>"><?=$city_item->name?></option>
                <?php endforeach; ?>
                <?php endif; ?>
            </select>
            <input type="button" class="btn" name="add_city" id="add_city" value="Добавить город"/>
            
            <br/>
	    <select name="firms_to_city" id="firms_to_city" size="6" style="height: 90px;">
	    </select>
            <br/>
            <input type="button" class="btn danger" name="del_city" id="del_city" value="Удалить город"/><br/>
           
            <script type="text/javascript">
                function update_firms_to_city(){
                    var n_firms_id = '<?=$firm_data->id?>';
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_update_firms_to_city_list",
                        data: ({firms_id : n_firms_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    $("#firms_to_city").empty().append(html);
                }
                
                $('#add_city').live('click', (function(){
                    var n_firms_id = '<?=$firm_data->id?>';
                    var n_cat_id = $('#city_id').val();
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_add_city_cat",
                        data: ({firms_id : n_firms_id, firms_cat_id : n_cat_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    update_firms_to_city();
                }));
                
                $('#del_city').live('click', (function(){
                    var n_cat_id = $('#firms_to_city').val();
                    var cct = $("input[name=ci_csrf_token]").val();
                    var html = $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>admin/a_firms/ajax_del_city",
                        data: ({firms_cat_id : n_cat_id, 'ci_csrf_token': cct}),
                        dataType: "html",
                        async: false
                    }).responseText;
                    update_firms_to_city();
                }));
                
                update_firms_to_city();
            </script>
        </td>
    </tr>
    <tr>
        <th><label for="gallery_id">Фото галерея:</label></th>
        <td>
            <select name="gallery_id" id="gallery_id">
		<option value="0">Нет</option>
		<?php if($gallerys_list->count()>0): ?>
		<?php foreach($gallerys_list as $gallery_item): ?>
		<option value="<?=$gallery_item->id?>" <?=set_select('gallery_id', $gallery_item->id, $firm_data->gallery_id==$gallery_item->id ? TRUE : FALSE)?>><?=$gallery_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(5);'>Видимость:</h3>
<div id="page_5" style="display: none;"> <!-- Видиость -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th><label for="visible">Видимый:</label></th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', $firm_data->visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="zindex">Очередь (zIndex):</label></th>
        <td><input name="zindex" id="zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('zindex', $firm_data->zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_name">Название для меню:</label></th>
        <td><input name="menu_name" id="menu_name" type="text" class="span11" style="width: 450px;" value="<?=set_value('menu_name', $firm_data->menu_name)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_visible">Отображать в меню:</label></th>
        <td><input name="menu_visible" id="menu_visible" type="checkbox" <?=set_checkbox('menu_visible', 'checked', $firm_data->menu_visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_zindex">Очередь в меню (zIndex):</label></th>
        <td><input name="menu_zindex" id="menu_zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('menu_zindex', $firm_data->menu_zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>
<input type="submit" class="btn primary" value="Сохранить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>