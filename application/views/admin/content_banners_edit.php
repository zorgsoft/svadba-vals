<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>

<script>
    $(document).ready(function(){
        Date.format = 'dd-mm-yyyy';
        $.dpText = {
            TEXT_PREV_YEAR		:	'Предыдущий год',
            TEXT_PREV_MONTH		:	'Предыдущий месяц',
            TEXT_NEXT_YEAR		:	'Следующий год',
            TEXT_NEXT_MONTH		:	'Следующий месяц',
            TEXT_CLOSE			:	'Закрыть',
            TEXT_CHOOSE_DATE	:	'Выбор даты',
            HEADER_FORMAT		:	'mmmm yyyy'
        };
        $('#paid_to').datePicker({startDate:'01-01-2000', clickInput:true});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open_multipart('admin/banners/edit/'.$banner_data->id)?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные банера:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Название:</th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name', $banner_data->name)?>" /></td>
        <td>
            <span class="label warning">Обязательное поле.</span>
        </td>
    </tr>
    <tr>
        <th align="right" width="200px">Заголовок (title):</th>
        <td width="450px"><input name="title" id="title" type="text" class="span11" value="<?=set_value('title', $banner_data->title)?>" /></td>
        <td>
	    &nbsp;
        </td>
    </tr>
    <tr>
        <th>Изображение:</th>
        <td>
		<div id="image_div">
		<?php if(trim($banner_data->file_name) != ''): ?>
		<ul class="media-grid">
		    <li>
			 <a>
			    <img class="thumbnail" src="<?=base_url()?>resources/banners/<?=$banner_data->file_name?>">
			 </a>
		    </li>
		</ul>
		<? endif; ?>
		</div>
		<div id="upl_but_div"><input type="button" name="upload_button" id="upload_button" value="Загрузить" class="btn small info"></div>
		<?php if(trim($banner_data->file_name) != ''): ?>
		<div id="del_btn" onclick="javascript: img_del();" class="btn small danger" style="cursor: pointer;">Удалить</div>
		<script type="text/javascript">
		    $('#upl_but_div').hide();
		</script>
		<?php else: ?>
		<span id="del_btn" onclick="javascript: img_del();" class="btn small danger" style="cursor: pointer; display: none;">Удалить</span>
		<?php endif; ?>
	    <script type="text/javascript">
		function img_del(){
		    $('#upl_but_div').show();
		    $('#del_btn').hide();
		    var cct = $("input[name=ci_csrf_token]").val();
		    var html = $.ajax({
			type: "POST",
			url: "<?= base_url(); ?>admin/banners/image_del",
			data: ({banner_id : '<?=$banner_data->id?>', 'ci_csrf_token': cct}),
			dataType: "html",
			async: false
		    }).responseText;
		    $('#image_div').empty();
		};

		new AjaxUpload('upload_button', {
		    action: '<?= base_url(); ?>admin/banners/image_add/<?=$banner_data->id?>',
		    name: 'image_file',
		    autoSubmit: true,
		    onComplete: function(file, response) {
			    $('#upl_but_div').hide();
			    $('#del_btn').show();
			    $('#image_div').empty().append(response);
                    }
                });
	    </script>
	    
	</td>
        <td>
	    &nbsp;
        </td>
    </tr>
    <tr>
        <th>Краткая ссылка:</th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url', $banner_data->url)?>" /></td>
        <td>
	    <div class="label info">Будет полностью как <?=base_url()?>/bgo/ваша_ссылка.</div>
        </td>
    </tr>
    <tr>
        <th>Полная ссылка ссылка:</th>
        <td><input name="site_url" id="site_url" type="text" class="span11" value="<?=set_value('site_url', $banner_data->site_url)?>" /></td>
        <td>
            <span class="label warning">Обязательное поле.</span>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(6);'>Оплата:</h3>
<div id="page_6"> <!-- Оплата -->
<table id="id-form_2" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Оплачено:</th>
        <td>
	    <input name="paid" id="paid" type="checkbox" <?=set_checkbox('paid', 'checked', $banner_data->paid)?>/>&nbsp;
	    - Тип оплаты: <select name="paid_type" id="paid_type">
		<option value="0" <?=set_select('paid_type', '0', $banner_data->paid_type=='0' ? TRUE : FALSE)?>>Личное (вечное)</option>
		<option value="1" <?=set_select('paid_type', '1', $banner_data->paid_type=='1' ? TRUE : FALSE)?>>Обычное (отключается при неуплате)</option>
		<option value="2" <?=set_select('paid_type', '2', $banner_data->paid_type=='2' ? TRUE : FALSE)?>>Заменяемое (заменяется другим Личным)</option>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Сумма:</th>
        <td><input name="paid_sum" id="paid_sum" type="text" class="span11" style="width: 50px;" value="<?=set_value('paid_sum', $banner_data->paid_sum)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Оплачено до:</th>
        <td><input name="paid_to" id="paid_to" type="text" class="span11" style="width: 150px;" value="<?=set_value('paid_to', $banner_data->paid_to)?>" /> </td>
        <script>
            $('.date-pick').datePicker({startDate:'2000-01-01', clickInput:true});
        </script>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Хозяин (пользователь):</th>
        <td>
	    <select name="user_id" id="user_id">
		<option value="0">Нет</option>
		<?php if($users_list->count()>0): ?>
		<?php foreach($users_list as $user_item): ?>
		<option value="<?=$user_item->id?>" <?=set_select('user_id', $user_item->id, $banner_data->user_id==$user_item->id ? TRUE : FALSE)?>><?=$user_item->name?> (<?=$user_item->login?>)</option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(5);'>Видимость:</h3>
<div id="page_5" style="display: none;"> <!-- Видиость -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Видимый:</th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', $banner_data->visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Очередь (zIndex):</th>
        <td><input name="zindex" id="zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('zindex', $banner_data->zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>
<input type="submit" class="btn primary" value="Сохранить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>