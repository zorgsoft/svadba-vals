<?php foreach($adds_list as $adds_item): ?>
<table width="100%" border="0">
    <tr>
        <td style="text-align: right" width="30%">Город:</td>
        <td>
            <select name="adds_cities_id" id="adds_cities_id">
                <option value="0" <?=set_select('adds_cities_id', '0', $adds_item->cities_id==0 ? TRUE : FALSE)?>>Нет</option>
                <?php foreach($cities_list as $cities_item): ?>
                <option value="<?=$cities_item->id?>" <?=set_select('adds_cities_id', $cities_item->id, $adds_item->cities_id==$cities_item->id ? TRUE : FALSE)?>><?=$cities_item->name?></option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">Список E-Mail:</td>
        <td>
            <input name="adds_emails" id="adds_emails" type="text" class="span11" style="width: 450px;" value="<?=$adds_item->emails?>" /><br/>
            <span class="label info">(Введите список e-mail адресов, разделяемые запятой)</span>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">URL:</td>
        <td>
            <input name="adds_url" id="adds_url" type="text" class="span11" style="width: 450px;" value="<?=$adds_item->url?>" /><br/>
            <span class="label info">(Введите адрес сайта)</span>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">Телефоны:</td>
        <td>
            <input name="adds_phones" id="adds_phones" type="text" class="span11" style="width: 450px;" value="<?=$adds_item->phones?>" /><br/>
            <span class="label info">(Введите список телефонов, разделяемые запятой)</span>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">Адрес:</td>
        <td>
            <input name="adds_address" id="adds_address" type="text" class="span11" style="width: 450px;" value="<?=$adds_item->address?>" /><br/>
            <span class="label info">(Введите полный адрес фирмы, улица, дом и т.д.)</span>
        </td>
    </tr>
    <tr style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #e3eceb;">
        <td style="text-align: right">Можно редактировать:</td>
        <td>
            <select name="adds_can_edit" id="adds_can_edit">
                <option value="0" <?=set_select('adds_can_edit', '0', $adds_item->can_edit=='0' ? TRUE : FALSE)?>>Нет</option>
                <option value="1" <?=set_select('adds_can_edit', '1', $adds_item->can_edit=='1' ? TRUE : FALSE)?>>Да</option>
            </select>
            <span class="label info">(Может ли пользователь редактировать этот адрес)</span>
        </td>
    </tr>
    <tr style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #8c8c8c;">
        <td>&nbsp;</td>
        <td style="text-align: right;">
            <input type="hidden" name="adds_id" id="adds_id" value="<?=$adds_item->id?>" />
            <input type="button" name="adds_save" id="adds_save" class="btn info" value="Сохранить" />
            &nbsp;&nbsp;&nbsp;<input type="button" name="adds_del" id="adds_del" class="btn danger" value="Удалить" />
        </td>
    </tr>
</table>
<?php endforeach; ?>