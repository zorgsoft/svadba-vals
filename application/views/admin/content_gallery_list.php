<!-- CONTENT PANEL START -->
                <h2>Список фото галерей</h2>

                <div class="content-box">
                    <div class="content-box-header">
                        <h3>Галереи</h3>
                    </div>
                    <div class="content-box-content">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
                                <tfoot>
                                    <tr>
                                        <td colspan="3"><a href ="<?=base_url()?>admin/gallery/add" class="button">Добавить галерею</a></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                            <?php if($gallery_list!=null): ?>
                            <?php foreach($gallery_list as $gallery_item): ?>
                                    <tr>
                                        <td><strong><?=$gallery_item->name?></strong></td><td>URL: <strong><?=$gallery_item->url?></strong>, zIndex: <strong><?=$gallery_item->zindex?></strong>, Фотографий: <strong><?=count($gallery_item->Photos)?></strong></td>
                                        <td width="40px" class="options-width">
                                            <a href="<?=base_url()?>admin/gallery/edit/<?=$gallery_item->id?>" title="Изменить" class="label success">Р</a>
                                            <a onclick="return confirm('Удалить галерею - <?=$gallery_item->name?>?')" href="<?=base_url()?>admin/gallery/delete/<?=$gallery_item->id?>" title="Удалить" class="label important">У</a>
                                        </td>
                                    </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                                </tbody>
                            </table>
                    </div>
                </div>

                <div class="clear"></div>
