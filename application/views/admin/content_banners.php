<h3>Список банернов</h3>
<?php if(Auth::canAccess(R_USER_ARTICLES_ADD)): ?>
<a href="<?=base_url()?>admin/banners/add" class="btn">Добавить банер</a><br/><br/>
<?php endif; ?>
<?php if($banners_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Название</th>
        <th class="table-header-repeat line-left minwidth-1">Ссылка Url</th>
        <th class="table-header-repeat line-left"><span class="table-header-text">Пользователь</span></th>
        <th class="table-header-options line-left"><span class="table-header-text">Действия</span></th>
    </tr>
    </thead> 
    <?php foreach($banners_list as $banner_item): ?>
    <tr>
        <td>
            <?php if(Auth::canAccess(R_USER_BANNERS_EDIT)): ?>
            <a href="<?=base_url()?>admin/banners/edit/<?=$banner_item->id?>" title="Редактировать"><?=$banner_item->name?></a>
            <?php else: ?>
            <?=$banner_item->name?>
            <?php endif; ?>
        </td>
        <td><?=$banner_item->url?></td>
        <td>
            <?php if($banner_item->user_id > 0): ?>
            <?=$banner_item->User->name?>
            <?php else: ?>
            Администратор
            <?php endif; ?>
        </td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_ARTICLES_EDIT)): ?>
            <a href="<?=base_url()?>admin/banners/edit/<?=$banner_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php if($banner_item->visible==1): ?>
            <a href="<?=base_url()?>admin/banners/hide/<?=$banner_item->id?>" title="Скрыть" class="label notice">С</a>
            <?php else: ?>
            <a href="<?=base_url()?>admin/banners/show/<?=$banner_item->id?>" title="Показать" class="label notice">П</a>
            <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_ARTICLES_DELETE)): ?>
            <a onclick="return confirm('Удалить банер - <?=$banner_item->name?>?')" href="<?=base_url()?>admin/banners/delete/<?=$banner_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                4: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_BANNERS_ADD)): ?>
<a href="<?=base_url()?>admin/banners/add" class="btn">Добавить банер</a><br/><br/>
<?php endif; ?>

<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Банеров ещё не существует. Создайте новый.</td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>