<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>

<script>
    $(document).ready(function(){
        Date.format = 'dd-mm-yyyy';
        $.dpText = {
            TEXT_PREV_YEAR		:	'Предыдущий год',
            TEXT_PREV_MONTH		:	'Предыдущий месяц',
            TEXT_NEXT_YEAR		:	'Следующий год',
            TEXT_NEXT_MONTH		:	'Следующий месяц',
            TEXT_CLOSE			:	'Закрыть',
            TEXT_CHOOSE_DATE	:	'Выбор даты',
            HEADER_FORMAT		:	'mmmm yyyy'
        };
        $('#paid_to').datePicker({startDate:'01-01-2000', clickInput:true});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open_multipart('admin/banners/add')?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные банера:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Название:</th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name')?>" /></td>
        <td>
            <span class="label warning">Обязательное поле.</span>
        </td>
    </tr>
    <tr>
        <th align="right" width="200px">Заголовок (title):</th>
        <td width="450px"><input name="title" id="title" type="text" class="span11" value="<?=set_value('title')?>" /></td>
        <td>
	    &nbsp;
        </td>
    </tr>
    <tr>
        <th>Изображение:</th>
        <td>
	    <input class="input-file" id="banner_file" name="banner_file" type="file">
	</td>
        <td>
	    &nbsp;
        </td>
    </tr>
    <tr>
        <th>Краткая ссылка:</th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url')?>" /></td>
        <td>
	    <div class="label info">Будет полностью как <?=base_url()?>/bgo/ваша_ссылка.</div>
        </td>
    </tr>
    <tr>
        <th>Полная ссылка ссылка:</th>
        <td><input name="site_url" id="site_url" type="text" class="span11" value="<?=set_value('site_url')?>" /></td>
        <td>
            <span class="label warning">Обязательное поле.</span>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(6);'>Оплата:</h3>
<div id="page_6"> <!-- Оплата -->
<table id="id-form_2" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Оплачено:</th>
        <td>
	    <input name="paid" id="paid" type="checkbox" <?=set_checkbox('paid', 'checked', FALSE)?>/>&nbsp;
	    - Тип оплаты: <select name="paid_type" id="paid_type">
		<option value="0">Личное (вечное)</option>
		<option value="1">Обычное (отключается при неуплате)</option>
		<option value="2">Заменяемое (заменяется другим Личным)</option>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Сумма:</th>
        <td><input name="paid_sum" id="paid_sum" type="text" class="span11" style="width: 50px;" value="0.00" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Оплачено до:</th>
        <td><input name="paid_to" id="paid_to" type="text" class="span11" style="width: 150px;" value="<?=set_value('paid_to', date('d-m-Y', now()))?>" /> </td>
        <script>
            $('.date-pick').datePicker({startDate:'2000-01-01', clickInput:true});
        </script>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Хозяин (пользователь):</th>
        <td>
	    <select name="user_id" id="user_id">
		<option value="0">Нет</option>
		<?php if($users_list->count()>0): ?>
		<?php foreach($users_list as $user_item): ?>
		<option value="<?=$user_item->id?>"><?=$user_item->name?> (<?=$user_item->login?>)</option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(5);'>Видимость:</h3>
<div id="page_5" style="display: none;"> <!-- Видиость -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Видимый:</th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', TRUE)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Очередь (zIndex):</th>
        <td><input name="zindex" id="zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('zindex', 0)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>
<input type="submit" class="btn primary" value="Добавить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>