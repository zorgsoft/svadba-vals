<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open('admin/articles/groups_edit/'.$article_groups_data->id)?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Название:</th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name', $article_groups_data->name)?>" /></td>
        <td>
            <div class="error-left"></div>
            <div class="error-inner">Обязательное поле.</div>
        </td>
    </tr>
    <tr>
        <th>Ссылка:</th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url', $article_groups_data->url)?>" /></td>
        <td>
            <div class="error-left"></div>
            <div class="error-inner">Обязательное поле.</div>
        </td>
    </tr>
    <tr>
        <th><label for="meta_title">Meta Title:</label></th>
        <td><input name="meta_title" id="meta_title" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_title', $article_groups_data->meta_title)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_description">Meta Description:</label></th>
        <td><input name="meta_description" id="meta_description" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_description', $article_groups_data->meta_description)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_keywords">Meta Keywords:</label></th>
        <td><input name="meta_keywords" id="meta_keywords" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_keywords', $article_groups_data->meta_keywords)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th valign="top"><label for="a_content">Текст:</label></th>
	<td>
	    <textarea name="a_content" id="a_content" rows="12" cols="120" class="xxlarge"><?=set_value('a_content', $article_groups_data->content)?></textarea>
	    <?=Editor::CKEditor('a_content')?>
	</td>
	<td></td>
    </tr>
    <tr>
        <th><label for="visible">Видимая:</label></th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', $article_groups_data->visible)?>/></td>
        <td>Видна пользователям</td>
    </tr>
</table>
</div>

<input type="submit" class="btn primary" value="Сохранить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>