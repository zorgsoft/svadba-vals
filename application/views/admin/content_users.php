<h3>Список пользователей</h3>
<?php if(Auth::canAccess(R_USER_ADMIN)): ?>
<a href="<?=base_url()?>admin/users/add" class="btn">Добавить пользователя</a><br/><br/>
<?php endif; ?>
<?php if($users_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Логин</th>
        <th class="table-header-repeat line-left minwidth-1">Имя</th>
        <th class="table-header-repeat line-left">E-Mail</th>
        <th class="table-header-repeat line-left">Телефон</th>
        <th class="table-header-options line-left">Действия</th>
    </tr>
    </thead> 
    <?php foreach($users_list as $user_item): ?>
    <tr>
        <td>
            <?php if(Auth::canAccess(R_USER_ADMIN)): ?>
            <a href="<?=base_url()?>admin/users/user/<?=$user_item->id?>" title="Просмотр"><?=$user_item->login?></a>
            <?php else: ?>
            <?=$user_item->login?>
            <?php endif; ?>
        </td>
        <td><?=$user_item->name?></td>
        <td><?=$user_item->email?></td>
        <td><?=$user_item->phone?></td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_ADMIN)): ?>
            <a href="<?=base_url()?>admin/users/edit/<?=$user_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_ADMIN)): ?>
            <a onclick="return confirm('Удалить пользователя - <?=$user_item->login?>?')" href="<?=base_url()?>admin/users/delete/<?=$user_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                4: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_ADMIN)): ?>
<a href="<?=base_url()?>admin/users/add" class="btn">Добавить пользователя</a><br/><br/>
<?php endif; ?>

<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Пользователей ещё не существует. Создайте нового.</td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>