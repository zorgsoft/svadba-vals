<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>

<script>
    $(document).ready(function(){
        Date.format = 'dd-mm-yyyy';
        $.dpText = {
            TEXT_PREV_YEAR		:	'Предыдущий год',
            TEXT_PREV_MONTH		:	'Предыдущий месяц',
            TEXT_NEXT_YEAR		:	'Следующий год',
            TEXT_NEXT_MONTH		:	'Следующий месяц',
            TEXT_CLOSE			:	'Закрыть',
            TEXT_CHOOSE_DATE	:	'Выбор даты',
            HEADER_FORMAT		:	'mmmm yyyy'
        };
        $('#paid_to').datePicker({startDate:'01-01-2000', clickInput:true});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open('admin/adverts/add')?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные объявления:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px">Название:</th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name')?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
    <tr>
        <th>Ссылка:</th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url')?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(2);'>Мета данные:</h3>
<div id="page_2" style="display: none;"> <!-- Meta данные -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th width="200px">Meta Title:</th>
        <td><input name="meta_title" id="meta_title" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_title')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Meta Description:</th>
        <td><input name="meta_description" id="meta_description" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_description')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Meta Keywords:</th>
        <td><input name="meta_keywords" id="meta_keywords" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_keywords')?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(3);'>Тексты объявления:</h3>
<div id="page_3"> <!-- Текст -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th width="200px">Цена:</th>
        <td><input name="price" id="price" type="text" class="span11" style="width: 90px;" value="0.00" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th valign="top">Полный текст:</th>
	<td>
	    <textarea name="a_content" id="a_content" rows="12" cols="120" class="xxlarge"><?=set_value('a_content')?></textarea>
	    <?=Editor::CKEditor('a_content')?>
	</td>
	<td></td>
    </tr>
    <tr>
        <th>Имя автора:</th>
        <td><input name="author_name" id="author_name" type="text" class="span11" style="width: 450px;" value="<?=set_value('author_name')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>E-mail автора:</th>
        <td><input name="author_email" id="author_email" type="text" class="span11" style="width: 450px;" value="<?=set_value('author_email')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Телефон автора:</th>
        <td><input name="author_phone" id="author_phone" type="text" class="span11" style="width: 450px;" value="<?=set_value('author_phone')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>ICQ автора:</th>
        <td><input name="author_icq" id="author_icq" type="text" class="span11" style="width: 450px;" value="<?=set_value('author_icq')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Город автора:</th>
        <td><input name="author_city" id="author_city" type="text" class="span11" style="width: 450px;" value="<?=set_value('author_city')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Адрес сайта:</th>
        <td><input name="site_url" id="site_url" type="text" class="span11" style="width: 450px;" value="<?=set_value('site_url')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Город:</th>
        <td>
	    <select name="city_id" id="city_id">
		<option value="0">Нет</option>
		<?php if($cities_list->count()>0): ?>
		<?php foreach($cities_list as $cities_item): ?>
		<option value="<?=$cities_item->id?>"><?=$cities_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(6);'>Оплата:</h3>
<div id="page_6"> <!-- Оплата -->
<table id="id-form_2" width="100%" class="z_form">
    <tr>
        <th width="200px">Оплачено:</th>
        <td>
	    <input name="paid" id="paid" type="checkbox" <?=set_checkbox('paid', 'checked', FALSE)?>/>&nbsp;
	    - Тип оплаты: <select name="paid_type" id="paid_type">
		<option value="0">Обычное объявление</option>
		<option value="1">Подсветка</option>
		<option value="2">VIP (выше чем остальные)</option>
	    </select> <small>(если оплатили)</small>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Сумма:</th>
        <td><input name="paid_sum" id="paid_sum" type="text" class="span11" style="width: 90px;" value="0.00" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Оплачено до:</th>
        <td><input name="paid_to" id="paid_to" type="text" class="span11" style="width: 150px;" value="<?=set_value('paid_to', date('d-m-Y', now()))?>" /></td>
        <script>
            $('.date-pick').datePicker({startDate:'2000-01-01', clickInput:true});
        </script>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Хозяин (пользователь):</th>
        <td>
	    <select name="user_id" id="user_id">
		<option value="0">Нет</option>
		<?php if($users_list->count()>0): ?>
		<?php foreach($users_list as $user_item): ?>
		<option value="<?=$user_item->id?>"><?=$user_item->name?> (<?=$user_item->login?>)</option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(4);'>Связи:</h3>
<div id="page_4"> <!-- Связи -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th width="200px">Раздел:</th>
        <td>
	    <select name="group_id" id="group_id">
		<option value="0">Нет</option>
		<?php if($adverts_groups_list->count()>0): ?>
		<?php foreach($adverts_groups_list as $adverts_groups_item): ?>
		<option value="<?=$adverts_groups_item->id?>"><?=$adverts_groups_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Тип:</th>
        <td>
	    <select name="type_id" id="type_id">
		<option value="0">Нет</option>
		<?php if($adverts_types_list->count()>0): ?>
		<?php foreach($adverts_types_list as $adverts_types_item): ?>
		<option value="<?=$adverts_types_item->id?>"><?=$adverts_types_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Фото галерея:</th>
        <td>
	    <select name="gallery_id" id="gallery_id">
		<option value="0">Нет</option>
		<?php if($gallerys_list->count()>0): ?>
		<?php foreach($gallerys_list as $gallery_item): ?>
		<option value="<?=$gallery_item->id?>"><?=$gallery_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(5);'>Видимость:</h3>
<div id="page_5" style="display: none;"> <!-- Видиость -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th width="200px">Видимый:</th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', TRUE)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Очередь (zIndex):</th>
        <td><input name="zindex" id="zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('zindex', 0)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Пользователь может изменять:</th>
        <td><input name="can_edit" id="can_edit" type="checkbox" <?=set_checkbox('can_edit', 'checked', TRUE)?>/></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>
<input type="submit" class="btn primary" value="Добавить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>