<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open('admin/adverts/cat_edit/'.$adverts_cat->id)?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные раздела:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px"><label for="name">Название раздела:</label></th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name', $adverts_cat->name)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
    <tr>
        <th><label for="url">Ссылка:</label></th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url', $adverts_cat->url)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(2);'>Мета данные:</h3>
<div id="page_2" style="display: none;"> <!-- Meta данные -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th width="200px"><label for="meta_title">Meta Title:</label></th>
        <td><input name="meta_title" id="meta_title" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_title', $adverts_cat->meta_title)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_description">Meta Description:</label></th>
        <td><input name="meta_description" id="meta_description" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_description', $adverts_cat->meta_description)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_keywords">Meta Keywords:</label></th>
        <td><input name="meta_keywords" id="meta_keywords" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_keywords', $adverts_cat->meta_keywords)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(3);'>Тексты раздела:</h3>
<div id="page_3"> <!-- Текст -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th valign="top" width="200px"><label for="a_content">Полный текст:</label></th>
	<td>
	    <textarea name="a_content" id="a_content" rows="12" cols="120" class="xxlarge"><?=set_value('a_content', $adverts_cat->content)?></textarea>
	    <?=Editor::CKEditor('a_content', '100%', '150')?>
	</td>
	<td></td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(4);'>Связи:</h3>
<div id="page_4" style="display: none;"> <!-- Связи -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th width="200px"><label for="parent_id">Раздел родитель:</label></th>
        <td>
	    <select name="parent_id" id="parent_id">
		<option value="">Нет</option>
		<?php if($adverts_cat_list->count()>0): ?>
		<?php foreach($adverts_cat_list as $adverts_cat_item): ?>
		<?php if($adverts_cat_item->id != $adverts_cat->id):?>
		<option value="<?=$adverts_cat_item->id?>" <?=set_select('parent_id', $adverts_cat_item->id, $adverts_cat->parent_id==$adverts_cat_item->id ? TRUE : FALSE)?>><?=$adverts_cat_item->name?></option>
		<?php endif; ?>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(5);'>Видимость:</h3>
<div id="page_5" style="display: none;"> <!-- Видиость -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th width="200px"><label for="visible">Видимый:</label></th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', $adverts_cat->visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="can_add">Пользователи могут добавлять сюда объявления:</label></th>
        <td><input name="can_add" id="can_add" type="checkbox" <?=set_checkbox('can_add', 'checked', $adverts_cat->can_add)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="zindex">Очередь (zIndex):</label></th>
        <td><input name="zindex" id="zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('zindex', $adverts_cat->zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>
<input type="submit" class="btn primary" value="Сохранить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>