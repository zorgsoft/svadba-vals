<h3>Список последних (50) действий пользователей</h3>
<?php if($actionsList->count()>0): ?>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1" width="130px">Дата</th>
        <th class="table-header-repeat line-left minwidth-1" width="50px">Логин</th>
        <th class="table-header-repeat line-left">Название</th>
        <th class="table-header-options line-left">Действия</th>
    </tr>
    </thead> 
    <?php foreach($actionsList as $actionsItem): ?>
    <tr>
        <td><?=date('d-m-Y [H:i:s]', strtotime($actionsItem->created_at))?></td>
        <td>
            <?php if(Auth::canAccess(R_USER_ADMIN)): ?>
            <a href="<?=base_url()?>admin/users/user/<?=$actionsItem->User->id?>" title="Просмотр пользователя"><?=$actionsItem->User->login?></a>
            <?php else: ?>
            <?=$user_item->login?>
            <?php endif; ?>
        </td>
        <td><a title="Перейти к просмотру или редактированию изменённого объекта" href="<?=$actionsItem->url?>"><?=$actionsItem->name?></a></td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_ADMIN)): ?>
            <a onclick="return confirm('Удалить действие - <?=$actionsItem->name?>?')" href="<?=base_url()?>admin/options/log/delete/<?=$actionsItem->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                3: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->

<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Активности нет.</td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>