<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?=$title?></title>

<link rel="stylesheet" href="<?=sysRes()?>bs/bootstrap.css" type="text/css" media="screen" title="default" />


<style type="text/css">
      body {
        padding-top: 60px;
      }
    </style>
<!--  jquery core -->
<script src="http://code.jquery.com/jquery-1.5.1.min.js"></script>
<script src="<?=sysRes()?>bs/js/bootstrap-dropdown.js"></script>
<script src="<?=sysRes()?>bs/js/bootstrap-twipsy.js"></script>
<script src="<?=sysRes()?>bs/js/bootstrap-scrollspy.js"></script>

<!--  checkbox styling script -->
<script src="<?=sysRes()?>js/jquery/jquery.tablesorter.min.js" type="text/javascript"></script>

 
<!--  date picker script -->
<link rel="stylesheet" href="<?=sysRes()?>css/datePicker.css" type="text/css" media="screen" title="default" />
<script src="<?=sysRes()?>js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/date.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=sysRes()?>js/ajaxupload.js"></script>
<script type="text/javascript" src="<?=sysRes()?>ckeditor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=sysRes()?>ckeditor/AjexFileManager/ajex.js"></script>


<script type="text/javascript" charset="utf-8">
        $(function()
{

// initialise the "Select date" link
$('#date-pick')
	.datePicker(
		// associate the link with a date picker
		{
			createButton:false,
			startDate:'01/01/2005',
			endDate:'31/12/2020'
		}
	).bind(
		// when the link is clicked display the date picker
		'click',
		function()
		{
			updateSelects($(this).dpGetSelected()[0]);
			$(this).dpDisplay();
			return false;
		}
	).bind(
		// when a date is selected update the SELECTs
		'dateSelected',
		function(e, selectedDate, $td, state)
		{
			updateSelects(selectedDate);
		}
	).bind(
		'dpClosed',
		function(e, selected)
		{
			updateSelects(selected[0]);
		}
	);
	
var updateSelects = function (selectedDate)
{
	var selectedDate = new Date(selectedDate);
	$('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
	$('#m option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');
	$('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
}
// listen for when the selects are changed and update the picker
$('#d, #m, #y')
	.bind(
		'change',
		function()
		{
			var d = new Date(
						$('#y').val(),
						$('#m').val()-1,
						$('#d').val()
					);
			$('#date-pick').dpSetSelected(d.asString());
		}
	);

// default the position of the selects to today
var today = new Date();
updateSelects(today.getTime());

// and update the datePicker to reflect it...
$('#d').trigger('change');
});
</script>

</head>

<body> 
<div class="topbar" data-dropdown="dropdown">
      <div class="topbar-inner">
        <div class="container-fluid">
          <a class="brand" href="#">Свадьба Вальс</a>
		<?php
			// Пункты меню
			// Сначала идёт описание основного меню, в виде
			// Название, ссылка, название роли для доступа, название выбранного меню, название выбранного подменю
			// Затем, идут строки описания подменю в виде
			// Название меню|ссылка|роль
			// Параметры подменю разделяются символом прямой черты |
			// Роль должна быть обязательно указанна, если затрудняетесь,
			// то ставте стандартную роль admin
			// Так же вы можете использовать константы для прав, которые находятся в файле
			// /application/config/constants.php
		?>
		<?=Menu::adminMenu('Начало', base_url().'admin', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Добро пожаловать|'.base_url().'admin|'.R_USER_ADMIN,
                        'Запросы|'.base_url().'admin/requests|'.R_USER_ADMIN,
			'Новости|'.base_url().'admin/news|'.R_USER_ADMIN
		))?>
		
		<?=Menu::adminMenu('Статьи', base_url().'admin/articles', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Список статей|'.base_url().'admin/articles|'.R_USER_ADMIN,
			'Добавить статью|'.base_url().'admin/articles/add|'.R_USER_ARTICLES_ADD,
			'Группы статей|'.base_url().'admin/articles/groups|'.R_USER_ARTICLES_GROUPS,
			'Добавить группу|'.base_url().'admin/articles/groups_add|'.R_USER_ARTICLES_GROUPS_ADD,
		))?>
                
                
                <?=Menu::adminMenu('Фирмы', base_url().'admin/firms', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Разделы|'.base_url().'admin/firms/cat|'.R_USER_FIRMS_GROUPS,
			'Фирмы|'.base_url().'admin/firms|'.R_USER_FIRMS
		))?>
		
                <?=Menu::adminMenu('Объявления', base_url().'admin/adverts', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Разделы|'.base_url().'admin/adverts/cat|'.R_USER_ADMIN,
			'Типы|'.base_url().'admin/adverts/types|'.R_USER_ADMIN,
			'Объявления|'.base_url().'admin/adverts|'.R_USER_ADMIN
		))?>
		
                <?=Menu::adminMenu('Банеры', base_url().'admin/banners', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Банеры|'.base_url().'admin/banners|'.R_USER_ADMIN,
			'Добавить|'.base_url().'admin/banners/add|'.R_USER_ADMIN
		))?>
                
                
                <?=Menu::adminMenu('Галлереи', base_url().'admin/gallery', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Список галлерей|'.base_url().'admin/gallery|'.R_USER_FIRMS_GROUPS,
			'Добавить галлерею|'.base_url().'admin/gallery/add|'.R_USER_FIRMS
		))?>
		
                
		<?=Menu::adminMenu('Пользователи', base_url().'admin/users', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Список пользователей|'.base_url().'admin/users|'.R_USER_ADMIN,
			'Добавить пользователя|'.base_url().'admin/users/add|'.R_USER_ADMIN,
			'Групы доступа|'.base_url().'admin/access|'.HC_SUPER_ADMIN_NAME,
			'Добавить групу|'.base_url().'admin/access/add|'.HC_SUPER_ADMIN_NAME,
		))?>
		
		<?=Menu::adminMenu('Свадьбы', base_url().'admin/weddings', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Список свадеб|'.base_url().'admin/weddings|'.R_USER_ADMIN,
			
			
			
		))?>
		
		<?=Menu::adminMenu('SEO', base_url().'admin/seo', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Социальные кнопки|'.base_url().'admin/socbuttons|'.R_USER_ADMIN,
			
			
		))?>

		<?=Menu::adminMenu('Настройки', 'admin/options', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Настройка сайта|'.base_url().'admin/options|'.R_USER_ADMIN,
			'Шаблоны|'.base_url().'admin/templates|'.R_USER_ADMIN,
			'Права доступа|'.base_url().'admin/options/access|'.HC_SUPER_ADMIN_NAME,
			'Последние действия|'.base_url().'admin/options/log|'.R_USER_ADMIN,
		))?>
          <p class="pull-right">Вошли как <a href="#"><?=Auth::user()->name?></a></p>
        </div>
      </div>
    </div>

<!--  start left menu -->
    <div class="container-fluid">
      <div class="sidebar">
        <div class="well">
          <h5>Быстро добавить</h5>
          <ul>
            <li><a href="#">Статью</a></li>
            <li><a href="#">Фирму</a></li>
            <li><a href="#">Объявление</a></li>
            <li><a href="#">Новость</a></li>
          </ul>
          <h5>Статистика</h5>
          <ul>
            <li><a href="#">Последние действия</a></li>
          </ul>
	  <h5>Разное</h5>
	  <ul>
		<li><a href="#">Перейти на сайт</a></li>
		<li><a href="#">Выход</a></li>
	  </ul>
        </div>
      </div>
 
<div class="content">
        <!-- Main hero unit for a primary marketing message or call to action -->
        <blockquote>
          <h1>{page_head}</h1>
        </blockquote>
	
	{content}
	
<footer>
	<p>HardKor CMS &copy; Copyright ZoRg Soft <a href="http://agarkov.com">agarkov.org</a>. All rights reserved.</p>
</footer>
</div>
</div>
</div>

</body>
</html>