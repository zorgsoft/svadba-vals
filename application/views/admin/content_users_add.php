<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open('admin/users/add')?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные:</h3>

<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th><label for="login">Логин (login):</label></th>
        <td><input name="login" id="login" type="text" class="span11" value="<?=set_value('login')?>" /></td>
        <td>
            <span class="label warning">Обязательное поле.</span>
        </td>
    </tr>
    <tr>
        <th><label for="password">Пароль:</label></th>
        <td><input name="password" id="password" type="text" class="span11" value="<?=set_value('password')?>" /></td>
        <td>
            <span class="label warning">Обязательное поле.</span>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(2);'>Дополнительно:</h3>
<div id="page_2" style="display: none;"> <!-- Meta данные -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th><label for="name">Фамилия И.О.:</label></th>
        <td><input name="name" id="name" type="text" class="span11" value="<?=set_value('name')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="email">E-mail:</label></th>
        <td><input name="email" id="email" type="text" class="span11" value="<?=set_value('email')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="phone">Телефон:</label></th>
        <td><input name="phone" id="phone" type="text" class="span11" value="<?=set_value('phone')?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="comment">Комментарий:</label></th>
        <td><input name="comment" id="comment" type="text" class="span11" value="<?=set_value('comment')?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(4);'>Права:</h3>
<div id="page_4" style="display: none;"> <!-- Связи -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th><label for="roles_id">Роль:</label></th>
        <td>
	    <select name="roles_id" id="roles_id">
		<?php if($roles_list->count()>0): ?>
		<?php foreach($roles_list as $role_item): ?>
		<?php if($role_item->id == 2): ?>
		<option value="<?=$role_item->id?>" selected><?=$role_item->name?></option>
		<?php else: ?>
		<option value="<?=$role_item->id?>"><?=$role_item->name?></option>
		<?php endif; ?>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<input type="submit" class="btn primary" value="Добавить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>