<h3>Список шаблонов</h3>
<?php if(Auth::canAccess(R_USER_TEMPLATES_ADD)): ?>
<a href="<?=base_url()?>admin/templates/add" class="button_n">Добавить шаблон</a><br/><br/>
<?php endif; ?>
<?php if($templates_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
    <thead> 
    <tr>
        <th class="table-header-check"><a id="toggle-all" ></a> </th>
        <th class="table-header-repeat line-left minwidth-1"><a href="">Название</a>	</th>
        <th class="table-header-repeat line-left minwidth-1"><a href="">Название (англ)</a></th>
        <th class="table-header-options line-left"><span class="table-header-text">Действия</span></th>
    </tr>
    </thead> 
    <?php foreach($templates_list as $template_item): ?>
    <tr>
        <td><input  type="checkbox"/></td>
        <td><?=$template_item->name?></td>
        <td><?=$template_item->template_name?></td>
        <td class="options-width">
            <?php if(Auth::canAccess(R_USER_TEMPLATES_EDIT)): ?>
            <a href="<?=base_url()?>admin/templates/edit/<?=$template_item->id?>" title="Редактировать" class="icon-1 info-tooltip"></a>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_TEMPLATES_DELETE)): ?>
            <a onclick="return confirm('Удалить шаблон - <?=$template_item->name?>?')" href="<?=base_url()?>admin/templates/delete/<?=$template_item->id?>" title="Удалить" class="icon-2 info-tooltip"></a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                0: { sorter: false },
                3: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_TEMPLATES_ADD)): ?>
<a href="<?=base_url()?>admin/template/add" class="button_n">Добавить шаблон</a><br/><br/>
<?php endif; ?>
<!--  start actions-box ............................................... -->
<div id="actions-box">
    <a href="" class="action-slider"></a>
    <div id="actions-box-slider">
        <?php if(Auth::canAccess(R_USER_TEMPLATES_DELETE)): ?>
        <a href="" class="action-delete">Удалить</a>
        <?php endif; ?>
    </div>
    <div class="clear"></div>
</div>
<!-- end actions-box........... -->

<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Шаблоны ещё не существует. Создайте новый.</td>
<td class="red-right"><a class="close-red"><img src="<?=sysRes()?>images/table/icon_close_red.gif"   alt="" /></a></td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>