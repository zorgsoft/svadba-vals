<h3>Список фирм</h3>
<?php if(Auth::canAccess(R_USER_FIRMS_ADD)): ?>
<a href="<?=base_url()?>admin/firms/add" class="btn">Добавить фирму</a><br/><br/>
<?php endif; ?>
<?php if($firms_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Название</th>
        <th class="table-header-repeat line-left minwidth-1">Разделы</th>
        <th class="table-header-repeat line-left minwidth-1">Ссылка Url</th>
        <th class="table-header-repeat line-left">Login</th>
        <th class="table-header-repeat line-left">Добавили</th>
        <th class="table-header-repeat line-left">Изменили</th>
        <th class="table-header-repeat line-left">Оплата</th>
        <th class="table-header-options line-left"><span class="table-header-text">Действия</span></th>
    </tr>
    </thead> 
    <?php foreach($firms_list as $firm_item): ?>
    <tr>
        <td>
            <?php if(Auth::canAccess(R_USER_FIRMS_EDIT)): ?>
            <a href="<?=base_url()?>admin/firms/edit/<?=$firm_item->id?>" title="Редактировать"><?=$firm_item->name?></a>
            <?php else: ?>
            <?=$firm_item->name?>
            <?php endif; ?>
        </td>
        <td>
            <?php if($firm_item->Firms_cats->count() > 0): ?>
            <select name="cat_list" style="width: auto;">
            <?php foreach($firm_item->Firms_cats as $firm_cat_item): ?>
            <option value="<?=$firm_cat_item->Firms_cat->id?>"><?=$firm_cat_item->Firms_cat->name?></option>
            <?php endforeach; ?>
            </select>
            <?php endif; ?>
        </td>
        <td><?=$firm_item->url?></td>
        <td><a href="<?=base_url()?>admin/user/<?=$firm_item->User->id?>"><?=$firm_item->User->login?></a></td>
        <td><?=date('d-m-Y', strtotime($firm_item->created_at))?></td>
        <td><?=date('d-m-Y', strtotime($firm_item->updated_at))?></td>
        <td>
            <?php if($firm_item->paid == 1): ?>
            <center><?=date('d-m-Y', strtotime($firm_item->paid_to))?><br/>
            <strong><?=$firm_item->paid_sum?></strong></center>
            <?php else: ?>
            Нет
            <?php endif; ?>
        </td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_FIRMS_EDIT)): ?>
            <a href="<?=base_url()?>admin/firms/edit/<?=$firm_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php if($firm_item->visible==1): ?>
            <a href="<?=base_url()?>admin/firms/hide/<?=$firm_item->id?>" title="Скрыть" class="label notice">С</a>
            <?php else: ?>
            <a href="<?=base_url()?>admin/firms/show/<?=$firm_item->id?>" title="Показать" class="label notice">П</a>
            <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_FIRMS_DELETE)): ?>
            <a onclick="return confirm('Удалить фирму - <?=$firm_item->name?>?')" href="<?=base_url()?>admin/firms/delete/<?=$firm_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                7: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_FIRMS_ADD)): ?>
<a href="<?=base_url()?>admin/firms/add" class="btn">Добавить фирму</a><br/><br/>
<?php endif; ?>
<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Фирм ещё не существует. Создайте новую.</td>
<td class="red-right"><a class="close-red"><img src="<?=sysRes()?>images/table/icon_close_red.gif"   alt="" /></a></td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>