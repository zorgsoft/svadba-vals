<h3>Разделы фирм</h3>
<?php if(Auth::canAccess(R_USER_FIRMS_GROUPS_ADD)): ?>
<a href="<?=base_url()?>admin/firms/cat_add" class="btn">Добавить раздел</a><br/><br/>
<?php endif; ?>
<?php if($firms_cat_list->count()>0): ?>

<?php if($firms_cat_parent != NULL): ?>
<h3>Список подразделов раздела <?=$firms_cat_parent->name?></h3>
<strong><a href="<?=base_url()?>admin/firms/cat"><img src="<?=sysRes()?>images/iconic/gray_dark/folder_fill_16x16.png" style="vertical-align: sub;" /> В самое начало</a>
<?php if($firms_cat_parent->parent_id > 0): ?>
<a href="<?=base_url()?>admin/firms/cat/<?=$firms_cat_parent->Parent->url?>"><img src="<?=sysRes()?>images/iconic/gray_dark/arrow_up_16x16.png" style="vertical-align: sub;" /> На уровень выше</a>
<?php endif; ?>
</strong>
<br/><br/>
<?php endif; ?>

<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Название</th>
        <th class="table-header-repeat line-left minwidth-1">Ссылка Url</th>
        <th class="table-header-options line-left"><span class="table-header-text">Действия</span></th>
    </tr>
    </thead> 
    <?php foreach($firms_cat_list as $firm_cat_item): ?>
    <tr>
        <td>
            <?php if($firm_cat_item->Childs->count()>0): ?>
            <a title="У этого раздела есть подразделы" href="<?=base_url()?>admin/firms/cat/<?=$firm_cat_item->url?>">
                <img src="<?=sysRes()?>images/iconic/gray_dark/folder_fill_16x16.png" style="vertical-align: sub;" />  <?=$firm_cat_item->name?>
            </a>
            <?php else: ?>
            <?=$firm_cat_item->name?>
            <?php endif; ?>
        </td>
        <td><?=$firm_cat_item->url?></td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_FIRMS_GROUPS_EDIT)): ?>
            <a href="<?=base_url()?>admin/firms/cat_edit/<?=$firm_cat_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php if($firm_cat_item->visible==1): ?>
            <a href="<?=base_url()?>admin/firms/cat_hide/<?=$firm_cat_item->id?>" title="Скрыть" class="label notice">С</a>
            <?php else: ?>
            <a href="<?=base_url()?>admin/firms/cat_show/<?=$firm_cat_item->id?>" title="Показать" class="label notice">П</a>
            <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_FIRMS_GROUPS_DELETE)): ?>
            <a onclick="return confirm('Удалить раздел - <?=$firm_cat_item->name?>?')" href="<?=base_url()?>admin/firms/cat_delete/<?=$firm_cat_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                3: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_FIRMS_GROUPS_ADD)): ?>
<a href="<?=base_url()?>admin/firms/cat_add" class="btn">Добавить раздел</a><br/><br/>
<?php endif; ?>
<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Разделов фирм ещё не существует. Создайте новый.</td>
<td class="red-right"><a class="close-red"><img src="<?=sysRes()?>images/table/icon_close_red.gif"   alt="" /></a></td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>