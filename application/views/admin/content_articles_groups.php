<h3>Список груп статей</h3>
<?php if(Auth::canAccess(R_USER_ARTICLES_ADD)): ?>
<a href="<?=base_url()?>admin/articles/groups_add" class="btn">Добавить групу</a><br/><br/>
<?php endif; ?>
<?php if($articles_groups_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Название</th>
        <th class="table-header-repeat line-left minwidth-1">Ссылка Url</th>
        <th class="table-header-repeat line-left"><span class="table-header-text">Дочерние статьи</span></th>
        <th class="table-header-options line-left"><span class="table-header-text">Действия</span></th>
    </tr>
    </thead> 
    <?php foreach($articles_groups_list as $article_groups_item): ?>
    <tr>
        <td><a href="<?=base_url()?>admin/articles/<?=$article_groups_item->id?>" title="Открыть список статей для этой групы"><?=$article_groups_item->name?></a></td>
        <td><?=$article_groups_item->url?></td>
        <td>
            <?php if($article_groups_item->Articles->count()>0) :?>
            <select name="article_id" id="article_id">
            <?php foreach($article_groups_item->Articles as $group_article_item): ?>
            <option value="<?=$group_article_item->id?>"><?=$group_article_item->name?></option>
            <?php endforeach; ?>
            </select>
            (<?=$article_groups_item->Articles->count()?>)
            <?php else: ?>
            Нет
            <?php endif; ?>
        </td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_ARTICLES_EDIT)): ?>
            <a href="<?=base_url()?>admin/articles/groups_edit/<?=$article_groups_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_ARTICLES_DELETE)): ?>
            <a onclick="return confirm('Удалить групу сатей - <?=$article_groups_item->name?>?')" href="<?=base_url()?>admin/articles/groups_delete/<?=$article_groups_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                3: { sorter: false },
                4: { sorter: false },
                5: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_ARTICLES_ADD)): ?>
<a href="<?=base_url()?>admin/articles/groups_add" class="btn">Добавить групу</a><br/><br/>
<?php endif; ?>

<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Групы статей ещё не существуют. Создайте новую.</td>
<td class="red-right"><a class="close-red"><img src="<?=sysRes()?>images/table/icon_close_red.gif"   alt="" /></a></td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>