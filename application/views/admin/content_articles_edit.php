<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open('admin/articles/edit/'.$article_data->id)?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные статьи:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px"><label for="name">Название статьи:</label></th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name', $article_data->name)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
    <tr>
        <th><label for="url">Ссылка:</label></th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url', $article_data->url)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(2);'>Мета данные:</h3>
<div id="page_2" style="display: none;"> <!-- Meta данные -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th><label for="meta_title">Meta Title:</label></th>
        <td><input name="meta_title" id="meta_title" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_title', $article_data->meta_title)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_description">Meta Description:</label></th>
        <td><input name="meta_description" id="meta_description" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_description', $article_data->meta_description)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_keywords">Meta Keywords:</label></th>
        <td><input name="meta_keywords" id="meta_keywords" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_keywords', $article_data->meta_keywords)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(3);'>Текст статьи:</h3>
<div id="page_3"> <!-- Текст -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th valign="top"><label for="description">Краткое содержание:</label></th>
	<td>
	    <textarea name="description" id="description" rows="6" cols="120" class="xxlarge"><?=set_value('description', $article_data->description)?></textarea>
	    <?=Editor::CKEditor('description', '100%', '150')?>
	</td>
	<td></td>
    </tr>
    <tr>
        <th valign="top"><label for="a_content">Текст статьи:</label></th>
	<td>
	    <textarea name="a_content" id="a_content" rows="12" cols="120" class="xxlarge"><?=set_value('a_content', $article_data->content)?></textarea>
	    <?=Editor::CKEditor('a_content')?>
	</td>
	<td></td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(4);'>Связи:</h3>
<div id="page_4" style="display: none;"> <!-- Связи -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th><label for="user_id">Принадлежит пользователю (автор):</label></th>
        <td>
	    <select name="user_id" id="user_id">
		<option value="">Нет</option>
		<?php if($users_list->count()>0): ?>
		<?php foreach($users_list as $user_item): ?>
		<option value="<?=$user_item->id?>" <?=set_select('user_id', $user_item->id, $article_data->user_id==$user_item->id ? TRUE : FALSE)?>><?=$user_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="parent_id">Статья родитель:</label></th>
        <td>
	    <select name="parent_id" id="parent_id">
		<option value="">Нет</option>
		<?php if($articles_list->count()>0): ?>
		<?php foreach($articles_list as $article_item): ?>
		<?php if($article_item->id != $article_data->id): ?>
		<option value="<?=$article_item->id?>" <?=set_select('parent_id', $article_item->id, $article_data->parent_id==$article_item->id ? TRUE : FALSE)?>><?=$article_item->name?></option>
		<?php endif; ?>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="group_id">Група родитель:</label></th>
        <td>
	    <select name="group_id" id="group_id">
		<option value="">Нет</option>
		<?php if($articles_groups_list->count()>0): ?>
		<?php foreach($articles_groups_list as $articles_groups_item): ?>
		<option value="<?=$articles_groups_item->id?>" <?=set_select('group_id', $articles_groups_item->id, $article_data->group_id==$articles_groups_item->id ? TRUE : FALSE)?>><?=$articles_groups_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="gallery_id">Фото галерея:</label></th>
        <td>
	    <select name="gallery_id" id="gallery_id">
		<option value="">Нет</option>
		<?php if($gallerys_list->count()>0): ?>
		<?php foreach($gallerys_list as $gallery_item): ?>
		<option value="<?=$gallery_item->id?>" <?=set_select('gallery_id', $gallery_item->id, $article_data->gallery_id==$gallery_item->id ? TRUE : FALSE)?>><?=$gallery_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(5);'>Видимость:</h3>
<div id="page_5" style="display: none;"> <!-- Видиость -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th><label for="visible">Видимая:</label></th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', $article_data->visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="zindex">Очередь (zIndex):</label></th>
        <td><input name="zindex" id="zindex" type="text" class="small" style="width: 50px;" value="<?=set_value('zindex', $article_data->zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_name">Название для меню:</label></th>
        <td><input name="menu_name" id="menu_name" type="text" class="xlarge" style="width: 450px;" value="<?=set_value('menu_name', $article_data->menu_name)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_visible">Отображать в меню:</label></th>
        <td><input name="menu_visible" id="menu_visible" type="checkbox" <?=set_checkbox('menu_visible', 'checked', $article_data->menu_visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_zindex">Очередь в меню (zIndex):</label></th>
        <td><input name="menu_zindex" id="menu_zindex" type="text" class="small" style="width: 50px;" value="<?=set_value('menu_zindex', $article_data->menu_zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="access_rights_id">Права на просмотр:</label></th>
        <td>
	    <select name="access_rights_id" id="access_rights_id">
		<option value="">Можно всем</option>
		<?php if($roles_list->count()>0): ?>
		<?php foreach($roles_list as $role_item): ?>
		<option value="<?=$role_item->id?>" <?=set_select('access_rights_id', $role_item->id, $article_data->access_rights_id==$role_item->id ? TRUE : FALSE)?>><?=$role_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>
<input type="submit" class="btn primary" value="Сохранить статью" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>