<h3>Список статей</h3>
<?php if(Auth::canAccess(R_USER_ARTICLES_ADD)): ?>
<a href="<?=base_url()?>admin/articles/add" class="btn">Добавить статью</a><br/><br/>
<?php endif; ?>
<?php if($articles_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Название</th>
        <th class="table-header-repeat line-left minwidth-1">Ссылка Url</th>
        <th class="table-header-repeat line-left"><span class="table-header-text">Родители</span></th>
        <th class="table-header-repeat line-left"><span class="table-header-text">Дочерние статьи</span></th>
        <th class="table-header-options line-left"><span class="table-header-text">Действия</span></th>
    </tr>
    </thead> 
    <?php foreach($articles_list as $article_item): ?>
    <tr>
        <td>
            <?php if(Auth::canAccess(R_USER_ARTICLES_EDIT)): ?>
            <a href="<?=base_url()?>admin/articles/edit/<?=$article_item->id?>" title="Редактировать"><?=$article_item->name?></a>
            <?php else: ?>
            <?=$article_item->name?>
            <?php endif; ?>
        </td>
        <td><?=$article_item->url?></td>
        <td>
            <?php if($article_item->parent_id>0) :?>
            <p>Статья: <?=$article_item->Parent->name?></p>
            <?php endif; ?>
            <?php if($article_item->group_id>0) :?>
            <p>Група: <a href="<?=base_url()?>admin/articles/<?=$article_item->Group->id?>"><?=$article_item->Group->name?></a></p>
            <?php endif; ?>
        </td>
        <td>
            <?php if($article_item->Childs->count()>0) :?>
            Всего: <?=$article_item->Childs->count()?>
            <?php else: ?>
            Нет
            <?php endif; ?>
        </td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_ARTICLES_EDIT)): ?>
            <a href="<?=base_url()?>admin/articles/edit/<?=$article_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php if($article_item->visible==1): ?>
            <a href="<?=base_url()?>admin/articles/hide/<?=$article_item->id?>" title="Скрыть" class="label notice">С</a>
            <?php else: ?>
            <a href="<?=base_url()?>admin/articles/show/<?=$article_item->id?>" title="Показать" class="label notice">П</a>
            <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_ARTICLES_DELETE)): ?>
            <a onclick="return confirm('Удалить статью - <?=$article_item->name?>?')" href="<?=base_url()?>admin/articles/delete/<?=$article_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                4: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_ARTICLES_ADD)): ?>
<a href="<?=base_url()?>admin/articles/add" class="btn">Добавить статью</a><br/><br/>
<?php endif; ?>

<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Статей ещё не существует. Создайте новую.</td>
<td class="red-right"><a class="close-red"><img src="<?=sysRes()?>images/table/icon_close_red.gif"   alt="" /></a></td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>