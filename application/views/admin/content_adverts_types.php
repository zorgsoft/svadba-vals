<h3>Типы объявлений</h3>
<?php if(Auth::canAccess(R_USER_ADVERTS_TYPES_ADD)): ?>
<a href="<?=base_url()?>admin/adverts/types_add" class="btn">Добавить тип</a><br/><br/>
<?php endif; ?>
<?php if($adverts_types_list->count()>0): ?>

<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Название</th>
        <th class="table-header-repeat line-left minwidth-1">Ссылка Url</th>
        <th class="table-header-options line-left">Действия</th>
    </tr>
    </thead> 
    <?php foreach($adverts_types_list as $adverts_types_item): ?>
    <tr>
        <td>
            <?=$adverts_types_item->name?>
        </td>
        <td><?=$adverts_types_item->url?></td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_ADVERTS_TYPES_EDIT)): ?>
            <a href="<?=base_url()?>admin/adverts/types_edit/<?=$adverts_types_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php if($adverts_types_item->visible==1): ?>
            <a href="<?=base_url()?>admin/adverts/types_hide/<?=$adverts_types_item->id?>" title="Скрыть" class="label notice">С</a>
            <?php else: ?>
            <a href="<?=base_url()?>admin/adverts/types_show/<?=$adverts_types_item->id?>" title="Показать" class="label notice">П</a>
            <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_ADVERTS_TYPES_DELETE)): ?>
            <a onclick="return confirm('Удалить тип - <?=$adverts_types_item->name?>?')" href="<?=base_url()?>admin/adverts/types_delete/<?=$adverts_types_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                2: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_ADVERTS_TYPES_ADD)): ?>
<a href="<?=base_url()?>admin/adverts/types_add" class="btn">Добавить тип</a><br/><br/>
<?php endif; ?>

<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Типов объявлений ещё не существует. Создайте новый.</td>
<td class="red-right"><a class="close-red"><img src="<?=sysRes()?>images/table/icon_close_red.gif"   alt="" /></a></td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>