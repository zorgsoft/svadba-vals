<script type="text/javascript">
$(function() {
    $('#name').change(function(){
	var a_name = $('#name').val();
	var cct = $("input[name=ci_csrf_token]").val();
	var html = $.ajax({
	    type: "POST",
	    url: "<?= base_url(); ?>admin/ajax_ruslat",
	    data: ({string_text : a_name, 'ci_csrf_token': cct}),
	    dataType: "html",
	    async: false
	    }).responseText;
	$('#url').val(html);
	});
    });
</script>
<?=validation_errors(
    '<div id="message-red"><table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td class="red-left">',
    '</td><td class="red-right"><a class="close-red"><img src="'.sysRes().'images/table/icon_close_red.gif"   alt="" /></a></td></tr></table></div>'
    )?>

<?=form_open('admin/firms/cat_edit/'.$firms_cat->id)?>
<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(1);'>Основные данные раздела:</h3>
<div id="page_1"> <!-- Основные параметры -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th align="right" width="200px"><label for="name">Название раздела:</label></th>
        <td width="450px"><input name="name" id="name" type="text" class="span11" value="<?=set_value('name', $firms_cat->name)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
    <tr>
        <th><label for="url">Ссылка:</label></th>
        <td><input name="url" id="url" type="text" class="span11" value="<?=set_value('url', $firms_cat->url)?>" /></td>
        <td>
            <div class="label warning">Обязательное поле.</div>
        </td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(2);'>Мета данные:</h3>
<div id="page_2" style="display: none;"> <!-- Meta данные -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th><label for="meta_title">Meta Title:</label></th>
        <td><input name="meta_title" id="meta_title" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_title', $firms_cat->meta_title)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_description">Meta Description:</label></th>
        <td><input name="meta_description" id="meta_description" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_description', $firms_cat->meta_description)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="meta_keywords">Meta Keywords:</label></th>
        <td><input name="meta_keywords" id="meta_keywords" type="text" class="span11" style="width: 450px;" value="<?=set_value('meta_keywords', $firms_cat->meta_keywords)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>


<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(3);'>Тексты раздела:</h3>
<div id="page_3"> <!-- Текст -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th valign="top"><label for="description">Краткое содержание:</label></th>
	<td>
	    <textarea name="description" id="description" rows="6" cols="120" class="xxlarge"><?=set_value('description', $firms_cat->description)?></textarea>
	    <?=Editor::CKEditor('description', '100%', '150')?>
	</td>
	<td></td>
    </tr>
    <tr>
        <th valign="top"><label for="a_content">Полный текст:</label></th>
	<td>
	    <textarea name="a_content" id="a_content" rows="12" cols="120" class="xxlarge"><?=set_value('a_content', $firms_cat->content)?></textarea>
	    <?=Editor::CKEditor('a_content')?>
	</td>
	<td></td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(4);'>Связи:</h3>
<div id="page_4" style="display: none;"> <!-- Связи -->
<table id="id-form_1" width="100%" class="z_form">
    <tr>
        <th><label for="parent_id">Раздел родитель:</label></th>
        <td>
	    <select name="parent_id" id="parent_id">
		<option value="">Нет</option>
		<?php if($firms_cat_list->count()>0): ?>
		<?php foreach($firms_cat_list as $firms_cat_item): ?>
		<option value="<?=$firms_cat_item->id?>" <?=set_select('parent_id', $firms_cat_item->id, $firms_cat->parent_id==$firms_cat_item->id ? TRUE : FALSE)?>><?=$firms_cat_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="gallery_id">Фото галерея:</label></th>
        <td>
	    <select name="gallery_id" id="gallery_id">
		<option value="">Нет</option>
		<?php if($gallerys_list->count()>0): ?>
		<?php foreach($gallerys_list as $gallery_item): ?>
		<option value="<?=$gallery_item->id?>" <?=set_select('gallery_id', $gallery_item->id, $firms_cat->gallery_id==$gallery_item->id ? TRUE : FALSE)?>><?=$gallery_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="gallery_id">Банер:</label></th>
        <td>
	    <select name="banner_id" id="banner_id">
		<option value="">Нет</option>
		<?php if($banners_list->count()>0): ?>
		<?php foreach($banners_list as $banner_item): ?>
		<option value="<?=$banner_item->id?>" <?=set_select('banner_id', $banner_item->id, $firms_cat->banner_id==$banner_item->id ? TRUE : FALSE)?>><?=$banner_item->name?></option>
		<?php endforeach; ?>
		<?php endif; ?>
	    </select>
	</td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<h3 class="tpl_h" title="Нажмите что бы показать или скрыть" onclick='javascript: pageShow(5);'>Видимость:</h3>
<div id="page_5" style="display: none;"> <!-- Видиость -->
<table id="id-form_2"  width="100%" class="z_form">
    <tr>
        <th><label for="visible">Видимый:</label></th>
        <td><input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', $firms_cat->visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="zindex">Очередь (zIndex):</label></th>
        <td><input name="zindex" id="zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('zindex', $firms_cat->zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_name">Название для меню:</label></th>
        <td><input name="menu_name" id="menu_name" type="text" class="span11" style="width: 450px;" value="<?=set_value('menu_name', $firms_cat->menu_name)?>" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_visible">Отображать в меню:</label></th>
        <td><input name="menu_visible" id="menu_visible" type="checkbox" <?=set_checkbox('menu_visible', 'checked', $firms_cat->menu_visible)?>/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th><label for="menu_zindex">Очередь в меню (zIndex):</label></th>
        <td><input name="menu_zindex" id="menu_zindex" type="text" class="span11" style="width: 50px;" value="<?=set_value('menu_zindex', $firms_cat->menu_zindex)?>" /></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>
<input type="submit" class="btn primary" value="Сохранить" /><br/><br/>
<?=form_close()?>

<script type="text/javascript">
    function pageShow(page){
        $('#page_'+page).toggle();
    };
</script>