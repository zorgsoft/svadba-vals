<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?=$title?></title>
<link rel="stylesheet" href="<?=sysRes()?>css/screen.css" type="text/css" media="screen" title="default" />
<link rel="stylesheet" href="<?=sysRes()?>css/datePicker.css" type="text/css" media="screen" title="default" />
<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="<?=sysRes()?>css/pro_dropline_ie.css" />
<![endif]-->

<!--  jquery core -->
<script src="<?=sysRes()?>js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>

<!--  checkbox styling script -->
<script src="<?=sysRes()?>js/jquery/ui.core.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/ui.checkbox.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/jquery.bind.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/jquery.tablesorter.min.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$('input').checkBox();
	$('#toggle-all').click(function(){
 	$('#toggle-all').toggleClass('toggle-checked');
	$('#mainform input[type=checkbox]').checkBox('toggle');
	return false;
	});
});
</script>  

<![if !IE 7]>

<!--  styled select box script version 1 -->
<script src="<?=sysRes()?>js/jquery/jquery.selectbox-0.5.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect').selectbox({ inputClass: "selectbox_styled" });
});
</script>
 

<![endif]>

<!--  styled select box script version 2 --> 
<script src="<?=sysRes()?>js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
	$('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
});
</script>

<!--  styled select box script version 3 --> 
<script src="<?=sysRes()?>js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" });
});
</script>

<!--  styled file upload script --> 
<script src="<?=sysRes()?>js/jquery/jquery.filestyle.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
  $(function() {
      $("input.file_1").filestyle({ 
          image: "images/forms/choose-file.gif",
          imageheight : 21,
          imagewidth : 78,
          width : 310
      });
  });
</script>

<!-- Custom jquery scripts -->
<script src="<?=sysRes()?>js/jquery/custom_jquery.js" type="text/javascript"></script>
 
<!-- Tooltips -->
<script src="<?=sysRes()?>js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/jquery.dimensions.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	$('a.info-tooltip ').tooltip({
		track: true,
		delay: 0,
		fixPNG: true, 
		showURL: false,
		showBody: " - ",
		top: -35,
		left: 5
	});
});
</script> 


<!--  date picker script -->
<link rel="stylesheet" href="<?=sysRes()?>css/datePicker.css" type="text/css" />
<script src="<?=sysRes()?>js/jquery/date.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
        $(function()
{

// initialise the "Select date" link
$('#date-pick')
	.datePicker(
		// associate the link with a date picker
		{
			createButton:false,
			startDate:'01/01/2005',
			endDate:'31/12/2020'
		}
	).bind(
		// when the link is clicked display the date picker
		'click',
		function()
		{
			updateSelects($(this).dpGetSelected()[0]);
			$(this).dpDisplay();
			return false;
		}
	).bind(
		// when a date is selected update the SELECTs
		'dateSelected',
		function(e, selectedDate, $td, state)
		{
			updateSelects(selectedDate);
		}
	).bind(
		'dpClosed',
		function(e, selected)
		{
			updateSelects(selected[0]);
		}
	);
	
var updateSelects = function (selectedDate)
{
	var selectedDate = new Date(selectedDate);
	$('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
	$('#m option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');
	$('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
}
// listen for when the selects are changed and update the picker
$('#d, #m, #y')
	.bind(
		'change',
		function()
		{
			var d = new Date(
						$('#y').val(),
						$('#m').val()-1,
						$('#d').val()
					);
			$('#date-pick').dpSetSelected(d.asString());
		}
	);

// default the position of the selects to today
var today = new Date();
updateSelects(today.getTime());

// and update the datePicker to reflect it...
$('#d').trigger('change');
});
</script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="<?=sysRes()?>js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
});
</script>
</head>
<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<div id="logo">
	<a href=""><img src="<?=sysRes()?>images/shared/logo.png" width="156" height="40" alt="" /></a>
	</div>
	<!-- end logo -->
	
	<!--  start top-search -->
	<div id="top-search">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><input type="text" value="Поиск" onblur="if (this.value=='') { this.value='Поиск'; }" onfocus="if (this.value=='Поиск') { this.value=''; }" class="top-search-inp" /></td>
		<td>
		<select  class="styledselect">
			<option value=""> Везде</option>
			<option value=""> Статьи</option>
			<option value=""> Фото</option>
			<option value="">Пользователи</option>
			<option value="">Новости</option>
		</select> 
		</td>
		<td>
		<input type="image" src="<?=sysRes()?>images/shared/top_search_btn.gif"  />
		</td>
		</tr>
		</table>
	</div>
 	<!--  end top-search -->
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer"> 

		<!-- start nav-right -->
		<div id="nav-right">
		
			<div class="nav-divider">&nbsp;</div>
			<div class="showhide-account"><img src="<?=sysRes()?>images/shared/nav/nav_myaccount.gif" width="93" height="14" alt="" /></div>
			<div class="nav-divider">&nbsp;</div>
			<a href="<?=base_url()?>logout" id="logout"><img src="<?=sysRes()?>images/shared/nav/nav_logout.gif" width="64" height="14" alt="" /></a>
			<div class="clear">&nbsp;</div>
		
			<!--  start account-content -->	
			<div class="account-content">
			<div class="account-drop-inner">
				<a href="" id="acc-settings">Настройки</a>
				<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>
				<a href="" id="acc-details">Информация </a>
				<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>
				<a href="" id="acc-inbox">Письма</a>
				<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>
				<a href="" id="acc-stats">Статистика</a> 
			</div>
			</div>
			<!--  end account-content -->
		
		</div>
		<!-- end nav-right -->


		<!--  start nav -->
		<div class="nav">
		<div class="table">
		<?php
			// Пункты меню
			// Сначала идёт описание основного меню, в виде
			// Название, ссылка, название роли для доступа, название выбранного меню, название выбранного подменю
			// Затем, идут строки описания подменю в виде
			// Название меню|ссылка|роль
			// Параметры подменю разделяются символом прямой черты |
			// Роль должна быть обязательно указанна, если затрудняетесь,
			// то ставте стандартную роль admin
			// Так же вы можете использовать константы для прав, которые находятся в файле
			// /application/config/constants.php
		?>
		<?=Menu::adminMenu('Начало', base_url().'admin', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Добро пожаловать|'.base_url().'admin|'.R_USER_ADMIN,
			'Новости|'.base_url().'admin/news|'.R_USER_ADMIN,
			'Подробности 3|#nogo|'.R_USER_ADMIN,
		))?>
		
		<div class="nav-divider">&nbsp;</div>
		
		<?=Menu::adminMenu('Статьи', base_url().'admin/articles', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Список статей|'.base_url().'admin/articles|'.R_USER_ADMIN,
			'Добавить статью|'.base_url().'admin/articles/add|'.R_USER_ARTICLES_ADD,
			'Группы статей|'.base_url().'admin/articles/groups|'.R_USER_ARTICLES_GROUPS,
			'Добавить группу|'.base_url().'admin/articles/groups_add|'.R_USER_ARTICLES_GROUPS_ADD,
		))?>
                
                <div class="nav-divider">&nbsp;</div>
                
                <?=Menu::adminMenu('Фирмы', base_url().'admin/firms', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Разделы|'.base_url().'admin/firms/cat|'.R_USER_FIRMS_GROUPS,
			'Фирмы|'.base_url().'admin/firms|'.R_USER_FIRMS
		))?>
		
                <?=Menu::adminMenu('Объявления', base_url().'admin/adverts', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Разделы|'.base_url().'admin/adverts/cat|'.R_USER_ADMIN,
			'Типы|'.base_url().'admin/adverts/types|'.R_USER_ADMIN,
			'Объявления|'.base_url().'admin/adverts|'.R_USER_ADMIN
		))?>
                
                 <div class="nav-divider">&nbsp;</div>
                
                <?=Menu::adminMenu('Галлереи', base_url().'admin/gallery', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Список галлерей|'.base_url().'admin/gallery|'.R_USER_FIRMS_GROUPS,
			'Добавить галлерею|'.base_url().'admin/gallery/add|'.R_USER_FIRMS
		))?>
		
		<div class="nav-divider">&nbsp;</div>
                
		<?=Menu::adminMenu('Пользователи', base_url().'admin/users', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Список пользователей|'.base_url().'admin/users|'.R_USER_ADMIN,
			'Добавить пользователя|'.base_url().'admin/users/add|'.R_USER_ADMIN,
			'Групы доступа|'.base_url().'admin/access|'.HC_SUPER_ADMIN_NAME,
			'Добавить групу|'.base_url().'admin/access/add|'.HC_SUPER_ADMIN_NAME,
		))?>
                
                <div class="nav-divider">&nbsp;</div>

		<?=Menu::adminMenu('Настройки', 'admin/options', R_USER_ADMIN, $menuSelected, $subSelected, array(
			'Настройка сайта|'.base_url().'admin/options|'.R_USER_ADMIN,
			'Шаблоны|'.base_url().'admin/templates|'.R_USER_ADMIN,
			'Права доступа|'.base_url().'admin/options/access|'.HC_SUPER_ADMIN_NAME,
		))?>
		
		<div class="nav-divider">&nbsp;</div>
		
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
		</div>
		<!--  start nav -->

</div>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

  <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>{page_head}</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=sysRes()?>images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=sysRes()?>images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
				{content}
			</div>
			<!--  end table-content  -->
	
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
<!-- <div id="footer-pad">&nbsp;</div> -->
	<!--  start footer-left -->
	<div id="footer-left">
	HardKor CMS &copy; Copyright ZoRg Soft <a href="http://agarkov.com">agarkov.org</a>. All rights reserved.</div>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
 
</body>
</html>