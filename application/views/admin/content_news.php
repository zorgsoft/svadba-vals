<h3>Список новостей</h3>
<?php if(Auth::canAccess(R_USER_NEWS_ADD)): ?>
<a href="<?=base_url()?>admin/news/add" class="btn">Добавить новость</a><br/><br/>
<?php endif; ?>
<?php if($news_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th class="table-header-repeat line-left minwidth-1">Название</th>
        <th class="table-header-repeat line-left minwidth-1">Ссылка Url</th>
        <th class="table-header-repeat line-left"><span class="table-header-text">Добавили</span></th>
        <th class="table-header-repeat line-left"><span class="table-header-text">Изменили</span></th>
        <th class="table-header-options line-left"><span class="table-header-text">Действия</span></th>
    </tr>
    </thead> 
    <?php foreach($news_list as $news_item): ?>
    <tr>
        <td><?=$news_item->name?></td>
        <td><?=$news_item->url?></td>
        <td><?=date('d-m-Y H:i:s', strtotime($news_item->created_at))?></td>
        <td><?=date('d-m-Y H:i:s', strtotime($news_item->updated_at))?></td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_NEWS_EDIT)): ?>
            <a href="<?=base_url()?>admin/news/edit/<?=$news_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php if($news_item->visible==1): ?>
            <a href="<?=base_url()?>admin/news/hide/<?=$news_item->id?>" title="Скрыть" class="label notice">С</a>
            <?php else: ?>
            <a href="<?=base_url()?>admin/news/show/<?=$news_item->id?>" title="Показать" class="label notice">П</a>
            <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_NEWS_DELETE)): ?>
            <a onclick="return confirm('Удалить новость - <?=$news_item->name?>?')" href="<?=base_url()?>admin/news/delete/<?=$news_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                0: { sorter: false },
                5: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_NEWS_ADD)): ?>
<a href="<?=base_url()?>admin/news/add" class="btn">Добавить новость</a><br/><br/>
<?php endif; ?>
<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Новостей ещё не существует. Создайте новую.</td>
<td class="red-right"><a class="close-red"><img src="<?=sysRes()?>images/table/icon_close_red.gif"   alt="" /></a></td>
</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>