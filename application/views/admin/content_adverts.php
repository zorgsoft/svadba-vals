<h3>Список объявлений</h3>
<?php if(Auth::canAccess(R_USER_ADVERTS_ADD)): ?>
<a href="<?=base_url()?>admin/adverts/add" class="btn">Добавить объявление</a><br/><br/>
<?php endif; ?>
<?php if($adverts_list->count()>0): ?>
<form id="mainform" action="">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="zebra-striped">
    <thead> 
    <tr>
        <th>Название</th>
        <th>Ссылка Url</th>
        <th>Пользователь</th>
        <th>Добавили</th>
        <th>Изменили</th>
        <th>Оплата</th>
        <th>Действия</th>
    </tr>
    </thead> 
    <?php foreach($adverts_list as $advert_item): ?>
    <tr>
        <td><?=$advert_item->name?></td>
        <td><?=$advert_item->url?></td>
        <td><a href="<?=base_url()?>admin/user/<?=$advert_item->User->id?>"><?=$advert_item->User->name?></a></td>
        <td><?=date('h:i d-m-Y', strtotime($advert_item->created_at))?></td>
        <td><?=date('h:i d-m-Y', strtotime($advert_item->updated_at))?></td>
        <td>
            <?php if($advert_item->paid == 1): ?>
            <?=date('d-m-Y', strtotime($advert_item->paid_to))?>
            <?php else: ?>
            Нет
            <?php endif; ?>
        </td>
        <td width="60px" style="text-align: center;">
            <?php if(Auth::canAccess(R_USER_ADVERTS_EDIT)): ?>
            <a href="<?=base_url()?>admin/adverts/edit/<?=$advert_item->id?>" title="Редактировать" class="label success">Р</a>
            <?php if($advert_item->visible==1): ?>
            <a href="<?=base_url()?>admin/adverts/hide/<?=$advert_item->id?>" title="Скрыть" class="label notice">С</a>
            <?php else: ?>
            <a href="<?=base_url()?>admin/adverts/show/<?=$advert_item->id?>" title="Показать" class="label notice">П</a>
            <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::canAccess(R_USER_ADVERTS_DELETE)): ?>
            <a onclick="return confirm('Удалить объявление - <?=$advert_item->name?>?')" href="<?=base_url()?>admin/adverts/delete/<?=$advert_item->id?>" title="Удалить" class="label important">У</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</form>

<!--  start tablesorter options ............................................... -->
<script type="text/javascript"> 
$(document).ready(function() 
    { 
        $("#product-table").tablesorter({
             headers: {
                6: { sorter: false }
             }
            }); 
    } 
);
</script>
<!--  end tablesorter options ............................................... -->
<?php if(Auth::canAccess(R_USER_ADVERTS_ADD)): ?>
<a href="<?=base_url()?>admin/adverts/add" class="btn">Добавить объявление</a><br/><br/>
<?php endif; ?>
<?php else: ?>
<!--  start message-red -->
<div id="message-red">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="red-left">Объявлений ещё не существует. Создайте новое.</td>

</tr>
</table>
</div>
<!--  end message-red -->
<?php endif; ?>