
                <!-- CONTENT PANEL START -->
                <script>
                   $(function() {
                    $('#name').change(function(){
                        var a_name = $('#name').val();
                        var cct = $("input[name=ci_csrf_token]").val();
                        var html = $.ajax({
                            type: "POST",
                            url: "<?= base_url(); ?>admin/ajax_ruslat",
                            data: ({string_text : a_name, 'ci_csrf_token': cct}),
                            dataType: "html",
                            async: false
                            }).responseText;
                        $('#url').val(html);
                        });
                    });
                </script>
                <h2>Добавление новой галереи</h2>
                <div class="notification information png_bg">
                    <a href="#" class="close"><img src="<?=base_url()?>images/icons/cross_grey_small.png" title="Закрыть эту подсказку" alt="Закрыть" /></a>
                    <div>
                        <h4>Галерея:</h4>
                        При добавлении или изменении галереи, обязательно укзаывайте <strong>URL</strong> галереи.<br/>Не забывайте указывать zindex, это указывает на порядок вывода галереи, чем выше номер zindex, тем дальше она будет при выводе.
                    </div>
                </div>
                <div class="clear"></div>
                <div class="content-box">
                    <div class="content-box-header">
                        <h3>Основные параметры галереи</h3>
                    </div>
                    <div class="content-box-content">
                        <?=validation_errors(
                                '<div class="notification error png_bg"><a href="#" class="close"><img src="'.base_url().'images/icons/cross_grey_small.png" title="Закрыть это сообщение" alt="close" /></a><div>',
                                '</div></div>');?>
                        <?=form_open('admin/gallery/add')?>
                        <fieldset>
                        <table>
                            <tbody>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="name">Название галереи</label>
                                    </td>
                                    <td>
                                        <input name="name" id="name" type="text" class="text-input medium-input" value="<?=set_value('name')?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="meta_title">Мета Title</label>
                                    </td>
                                    <td>
                                        <input name="meta_title" id="meta_title" type="text" class="text-input medium-input" value="<?=set_value('meta_title')?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="meta_description">Мета Description</label>
                                    </td>
                                    <td>
                                        <input name="meta_description" id="meta_description" type="text" class="text-input medium-input" value="<?=set_value('meta_description')?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="meta_keywords">Мета Keywords</label>
                                    </td>
                                    <td>
                                        <input name="meta_keywords" id="meta_keywords" type="text" class="text-input medium-input" value="<?=set_value('meta_keywords')?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="description">Краткое описание</label>
                                    </td>
                                    <td>
                                        <textarea name="description" id="description" class="text-input textarea wysiwyg" cols="64" rows="15"><?=set_value('description')?></textarea>
                                        <?//=ckeditor('description', 200);?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="content">Текст галлереи</label>
                                    </td>
                                    <td>
                                        <textarea name="content" id="content" class="text-input textarea wysiwyg" cols="64" rows="15"><?=set_value('content')?></textarea>
                                        <?//=ckeditor('content', 300);?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="url">URL ссылка этой галереи</label>
                                    </td>
                                    <td>
                                        <input name="url" id="url" type="text" class="text-input medium-input" value="<?=set_value('url')?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="zindex">Позиция галереи (zindex)</label>
                                    </td>
                                    <td>
                                        <input name="zindex" id="zindex" type="text" class="text-input small-input" value="<?=set_value('zindex', '0')?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="200px">
                                        <label for="visible">Видимая</label>
                                    </td>
                                    <td>
                                        <input name="visible" id="visible" type="checkbox" <?=set_checkbox('visible', 'checked', TRUE)?>/>
                                    </td>
                                </tr>
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td rowspan="2">
                                        <input class="button" type="submit" value="Продолжить добавление новой галереи... &rsaquo;"/>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        </fieldset>
                        <?=form_close()?>
                    </div>
                </div>

                <div class="clear"></div>
                <!-- CONTENT PANEL END -->

               