<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Вход</title>
<link rel="stylesheet" href="<?=sysRes()?>css/screen.css" type="text/css" media="screen" title="default" />
<!--  jquery core -->
<script src="<?=sysRes()?>js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>

<!-- Custom jquery scripts -->
<script src="<?=sysRes()?>js/jquery/custom_jquery.js" type="text/javascript"></script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="<?=sysRes()?>js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
});
</script>
</head>
<body id="login-bg"> 
 
<!-- Start: login-holder -->
<div id="login-holder">

	<!-- start logo -->
	<div id="logo-login">
		<a href="<?=base_url()?>"><img src="<?=sysRes()?>images/shared/logo.png" width="156" height="40" alt="" /></a>
	</div>
	<!-- end logo -->
	
	<div class="clear"></div>
	
	<!--  start loginbox ................................................................................. -->
	<div id="loginbox">
	
	<!--  start login-inner -->
	<div id="login-inner">
		
		<?=form_open('login')?>
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th></th>
			<td>
				<?=validation_errors()?><br/>
				<?=$error_message?>
			</td>
		</tr>
		<tr>
			<th><?=lang('Username')?></th>
			<td><input name="login" type="text" value="" class="login-inp" /></td>
		</tr>
		<tr>
			<th><?=lang('Password')?></th>
			<td><input name="password" type="password" value="" class="login-inp" /></td>
		</tr>
		<tr>
			<th></th>
			<td><input type="submit" class="submit-login" value="<?=lang('Login')?>" /></td>
		</tr>
		</table>
		<?=form_close()?>
	</div>
 	<!--  end login-inner -->
	<div class="clear"></div>
	<a href="" class="forgot-pwd"><?=lang('Forgot Password?')?></a>
 </div>
 <!--  end loginbox -->
 
	<!--  start forgotbox ................................................................................... -->
	<div id="forgotbox">
		<div id="forgotbox-text"><?=lang("Please send us your email and we'll reset your password.")?></div>
		<!--  start forgot-inner -->
		<div id="forgot-inner">
		<?=form_open('restorepassword')?>
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th><?=lang('Email address:')?></th>
			<td><input name="email" type="text" value=""   class="login-inp" /></td>
		</tr>
		<tr>
			<th> </th>
			<td><input type="button" class="submit-login"  value="<?=lang('Send')?>" /></td>
		</tr>
		</table>
		<?=form_close()?>
		</div>
		<!--  end forgot-inner -->
		<div class="clear"></div>
		<a href="" class="back-login"><?=lang('Back to login')?></a>
	</div>
	<!--  end forgotbox -->

</div>
<!-- End: login-holder -->
</body>
</html>