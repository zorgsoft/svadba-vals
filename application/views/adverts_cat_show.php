{tpl_header}
{tpl_left}
{tpl_right}



<!--kroshki start-->
<div class="kroshki">
<?=Breadcrumbs::AdvertsCat($advertsCatData->id, TRUE)?>
</div>
<!--kroshki end-->

<div class="line"></div>

<h1><?=$advertsCatData->name?></h1>
<p><?=$advertsCatData->content?></p>


<!--katalog start-->
<?php if($advertsCatData->Childs->count() > 0): ?>
<h2>Подразделы:</h2>
<?php foreach($advertsCatData->Childs as $advertsCatCildItem): ?>
<?php if($advertsCatCildItem->visible == 1): ?>
<div class="katalog_count">
<div class="katalog_text">
<a class="katalog_zag" href="<?=base_url()?>adverts/<?=$advertsCatCildItem->url?>"><?=$advertsCatCildItem->name?></a>
<br>
<?=character_limiter(strip_tags($advertsCatCildItem->content), 128)?>
</div>
</div>
<!--katalog end-->
<div class="line"></div>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>

<?php if($advertsCatList->count() > 0): ?>
<?php foreach($advertsCatList as $catAdvertItem): ?>
<?php if($catAdvertItem->paid_type == 2 and $catAdvertItem->paid == 1):  ?>
<table width="100%" border="1" style="background-color: rgb(200, 250, 200);" title="VIP">
<?php else: ?>
<table width="100%" border="1">
<?php endif; ?>
    <tr>
        <td><?=$catAdvertItem->name?></td>
        <td><?=date('d-m-Y H:i:s', strtotime($catAdvertItem->updated_at))?></td>
    </tr>
    <tr>
        <td>Тип: <a href="<?=base_url()?>adverts/type/<?=$catAdvertItem->Type->url?>"><?=$catAdvertItem->Type->name?></a></td>
        <td>Цена: <?=$catAdvertItem->price?></td>
    </tr>
    <tr>
        <td colspan="2">
            Город: <?php if($catAdvertItem->city_id > 0): ?>
            <?=$catAdvertItem->City->name?>
            <?php elseif(trim($catAdvertItem->author_city) != ''): ?>
            <?=$catAdvertItem->author_city?>
            <?php else: ?>
            Нет
            <?php endif; ?>
            <br>
            <?=character_limiter(strip_tags($catAdvertItem->content), 124)?>
        </td>
    </tr>
    <tr>
        <td title="Автор"><?=$catAdvertItem->author_name?></td>
        <td><a href="<?=base_url()?>adverts/<?=$advertsCatData->url?>/<?=$catAdvertItem->url?>">Подробнее...</a></td>
    </tr>
</table>
<br>
<?php endforeach; ?>
<?php endif; ?>

<a href="<?=base_url()?>adverts/add">Добавить объявление</a>
<br><br>

<!--main end-->

{tpl_futter}