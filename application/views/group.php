{tpl_header}
{tpl_banners}
{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">
<a class="kroshki_link" href="<?=base_url()?>">Главная</a>
    
<h1><?=$content_group->name?></h1>

<p><?=$content_group->content?></p>
</div>
<!--main text end-->

<?php if($content_group->Articles->count() > 0): ?>
<br>
<strong>Страницы:</strong><br>
<ul>
    <?php foreach($content_group->Articles as $articleChildItem): ?>
    <li><a href="<?=base_url()?>article/<?=$articleChildItem->url?>"><?=$articleChildItem->name?></a></li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>

{tpl_futter}