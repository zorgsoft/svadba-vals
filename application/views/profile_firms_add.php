{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">

<h1>Добавление новой фирмы:</h1>
<?=validation_errors('<div style="color: red;">','</div>')?>
<br/>

<script type="text/javascript" >
   $(document).ready(function() {
        $("#description").cleditor({
            width: "100%",
            height: 150
            });
        $("#a_content").cleditor({
            width: "100%",
            height: 350
            });
        $("#contacts").cleditor({
            width: "100%",
            height: 150
            });
   });
</script>
<?=form_open(base_url().'profile/firm/add')?>
<label for="name">Название фирмы:</label>
<input type="text" name="name" id="name" value="<?=set_value('name')?>" /><br/><br/>
<label for="catalog_id">Раздел: </label>
<select name="catalog_id" id="catalog_id">
<?php foreach($tpl_cat_list as $firmCatItem): ?>
<option value="<?=$firmCatItem->id?>"><?=$firmCatItem->name?></option>
<?php endforeach; ?>
</select>
<br/><br/>
<label for="description">Краткое описание:</label><br/>
<textarea name="description" id="description"><?=set_value('description')?></textarea><br/>
<label for="a_content">Полное описание фирмы:</label><br/>
<textarea name="a_content" id="a_content"><?=set_value('a_content')?></textarea>
<small>(Для добавления логотипа Вашей вирмы, в самом начале полного описания вставте ссылку на Ваш логотип, администратор после просмотра и одобрения Вашей фирмы, добавит его.)</small>
<br/><br/>
<label for="contacts">Контактные данные:</label><br/>
<textarea name="contacts" id="contacts"><?=set_value('contacts')?></textarea>
<small>(Введите ваши контактные данные, e-mail, телефоны, Ф.И.О. контактоного лица, а также адрес и город)</small>
<br/><br/>
<input type="submit" value="Добавить"/>

<?=form_close()?>
</div>

<!--main text end-->

{tpl_futter}