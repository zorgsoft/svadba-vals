{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">

<h1>Запрос на изменение или удаление статьи:</h1>
<?=validation_errors('<div style="color: red;">','</div>')?>
<br/>

<script type="text/javascript" >
   $(document).ready(function() {
        $("#a_content").cleditor({
            width: "100%",
            height: 350
            });
   });
</script>
<p>
    Запрос на изменение или удаление статьи <strong>"<?=$articleData->name?>"</strong>.<br><br>
    <small>Оставте своё сообщение, о том, что хотите сделать, если удалить, то так и напишите, если изменить какие
    либо данные статьи, то в сообщаении обязательно укажите что заменить.<br>
    Писать название стаьти или её ссылку в Вашем сообщении нет необходимости.<br>
    Поля (Фамилия И.О., E-Mail, Телефон) не обязательны для заполнения, они нужны для того, что бы указать другое контактное лицо с которым может связаться администратор по поводу запрошенных вами изменений.<br><br>
    После получения и просмотра Вашей заявки, администратор свяжется с Вами.</small><br><br>
</p>
<?=form_open(base_url().'profile/article/request/' . $articleData->id)?>
<label for="subject">Тема запроса:</label>
<input type="text" name="subject" id="subject" value="<?=set_value('subject', $articleData->name)?>" /><br/><br/>
<?php $curUserData = Auth::user(); ?>
<label for="name">Фамилия И.О.:</label>
<input type="text" name="name" id="name" value="<?=set_value('name', $curUserData->name)?>" /><br/><br/>
<label for="email">E-mail:</label>
<input type="text" name="email" id="email" value="<?=set_value('email', $curUserData->email)?>" /><br/><br/>
<label for="phone">Телефон:</label>
<input type="text" name="phone" id="phone" value="<?=set_value('phone', $curUserData->phone)?>" /><br/><br/>
<label for="a_content">Ваше сообщение администратору:</label><br/>
<textarea name="a_content" id="a_content"><?=set_value('a_content')?></textarea><br/>
<br/>
<input type="submit" value="Отправить"/>

<?=form_close()?>
</div>

<!--main text end-->

{tpl_futter}