{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">

    <h1>Личный кабинет:</h1>
    <br/>
    <strong style="color: green; cursor: pointer;" id="fl_data" onClick="javascript: $('#fl_data').hide();"><u><?=$this->session->flashdata('profile_flash')?></u><br/><br/></strong>
    <?php $userDataLoggedIn = Auth::user(); ?>
    Здравствуйте, <strong><?= $userDataLoggedIn->name ?></strong>.<br>
    Это Ваш личный кабинет. Тут Вы сможете добавить или изменить свою фирму, свадьбу или объявления. Так же изменить настройки своего профиля.
    <br><br>
    <small>Для изменений данных Вашей добавленной (фирмы, статьи, объявления или свадьбы) или удаления, отправьте заявку, наш менеджер свяжется с Вами в ближайшее время.</small>
    <ul style="list-style-type: none;">
        <li>Фирмы:<br>
            <?php if ($userFirmsList != NULL and $userFirmsList->count() > 0): ?>
                <ul style="list-style-type: none;">
                    <?php foreach ($userFirmsList as $userFirmItem): ?>
                        <li><?=Menu::visibleImage($userFirmItem->visible)?> <?= $userFirmItem->name ?> <a  title="Оставить заявку для удаления или изменения" href="<?= base_url() ?>profile/firm/request/<?= $userFirmItem->id ?>"><img style="vertical-align:text-bottom;" alt="Заявка" src="<?=base_url()?>resources/sys/images/16x16/order.png"></a></li>
                    <?php endforeach; ?>
                    <li><strong><a href="<?= base_url() ?>profile/firm/add">Добавить</a></strong></li>
                </ul>
            <?php else: ?>
                <strong>Нет фирмы - <a href="<?= base_url() ?>profile/firm/add">добавить</a></strong>
            <?php endif; ?>
        </li>
        <li>Свадьбы (и фото):
            <?php if ($userWeddingsList != NULL and $userWeddingsList->count() > 0): ?>
            <ul style="list-style-type: none;">
                    <?php foreach ($userWeddingsList as $userWeddingItem): ?>
                        <li><?=Menu::visibleImage($userWeddingItem->visible)?> <?= $userWeddingItem->name ?> <a title="Оставить заявку для удаления или изменения" href="<?= base_url() ?>profile/weddings/request/<?= $userWeddingItem->id ?>"><img style="vertical-align:text-bottom;" alt="Заявка" src="<?=base_url()?>resources/sys/images/16x16/order.png"></a></li>
                    <?php endforeach; ?>
                    <li><strong><a href="<?= base_url() ?>profile/wedding/add">Добавить</a></strong></li>
            </ul>
            <?php else: ?>
                <strong>Нет свадьбы - <a href="<?= base_url() ?>profile/wedding/add">добавить</a></strong></li>
            <?php endif; ?>
        <li>Объявления:
            <?php if ($userAdvertsList != NULL and $userAdvertsList->count() > 0): ?>
                <ul style="list-style-type: none;">
                    <?php foreach ($userAdvertsList as $userAdvertItem): ?>
                        <li><?=Menu::visibleImage($userAdvertItem->visible)?> <?= $userAdvertItem->name ?> <a title="Оставить заявку для удаления или изменения" href="<?= base_url() ?>profile/adverts/request/<?= $userAdvertItem->id ?>"><img style="vertical-align:text-bottom;" alt="Заявка" src="<?=base_url()?>resources/sys/images/16x16/order.png"></a></li>
                    <?php endforeach; ?>
                    <li><strong><a href="<?= base_url() ?>profile/adverts/add">Добавить</a></strong></li>
                </ul>
            <?php else: ?>
                <strong>Нет объявлений - <a href="<?= base_url() ?>profile/adverts/add">добавить</a></strong></li>
        <?php endif; ?>
        <li>Статьи:
            <?php if ($userArticlesList != NULL and $userArticlesList->count() > 0): ?>
                <ul style="list-style-type: none;">
                    <?php foreach ($userArticlesList as $userArticleItem): ?>
                        <li><?=Menu::visibleImage($userArticleItem->visible)?> <?= $userArticleItem->name ?> <a title="Оставить заявку для удаления или изменения" href="<?= base_url() ?>profile/article/request/<?= $userArticleItem->id ?>"><img style="vertical-align:text-bottom;" alt="Заявка" src="<?=base_url()?>resources/sys/images/16x16/order.png"></a></li>
                    <?php endforeach; ?>
                    <li><strong><a href="<?= base_url() ?>profile/article/add">Добавить</a></strong></li>
                </ul>
            <?php else: ?>
                <strong>Нет статей - <a href="<?= base_url() ?>profile/article/add">добавить</a></strong>
            <?php endif; ?>
        </li>
    </ul>

    <div style="padding-left: 50px;">
    <a href="<?= base_url() ?>profile/edit"><img style="vertical-align:text-bottom;" alt="Профиль" src="<?=base_url()?>resources/sys/images/16x16/my-account.png"> Изменить профиль</a> 
    <?php if (Auth::canAccess(R_USER_ADMIN) == TRUE): ?>
    <br><a href="<?= base_url() ?>admin" style="color: red;"><img style="vertical-align:text-bottom;" alt="Панель администратора" src="<?=base_url()?>resources/sys/images/16x16/user.png"> Панель администратора</a> 
    <?php else: ?>
    <br><a href="<?= base_url() ?>profile/delete" style="color: red;"><img style="vertical-align:text-bottom;" alt="Удалить" src="<?=base_url()?>resources/sys/images/16x16/busy.png"> Удалить профиль</a> 
    <?php endif; ?>
    </div>
    
</div>
<!--main text end-->

{tpl_futter}