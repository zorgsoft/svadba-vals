{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">
<?=Breadcrumbs::Article($content_article->id, TRUE)?>
    
<h1><?=$content_article->name?></h1>

<p><?=$content_article->content?></p>
</div>
<!--main text end-->

<?php if($content_article->Childs->count() > 0): ?>
<br>
<strong>Так же смотрите:</strong><br>
<ul>
    <?php foreach($content_article->Childs as $articleChildItem): ?>
    <li><a href="<?=base_url()?>article/<?=$articleChildItem->url?>"><?=$articleChildItem->name?></a></li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>

{tpl_futter}