</div>
<div class="obj_right">

<div class="obj_right_count">
    <?php if(trim($content_firm->img_logo) == ''): ?>
    <img src="<?=base_url()?>resources/images/logo.png" width="200" height="140" class="obj_right_img">
    <?php else: ?>
    <img src="<?=base_url()?>resources/firmlogos/<?=$content_firm->img_logo?>" width="200" height="140" class="obj_right_img">
    <?php endif; ?>
<br><br>
<?php foreach($content_firm->Addresses as $firmAddressItem): ?>
<span class="obj_right_adr">Адрес:</span><br>
<?=$firmAddressItem->address?>
<br>
<span class="obj_right_adr">Контактные телефоны:</span><br>
<span class="obj_right_tel">
<?php $phonesArray = explode(',', $firmAddressItem->phones); ?>
<?php foreach($phonesArray as $phoneItem): ?>
<?=trim($phoneItem)?><br>
<?php endforeach; ?>
</span>
<br><br>
<span class="obj_right_adr">E-mail:</span><br>
<?php $emailArray = explode(',', $firmAddressItem->emails); ?>
<?php foreach($emailArray as $emailItem): ?>
<a href="mailto:<?=trim($emailItem)?>" class="obj_right_link"><?=trim($emailItem)?></a>
<?php endforeach; ?>
<br><br>
<span class="obj_right_adr">Сайт:</span> <a href="<?=$firmAddressItem->url?>" class="obj_right_link"><?=$firmAddressItem->url?></a>
<br>
<?php endforeach; ?>
</div>

<br><br>

<div class="obj_right_tags">
<span class="obj_right_adr1"><strong>Облако тегов</strong></span><br><br>
<?php $tagsArray = explode(',', $content_firm->tags); ?>
<?php foreach($tagsArray as $tagItem): ?>
<a href="<?=base_url()?>tag/<?=trim($tagItem)?>" class="obj_right_tags_link"><?=trim($tagItem)?></a>
<?php endforeach; ?>
</div>

<!--Menu start-->
<br>
<span class="obj_rub"><strong>Рубрики</strong></span><br><br>
<div id="left_menu1">
<ul id="menu1">
    <?php foreach($content_firm->Firms_cats as $firmCat): ?>
    <li class="dop1"><a href="<?=base_url()?>cat/<?=$firmCat->Firms_cat->url?>"><?=$firmCat->Firms_cat->name?></a></li>
    <?php endforeach; ?>
</ul>
</div>
<!--menu end-->

</div>
<!--right end-->
