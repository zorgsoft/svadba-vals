<!--konkyrs start-->
<div id="konkyrs">

<div class="konkyrs_foto"><img src="<?=base_url()?>resources/images/5.jpg" width="243" class="konkyrs_img"><br><a href="" class="konkyrs_link">Тальяна и Олег Щербаковы</a></div>

<div class="konkyrs_right">
<span class="article">Фотоконкурс</span><br>
<span class="konkyrs_dop">"Лучшая свадьба Севастополя"</span>
<br>
<a href=""><img src="<?=base_url()?>resources/images/dob_foto.jpg" border="0" vspace="5"></a>
<br>
<br>
<br>
<a href=""><img src="<?=base_url()?>resources/images/pozdr.jpg" border="0" vspace="0"></a>
</div>
<br class="clear">
</div>
<!--konkyrs end-->

<!--main text start-->
<div class="main_text">
<h1>О портале "Свадебный Вальс"</h1>

<p>Свадьба - одно из особых, знаменательных событий в жизни человека. В прежние времена к ней относились как к некому таинству, к которому готовились задолго, в котором принимали участие многие люди. Свадьбе предшествовал и сопровождал ее не один десяток ритуалов. В нынешний век все обстоит намного проще, нет былого размаха, утрачено значение древних традиций, редко встретишь осознанное отношение к семейным узам, и все же подготовка к свадьбе - дело хлопотное, которое может занять не один месяц. Само свадебное действо длится не больше двух дней, но, чтобы все шло гладко и чинно, необходимо продумать все до мелочей, учесть несметное количество нюансов.</p>

<p>Цель создания нашего свадебного портала как раз и заключается в том, чтобы ПОМОЧЬ Вам в ОРГАНИЗАЦИИ СВАДЬБЫ на должном уровне, чтобы облегчить и направить Ваш поиск, предоставив, по возможности, полную информацию и о свадебных фирмах в Севастополе, и об услугах, предоставляемых ими, и много другого интересного материала, связанного со свадьбой.</p>
</div>
<!--main text end-->



<!--Spec start-->
<div id="spec">
<h1 class="spec">Специальные предложения</h1>

<!--jenihy start-->
<div class="jenih">
<a href="" class="jenih_link">Жениху</a>
<div class="jenih_dop">Богатая коллекция свадебных и вечерних нарядов и самых разнообразных аксессуаров к ним, от зарубежных и отечественных производителей свадебной одежды BRIDALANE , TUTTO BENE , NITE TIME , PAPILIO, PREMIUM PALACE, ТОН и др. - на любой вкус и возможности. Широкий диапазон цен от 5 до 50 тыс. руб.</div>
</div>
<!--jenihy end-->

<!--neveste start-->
<div class="nevesta">
<a href="" class="nevesta_link">Невесте</a>
<div class="nevesta_dop">В день свадьбы прекрасной должна быть не только невеста, но и гости.
При комплексном обслуживании салон «Etual» предоставляет каждому СКИДКУ в 5%.
<div align="center"><img src="<?=base_url()?>resources/images/skidka.png"></div>
</div>
</div>
<!--neveste start-->
<br class="clear">
</div>
<!--Spec start-->


<br>
<!--Foto svadeb start-->
<div id="foto">

<h1 class="spec">Фотогалерея свадеб</h1>

<div class="foto">
<a href=""><img src="<?=base_url()?>resources/images/left.png" align="left" hspace="7"  vspace="46"></a>
<a href=""><img src="<?=base_url()?>resources/images/6.jpg" align="left" hspace="1"></a>
<a href=""><img src="<?=base_url()?>resources/images/7.jpg" align="left" hspace="1"></a>
<a href=""><img src="<?=base_url()?>resources/images/8.jpg" align="left" hspace="1"></a>
<a href=""><img src="<?=base_url()?>resources/images/right.png"  align="right" hspace="5" vspace="46"></a>
</div>
<br>
<a href="" class="foto_link">Смотреть все альбомы</a>
<a href=""><img src="<?=base_url()?>resources/images/dob_foto.png" align="right" hspace="18"></a>

</div>
<!--Foto svadeb end-->
