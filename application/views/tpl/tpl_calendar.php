<?php if($datesArray != NULL): ?>
<?php foreach ($datesArray as $dateItem): ?>
<?php if($dateItem['itemUrl'] != '#'): ?>
<a class="<?=$dateItem['itemClass']?>" href="<?=$dateItem['itemUrl']?>"><?=$dateItem['itemText']?></a>
<?php else: ?>
<a title="В эту дату ещё нет свадеб, нажмите сюда что бы просмотреть все свадьбы" class="<?=$dateItem['itemClass']?>" href="<?=base_url()?>wedding"><?=$dateItem['itemText']?></a>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
