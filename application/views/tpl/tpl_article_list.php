<?php
    $articlesCounter = 0;
    $totalCount = $articlesList->count();
?>

<?php foreach($articlesList as $articleItem): ?>
<a href="<?=base_url()?>article/<?=$articleItem->url?>" class="article_link"><?=$articleItem->name?></a><br>
<?=character_limiter($articleItem->description, 90)?>
<?php
    $articlesCounter++;
    if($articlesCounter < $totalCount): ?>
    <img src="<?=base_url()?>resources/images/line.png" vspace="10">
    <?php endif; ?>
<?php endforeach; ?>