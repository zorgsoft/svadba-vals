<!--Right Start-->
<div id="sidebar2">
<!--Calendar start-->

<div class="article">Когда у вас Свадьба:</div>
<div class="calendar">

<div class="calendar_count">
<!--Month start-->
<div class="month"><a href="" class="month_r">&lt;&lt;</a> &nbsp;&nbsp;&nbsp; Ноябрь &nbsp;&nbsp;&nbsp; <a href="" class="month_r">&gt;&gt;</a></div>
<!--Month start-->
<!--Days start-->
<div class="days">Пн</div>
<div class="days">Вт</div>
<div class="days">Ср</div>
<div class="days">Чт</div>
<div class="days">Пт</div>
<div class="days_h">Сб</div>
<div class="days_h">Вс</div>
<!--Days start-->

<!--calendar start-->
<?=libWeddings::showCalendar(date('m'))?>
<!--calendar start-->
</div>

</div>

<div class="calendar_b_c">
<div class="article_pod_l"><a href="<?=base_url()?>wedding" class="article_pod_link">Все свадьбы</a></div>
<div class="calendar_link"><a href="<?=base_url()?>wedding/add" class="article_pod_link_r">Добавить свадьбу</a></div>
<br class="clear">
</div>

<!--Calendar end-->

<!--New firms start-->
<div id="new_firm">
<div class="new_firm">Новые фирмы</div>

<div>
<?=Data::getNewFirmsForMenu()?>
</div>

</div>
<!--New firms start-->


<!--soc zakl start-->
<div class="soc"><img src="<?=base_url()?>resources/images/10.jpg"></div>
<!--soc zakl end-->

<!--Obich start-->
<div class="obich">
<div class="article">Обычаи и традиции</div>
<br>

<?=Data::getArticlesForGroup('stati-i-publikacii')?>
</div>

<div class="obich_b">
<div class="article_pod_l"><a href="<?=base_url()?>group/stati-i-publikacii" class="article_pod_link">Все статьи</a></div>
<div class="article_pod_r"><a href="" class="article_pod_link_r">Стать автором</a></div>
<br class="clear">
</div>
<!--Obich end-->
<br><br>
</div>
<!--Right End-->


<!--main start-->
<div class="main">
