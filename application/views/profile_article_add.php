{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">

<h1>Добавление новой статьи:</h1>
<?=validation_errors('<div style="color: red;">','</div>')?>
<br/>

<script type="text/javascript" >
   $(document).ready(function() {
        $("#description").cleditor({
            width: "100%",
            height: 150
            });
        $("#a_content").cleditor({
            width: "100%",
            height: 250
            });
   });
</script>
<?=form_open(base_url().'profile/article/add')?>
<label for="name">Название статьи:</label>
<input type="text" name="name" id="name" value="<?=set_value('name')?>" /><br/><br/>
<label for="group_id">Раздел: </label>
<select name="group_id" id="group_id">
<?php foreach($articlesGroupsList as $articleGroupItem): ?>
<option value="<?=$articleGroupItem->id?>"><?=$articleGroupItem->name?></option>
<?php endforeach; ?>
</select>
<br/><br/>
<label for="description">Краткое описание:</label><br/>
<textarea name="description" id="description"><?=set_value('description')?></textarea><br/>
<label for="a_content">Текст статьи:</label><br/>
<textarea name="a_content" id="a_content"><?=set_value('a_content')?></textarea><br/>
<br/>
<input type="submit" value="Добавить"/>

<?=form_close()?>
</div>

<!--main text end-->

{tpl_futter}