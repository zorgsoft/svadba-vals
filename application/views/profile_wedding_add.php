{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">

<h1>Добавление Свадьбы:</h1>
<?=validation_errors('<div style="color: red;">','</div>')?>
<br/>
<link rel="stylesheet" href="<?=sysRes()?>css/datePicker.css" type="text/css" media="screen" title="default" />
<script src="<?=sysRes()?>js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/date.js" type="text/javascript"></script>
<script src="<?=sysRes()?>js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script type="text/javascript" >
   $(document).ready(function() {
        $("#description").cleditor({
            width: "100%",
            height: 150
            });
        $("#a_content").cleditor({
            width: "100%",
            height: 250
            });
   });
</script>

<script>
    $(document).ready(function(){
        Date.format = 'dd-mm-yyyy';
        $.dpText = {
            TEXT_PREV_YEAR		:	'Предыдущий год',
            TEXT_PREV_MONTH		:	'Предыдущий месяц',
            TEXT_NEXT_YEAR		:	'Следующий год',
            TEXT_NEXT_MONTH		:	'Следующий месяц',
            TEXT_CLOSE			:	'Закрыть',
            TEXT_CHOOSE_DATE	:	'Выбор даты',
            HEADER_FORMAT		:	'mmmm yyyy'
        };
        $('#wedding_date').datePicker({startDate:'01-01-2000', clickInput:true});
    });
</script>
<?=form_open(base_url().'profile/wedding/add')?>
<label for="name">Название свадьбы:</label>
<input type="text" name="name" id="name" value="<?=set_value('name')?>" /><br/><br/>
<label for="wedding_date">Дата свадьбы:</label>
 <input name="wedding_date" id="wedding_date" type="text" class="span11" style="width: 150px;margin-left:32px" value="<?=set_value('wedding_date', date('d-m-Y', now()))?>" />
        <script>
            $('.date-pick').datePicker({startDate:'2000-01-01', clickInput:true});
        </script>
<br/><br/>
<label for="description">Краткое описание:</label><br/>
<textarea name="description" id="description"><?=set_value('description')?></textarea><br/>
<label for="a_content">Текст статьи:</label><br/>
<textarea name="a_content" id="a_content"><?=set_value('a_content')?></textarea><br/>
<br/>
<input type="submit" value="Добавить"/>

<?=form_close()?>
</div>

<!--main text end-->

{tpl_futter}