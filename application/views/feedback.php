{tpl_header}

{tpl_left}
{tpl_right}

<!--main text start-->
<div class="main_text">

<h1>Форма обратной связи:</h1>
<?=validation_errors('<div style="color: red;">','</div>')?>
<br/>

<p>
    Оставьте Ваше соообщение администратору сайта, все поля кроме телефона являются обязательными для заполнения.
    <br>
</p>
<?=form_open(base_url().'feedback')?>
<label for="name">Фамилия И.О.:</label>
<input type="text" name="name" id="name" value="<?=set_value('name')?>" /><br/><br/>
<label for="email">E-mail:</label>
<input type="text" name="email" id="email" value="<?=set_value('email')?>" /><br/><br/>
<label for="phone">Телефон:</label>
<input type="text" name="phone" id="phone" value="<?=set_value('phone')?>" /><br/><br/>
<label for="a_content">Ваше сообщение администратору:</label><br/>
<textarea name="a_content" id="a_content" style="width: 100%; height: 150px;"><?=set_value('a_content')?></textarea><br/>
<br/>
<label for="cap">Код проверки: <?=$capImage?><br></label>
<input type="text" name="cap" id="cap" value="" /><br/><br/>
<br/>
<br/>
<input type="submit" value="Отправить"/>

<?=form_close()?>
</div>

<!--main text end-->

{tpl_futter}