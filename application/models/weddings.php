<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Weddings model

class Weddings extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('meta_title', 'string', 256);
        $this->hasColumn('meta_description', 'string', 256);
        $this->hasColumn('meta_keywords', 'string', 256);
        $this->hasColumn('description', 'string');
        $this->hasColumn('content', 'string');
        $this->hasColumn('url', 'string', 128);
        $this->hasColumn('zindex', 'integer', null, array('default' => '0'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '1'));
       
		$this->hasColumn('wedding_date', 'date');
        $this->hasColumn('gallery_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
        
    }
    
    public function setUp() {
        $this->setTableName('weddings');
        $this->actAs('Timestampable');
       
       
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
            ));
               
        $this->hasOne('gallery as Gallery', array(
            'local' => 'gallery_id',
            'foreign' => 'id'
        ));
    }
}