<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cities extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('url', 'string', 128);
    }
    
    public function setUp() {
        $this->setTableName('cities');
        $this->hasMany('cities_firms as Firms', array(
            'local' => 'id',
            'foreign' => 'cities_id',
            'cascade' => array('delete')
            ));
    }
}
?>
