<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
    Acces_rights is a table with list acces names for use to check access
    and description for this rights.
    Acces_rights have a connection to roles_rights table named by Rights,
    with list of conections acces_rights to roles, one role can have many
    connections with acces_rights
*/
class Access_rights extends Doctrine_Record{
    public function setTableDefinition(){
        $this->hasColumn('name', 'string', 128);
        $this->hasColumn('description', 'string', 256);
    }
    
    public function setUp(){
        $this->setTableName('access_rights');
        $this->actAs('Timestampable');
        $this->hasMany('roles_rights as Rights', array(
                                                       'local' => 'id',
                                                       'foreign' => 'access_rights_id'
                                                       )
                       );
    }
}
?>