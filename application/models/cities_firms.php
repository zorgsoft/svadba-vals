<?php
class Cities_firms extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('cities_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('firm_id', 'integer', null, array('notnull' => 'false'));
    }
    
    public function setUp() {
        $this->setTableName('cities_firms');
        $this->hasOne('cities as City', array(
            'local' => 'cities_id',
            'foreign' => 'id'
        ));
        $this->hasOne('firms as Firm', array(
            'local' => 'firm_id',
            'foreign' => 'id'
        ));
    }
}
?>
