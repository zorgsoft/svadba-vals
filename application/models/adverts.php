<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adverts extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 254);
        $this->hasColumn('meta_title', 'string', 254);
        $this->hasColumn('meta_description', 'string', 254);
        $this->hasColumn('meta_keywords', 'string', 254);
        $this->hasColumn('content', 'string');
	    $this->hasColumn('url', 'string', 128);
        $this->hasColumn('site_url', 'string', 254);
        $this->hasColumn('group_id', 'integer', null, array('default' => '0'));
        $this->hasColumn('type_id', 'integer', null, array('default' => '0'));
        $this->hasColumn('gallery_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('city_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('author_name', 'string', 254);
        $this->hasColumn('author_email', 'string', 254);
        $this->hasColumn('author_phone', 'string', 254);
        $this->hasColumn('author_icq', 'string', 254);
        $this->hasColumn('author_city', 'string', 254);
        $this->hasColumn('price', 'decimal', null, array('default' => '0'));
        $this->hasColumn('paid', 'boolean', null, array('default' => '1'));
        $this->hasColumn('paid_to', 'date');
        $this->hasColumn('paid_sum', 'decimal', null, array('default' => '0'));
        $this->hasColumn('paid_type', 'integer', null, array('default' => '0'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '0'));
        $this->hasColumn('can_edit', 'boolean', null, array('default' => '1'));
	$this->hasColumn('zindex', 'integer', null, array('default' => '0'));
    }
    
    public function setUp() {
        $this->setTableName('Adverts');
        $this->actAs('Timestampable');
        
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
            ));
        $this->hasOne('Adverts_groups as Group', array(
            'local' => 'group_id',
            'foreign' => 'id'
            ));
        $this->hasOne('Adverts_types as Type', array(
            'local' => 'type_id',
            'foreign' => 'id'
            ));
        $this->hasOne('Gallery as Gallery', array(
            'local' => 'gallery_id',
            'foreign' => 'id'
            ));
        $this->hasOne('Cities as City', array(
            'local' => 'city_id',
            'foreign' => 'id'
            ));
    }
}
?>