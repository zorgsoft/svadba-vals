<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
    Roles_rights a table with links roles to access_rights
    THis have a connection named Role and Access_right
*/
class Roles_rights extends Doctrine_Record{
    public function setTableDefinition(){
        $this->hasColumn('roles_id', 'boolean', null, array('default' => '0'));
        $this->hasColumn('access_rights_id', 'boolean', null, array('default' => '0'));
    }
    
    public function setUp(){
        $this->setTableName('roles_rights');
        $this->hasOne('roles as Role', array(
            'local' => 'roles_id',
            'foreign' => 'id'
            ));
        $this->hasOne('access_rights as Access_right', array(
            'local' => 'access_rights_id',
            'foreign' => 'id'
            ));
    }
}

?>