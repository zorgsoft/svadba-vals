<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Actions_list extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('url', 'string', 256);
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
    }
    
    public function setUp() {
        $this->setTableName('actions_list');
        $this->actAs('Timestampable');
        
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
            ));
    }
}
?>