<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* FeedBack model class
*/
class Feedback extends Doctrine_Record
{
	public function setTableDefinition(){
		$this->hasColumn('name', 'string', 256);
        $this->hasColumn('email', 'string', 128, array('email' => 'true'));
        $this->hasColumn('phone', 'string', 128);
        $this->hasColumn('content', 'string');
	}

	public function setUp(){
		$this->setTableName('feedback');
		$this->actAs('Timestampable');
	}
}
?>