<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 254);
        $this->hasColumn('meta_title', 'string', 254);
        $this->hasColumn('meta_description', 'string', 254);
        $this->hasColumn('meta_keywords', 'string', 254);
        $this->hasColumn('description', 'string');
        $this->hasColumn('content', 'string');
	$this->hasColumn('url', 'string', 128);
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '0'));
	$this->hasColumn('zindex', 'integer', null, array('default' => '0'));
    }
    
    public function setUp() {
        $this->setTableName('news');
        $this->actAs('Timestampable');
    }
}
?>