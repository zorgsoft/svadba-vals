<?php
class Gallery_photos extends Doctrine_Record{
    public function settableDefinition(){
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('file_name', 'string', 256);
        $this->hasColumn('thumb_name', 'string', 256);
        $this->hasColumn('visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('gallery_id', 'integer');
        $this->hasColumn('pluses', 'integer');
        $this->hasColumn('minuses', 'integer');
        $this->hasColumn('total_votes', 'integer');
        $this->hasColumn('vote', 'integer');
        $this->hasColumn('zindex', 'integer', null, array('default' => '0'));
    }

    public function setUp(){
        $this->setTableName('gallery_photos');
        $this->actAs('Timestampable');
        $this->hasOne('Gallery', array(
            'local' => 'gallery_id',
            'foreign' => 'id',
            'onDelete' => 'CASCADE'
        ));
        
        $this->hasMany('gallery_comments as Comments', array(
            'local' => 'id',
            'foreign' => 'photo_id'
            
        ));
    }
}
?>