<?php

class Gallery_comments extends Doctrine_Record{
    
    public function settableDefinition(){
        
        $this->hasColumn('theme', 'string', 256);
        $this->hasColumn('text', 'string', 1000);
        $this->hasColumn('visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('photo_id', 'integer');
        $this->hasColumn('user_id', 'integer');
        $this->hasColumn('author', 'string', 30);
       
    }
    
    public function setUp(){
        $this->setTableName('gallery_comments');
        $this->actAs('Timestampable');
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
            
        ));
        
       
    }
    
}

?>
