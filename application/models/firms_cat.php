<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Firms_cat extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('meta_title', 'string', 256);
        $this->hasColumn('meta_description', 'string', 256);
        $this->hasColumn('meta_keywords', 'string', 256);
        $this->hasColumn('description', 'string');
        $this->hasColumn('content', 'string');
        $this->hasColumn('url', 'string', 128);
        $this->hasColumn('menu_name', 'string', 256);
        $this->hasColumn('menu_visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('menu_zindex', 'integer', null, array('default' => '0'));
        $this->hasColumn('zindex', 'integer', null, array('default' => '0'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('parent_id', 'integer', null, array('default' => '0'));
        $this->hasColumn('gallery_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('banner_id', 'integer', null, array('default' => '0'));
    }
    
    public function setUp() {
        $this->setTableName('firms_cat');
        
        $this->hasOne('firms_cat as Parent', array(
            'local' => 'parent_id',
            'foreign' => 'id'
        ));
        $this->hasOne('gallery as Gallery', array(
            'local' => 'gallery_id',
            'foreign' => 'id'
        ));
        $this->hasMany('firms_cat as Childs', array(
            'local' => 'id',
            'foreign' => 'parent_id',
            'cascade' => array('delete')
        ));
        $this->hasMany('firms_to_cat as Firms', array(
            'local' => 'id',
            'foreign' => 'firms_cat_id',
            'cascade' => array('delete')
        ));
    }
}
?>
