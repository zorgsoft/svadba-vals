<?php
class Gallery extends Doctrine_Record{
    public function setTableDefinition(){
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('meta_title', 'string', 256);
        $this->hasColumn('meta_description', 'string', 256);
        $this->hasColumn('meta_keywords', 'string', 256);
        $this->hasColumn('description', 'string');
        $this->hasColumn('content', 'string');
        $this->hasColumn('url', 'string', 128);
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('zindex', 'integer', null, array('default' => '0'));
    }

    public function setUp(){
        $this->setTableName('gallery');
        $this->actAs('Timestampable');
        $this->hasOne('users as Users', array(
            'local' => 'user_id',
            'foreign' => 'id'
            
        ));
        
        $this->hasMany('gallery_photos as Photos', array(
            'local' => 'id',
            'foreign' => 'gallery_id',
            'orderBy' => 'zindex'
        ));
    }
}
?>