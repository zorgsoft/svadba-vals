<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Requests_list extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('subject', 'string', 256);
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('email', 'string', 128, array('email' => 'true'));
        $this->hasColumn('phone', 'string', 128);
        $this->hasColumn('content', 'string');
        $this->hasColumn('obj_type', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('firms_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('wedding_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('adverts_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('articles_id', 'integer', null, array('notnull' => 'false'));
    }
    
    public function setUp() {
        $this->setTableName('Requests_list');
        $this->actAs('Timestampable');
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
        ));
        $this->hasOne('firms as Firm', array(
            'local' => 'firms_id',
            'foreign' => 'id'
        ));
        $this->hasOne('weddings as Wedding', array(
            'local' => 'wedding_id',
            'foreign' => 'id'
        ));
        $this->hasOne('adverts as Advert', array(
            'local' => 'adverts_id',
            'foreign' => 'id'
        ));
        $this->hasOne('articles as Article', array(
            'local' => 'articles_id',
            'foreign' => 'id'
        ));
    }
}
?>
