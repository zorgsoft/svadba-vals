<?php
class Template_pages extends Doctrine_Record {
    public function setTableDefinition(){
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('template_name', 'string', 256);
        $this->hasColumn('content', 'string');
    }
}
?>