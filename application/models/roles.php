<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
    Roles table, descript a role name and have connection to
    Users table with list of users with this role,
    and have connection to many roles_rights table, named by Rights
    If this role deleted, users who habe this role, roles_id set to null
*/
class Roles extends Doctrine_Record{
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 128);
        $this->hasColumn('description', 'string', 256);
    }
    
    public function setUp() {
        $this->setTableName('roles');
        $this->actAs('Timestampable');
        $this->hasMany('roles_rights as Rights', array(
            'local' => 'id',
            'foreign' => 'roles_id',
            'cascade' => array('delete')
            ));
        $this->hasMany('users as Users', array(
            'local' => 'id',
            'foreign' => 'roles_id',
            'onDelete' => 'SET NULL'
            ));
    }
}
?>