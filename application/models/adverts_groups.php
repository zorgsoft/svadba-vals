<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Groups for adverts
 */
class Adverts_groups extends Doctrine_Record {
	
	public function setTableDefinition() {
		$this->hasColumn('name', 'string', 254);
		$this->hasColumn('meta_title', 'string', 254);
		$this->hasColumn('meta_description', 'string', 254);
	        $this->hasColumn('meta_keywords', 'string', 254);
		$this->hasColumn('url', 'string', 128);
	        $this->hasColumn('content', 'string');
		$this->hasColumn('parent_id', 'integer', null, array('default' => '0'));
		$this->hasColumn('can_add', 'boolean', null, array('default' => '1'));
		$this->hasColumn('visible', 'boolean', null, array('default' => '0'));
		$this->hasColumn('zindex', 'integer', null, array('default' => '0'));
	}
	
	public function setUp()	{
		$this->setTableName('Adverts_groups');
		$this->hasOne('Adverts_groups as Parent', array(
		'local' => 'parent_id',
		'foreign' => 'id',
		));
		
		$this->hasMany('Adverts_groups as Childs', array(
		'local' => 'id',
		'foreign' => 'parent_id',
		'orderBy' => 'zindex ASC, name ASC',
		'cascade' => array('delete')		// Delete child groups when delete parent record
		));
		$this->hasMany('Adverts as Adverts', array(
		'local' => 'id',
		'foreign' => 'group_id',
		'orderBy' => 'zindex ASC',
		'onDelete' => 'SET DEFAULT'			// Set to 0 group_id in Adverts when delete this group
		));
	}
}

?>