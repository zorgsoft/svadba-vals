<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Articles model

class Articles extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('meta_title', 'string', 256);
        $this->hasColumn('meta_description', 'string', 256);
        $this->hasColumn('meta_keywords', 'string', 256);
        $this->hasColumn('description', 'string');
        $this->hasColumn('content', 'string');
        $this->hasColumn('url', 'string', 128);
        $this->hasColumn('menu_name', 'string', 256);
        $this->hasColumn('menu_visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('menu_zindex', 'integer', null, array('default' => '0'));
        $this->hasColumn('zindex', 'integer', null, array('default' => '0'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('parent_id', 'integer', null, array('default' => '0'));
        $this->hasColumn('group_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('gallery_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('access_rights_id', 'integer', null, array('notnull' => 'false'));
    }
    
    public function setUp() {
        $this->setTableName('articles');
        $this->actAs('Timestampable');
        
        $this->hasOne('articles as Parent', array(
            'local' => 'parent_id',
            'foreign' => 'id'
            ));
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
            ));
        $this->hasOne('articles_groups as Group', array(
            'local' => 'group_id',
            'foreign' => 'id'
            ));
        $this->hasMany('articles as Childs', array(
            'local' => 'id',
            'foreign' => 'parent_id',
            'onDelete' => 'SET DEFAULT'
            ));
        $this->hasOne('access_rights as Access', array(
            'local' => 'access_rights_id',
            'foreign' => 'id'
            ));
        $this->hasOne('gallery as Gallery', array(
            'local' => 'gallery_id',
            'foreign' => 'id'
        ));
    }
}