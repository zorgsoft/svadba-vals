<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Articles groups model

class Articles_groups extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('url', 'string', 256);
        $this->hasColumn('access_rights_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('meta_title', 'string', 256);
        $this->hasColumn('meta_description', 'string', 256);
        $this->hasColumn('meta_keywords', 'string', 256);
        $this->hasColumn('content', 'string');
        $this->hasColumn('visible', 'boolean', null, array('default' => '0'));
    }
    
    public function setUp() {
        $this->setTableName('articles_groups');
        $this->hasMany('articles as Articles', array(
            'local' => 'id',
            'foreign' => 'group_id',
            'onDelete' => 'SET DEFAULT'
            ));
    }
}
?>