<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Firms extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('meta_title', 'string', 256);
        $this->hasColumn('meta_description', 'string', 256);
        $this->hasColumn('meta_keywords', 'string', 256);
        $this->hasColumn('description', 'string');
        $this->hasColumn('tags', 'string');
        $this->hasColumn('content', 'string');
        $this->hasColumn('contacts', 'string');
        $this->hasColumn('url', 'string', 128);
        $this->hasColumn('img_logo', 'string', 256);
        $this->hasColumn('menu_name', 'string', 256);
        $this->hasColumn('menu_visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('menu_zindex', 'integer', null, array('default' => '0'));
        $this->hasColumn('zindex', 'integer', null, array('default' => '0'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '1'));
        $this->hasColumn('views', 'integer', null, array('default' => '0'));
        $this->hasColumn('paid', 'boolean', null, array('default' => '1'));
        $this->hasColumn('paid_to', 'date');
        $this->hasColumn('paid_sum', 'decimal', null, array('default' => '0'));
        $this->hasColumn('gallery_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
    }
    
    public function setUp() {
        $this->setTableName('firms');
        $this->actAs('Timestampable');
        
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
        ));
        $this->hasOne('gallery as Gallery', array(
            'local' => 'gallery_id',
            'foreign' => 'id'
        ));
        
        $this->hasMany('cities_firms as Cities', array(
            'local' => 'id',
            'foreign' => 'firm_id',
            'cascade' => array('delete')
        ));
        $this->hasMany('firms_to_cat as Firms_cats', array(
            'local' => 'id',
            'foreign' => 'firm_id',
            'cascade' => array('delete'),
            'orderBy' => 'id ASC'
        ));
        
        $this->hasMany('firms_adds as Addresses', array(
            'local' => 'id',
            'foreign' => 'firm_id',
            'cascade' => array('delete'),
            'orderBy' => 'id ASC'
        ));
    }
}
?>
