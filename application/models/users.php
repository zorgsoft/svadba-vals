<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
    Users table
    Users has connection with role, named link Role
*/
class Users extends Doctrine_Record{
    public function setTableDefinition(){
        $this->hasColumn('login', 'string', 128, array('unique' => 'true'));
        $this->hasColumn('password', 'string', 128);
        $this->hasColumn('name', 'string', 256);
        $this->hasColumn('email', 'string', 128, array('email' => 'true', 'unique' => 'true'));
        $this->hasColumn('phone', 'string', 128);
        $this->hasColumn('comment', 'string');
        $this->hasColumn('roles_id', 'boolean', null, array('default' => '0'));
    }
    
    public function setUp() {
        $this->setTableName('users');
        $this->actAs('Timestampable');
        $this->hasMutator('password', 'md5Password');
        $this->hasOne('roles as Role', array(
                                             'local' => 'roles_id',
                                             'foreign' => 'id'));
    }
    
    protected function md5Password($value) {
        $this->_set('password', md5($value));
    }
}

?>