<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Adverts types
 */
class Adverts_types extends Doctrine_Record {
	public function setTableDefinition() {
		$this->hasColumn('name', 'string', 254);
		$this->hasColumn('meta_title', 'string', 254);
		$this->hasColumn('meta_description', 'string', 254);
		$this->hasColumn('meta_keywords', 'string', 254);
		$this->hasColumn('url', 'string', 128);
		$this->hasColumn('content', 'string');
		$this->hasColumn('can_add', 'boolean', null, array('default' => '1'));
		$this->hasColumn('visible', 'boolean', null, array('default' => '0'));
		$this->hasColumn('zindex', 'integer', null, array('default' => '0'));
	}
	
	public function setUp() {
		$this->setTableName('Adverts_types');
		$this->hasMany('Adverts as Adverts', array(
		'local' => 'id',
		'foreign' => 'type_id',
		'orderBy' => 'zindex ASC',
		'onDelete' => 'SET DEFAULT'
		));
	}
}

?>