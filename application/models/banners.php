<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Banners extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('name', 'string', 254);
        $this->hasColumn('title', 'string', 254);
        $this->hasColumn('url', 'string', 128);
        $this->hasColumn('site_url', 'string', 254);
        $this->hasColumn('file_name', 'string', 256);
        $this->hasColumn('user_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('paid', 'boolean', null, array('default' => '1'));
        $this->hasColumn('paid_to', 'date');
        $this->hasColumn('paid_sum', 'decimal', null, array('default' => '0'));
        $this->hasColumn('paid_type', 'integer', null, array('default' => '0'));
        $this->hasColumn('visible', 'boolean', null, array('default' => '0'));
        $this->hasColumn('zindex', 'integer', null, array('default' => '0'));
    }
    
    public function setUp() {
        $this->setTableName('Banners');
        $this->hasOne('users as User', array(
            'local' => 'user_id',
            'foreign' => 'id'
            ));
    }
}
?>