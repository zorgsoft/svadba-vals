<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Firms_to_cat extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('firms_cat_id', 'integer', null, array('notnull' => 'false'));
        $this->hasColumn('firm_id', 'integer', null, array('notnull' => 'false'));
    }
    
    public function setUp() {
        $this->setTableName('firms_to_cat');
        $this->hasOne('firms_cat as Firms_cat', array(
            'local' => 'firms_cat_id',
            'foreign' => 'id'
        ));
        $this->hasOne('firms as Firm', array(
            'local' => 'firm_id',
            'foreign' => 'id'
        ));
    }
}
?>
