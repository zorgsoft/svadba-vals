<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Firms_adds extends Doctrine_Record {
    public function setTableDefinition() {
        $this->hasColumn('cities_id', 'integer', null, array('default' => '0'));
        $this->hasColumn('firm_id', 'integer', null, array('default' => '0'));
        $this->hasColumn('emails', 'string', 1024);
        $this->hasColumn('url', 'string', 254);
        $this->hasColumn('phones', 'string', 1024);
        $this->hasColumn('address', 'string');
        $this->hasColumn('can_edit', 'boolean', null, array('default' => '1'));
    }
    
    public function setUp() {
        $this->setTableName('firms_adds');
        $this->hasOne('firms as Firm', array(
            'local' => 'firm_id',
            'foreign' => 'id'
        ));
    }
}
?>
