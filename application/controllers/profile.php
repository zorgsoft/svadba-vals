<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function index() {
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }

        $userData = Auth::user();

        $template_data = array();
        $template_data['meta_title'] = 'Личный кабинет - Пользователь: ' . $userData->name;
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, ' . $userData->name;
        $template_data['meta_keywords'] = 'свадьба, вальс';

        $userId = Auth::getUserId();

        $template_data['userArticlesList'] = Doctrine_Query::create()
                ->select('*')
                ->from('articles')
                ->where('user_id =?', $userId)
                ->orderBy('name ASC')
                ->execute();

        $template_data['userFirmsList'] = Doctrine_Query::create()
                ->select('*')
                ->from('firms')
                ->where('user_id =?', $userId)
                ->orderBy('name ASC')
                ->execute();

        $template_data['userAdvertsList'] = Doctrine_Query::create()
                ->select('*')
                ->from('adverts')
                ->where('user_id =?', $userId)
                ->orderBy('name ASC')
                ->execute();
        
        $template_data['userWeddingsList'] = Doctrine_Query::create()
                ->select('*')
                ->from('weddings')
                ->where('user_id =?', $userId)
                ->orderBy('name ASC')
                ->execute();

        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('profile.php', $template_data);
    }

    public function login() {
        // User login
        if (Auth::isAuthorized() != FALSE) {
            redirect(base_url() . 'profile');
        }

        $template_data = array();
        $template_data['meta_title'] = 'Личный кабинет - Вход';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, вход';
        $template_data['meta_keywords'] = 'свадьба, вальс';

        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $template_data['error_message'] = '';

        // Start form rules
        $this->form_validation->set_rules('login', 'Логин', 'trim|required|max_length[128]|xss_clean');
        $this->form_validation->set_rules('password', 'Пароль', 'trim|required|max_length[128]|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_login.php', $template_data);
        } else {
            $userLogin = $this->input->post('login');
            $userPassword = $this->input->post('password');
            if (Auth::login($userLogin, $userPassword) == TRUE) {
                redirect(base_url() . 'profile');
            } else {
                $template_data['error_message'] = 'Ошибка, неверный логин или пароль!';
                $this->parser->parse('profile_login.php', $template_data);
            }
        }
    }

    public function logout() {
        // Logout user
        Auth::logout();
        redirect(base_url());
    }

    public function articles_add() {
        // Add new user article
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }

        $template_data = array();
        $template_data['meta_title'] = 'Личный кабинет - Добавление статьи';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, добавление статьи';
        $template_data['meta_keywords'] = 'свадьба, вальс';

        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $template_data['articlesGroupsList'] = Doctrine_Query::create()
                ->select('id, name')
                ->from('articles_groups')
                ->where('visible =?', 1)
                ->orderBy('name ASC')
                ->execute();

        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[512]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст статьи', 'trim|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_article_add.php', $template_data);
        } else {
            $userId = Auth::getUserId();
            $newUserArticle = new Articles();
            $newUserArticle->name = $this->input->post('name');
            $newUserArticle->description = $this->input->post('description');
            $newUserArticle->content = $this->input->post('a_content');
            $newUserArticle->group_id = $this->input->post('group_id');
            $newUserArticle->user_id = $userId;
            $newUserArticle->visible = 0;
            $newUserArticle->save();

            // Send email to admin
            Actions::Add($userId, 'Новая статья - ' . $newUserArticle->name, base_url() . 'admin/articles/edit/' . $newUserArticle->id);
            // Send email to admin
            redirect(base_url() . 'profile');
        }
    }

    public function articles_edit($articleId) {
        // Add new user article
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }

        $userArticleData = Doctrine::getTable('articles')->findOneBy('id', $articleId);

        $userId = Auth::getUserId();

        if ($userArticleData == NULL or $userArticleData->user_id != $userId) {
            redirect(base_url() . 'profile');
        }

        $template_data = array();
        $template_data['meta_title'] = 'Личный кабинет - Редактирование статьи';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, редактирование статьи';
        $template_data['meta_keywords'] = 'свадьба, вальс';

        $template_data['userArticleData'] = $userArticleData;

        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $template_data['articlesGroupsList'] = Doctrine_Query::create()
                ->select('id, name')
                ->from('articles_groups')
                ->where('visible =?', 1)
                ->orderBy('name ASC')
                ->execute();

        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[512]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст статьи', 'trim|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_article_edit.php', $template_data);
        } else {
            $template_data['userArticleData']->name = $this->input->post('name');
            $template_data['userArticleData']->description = $this->input->post('description');
            $template_data['userArticleData']->content = $this->input->post('a_content');
            $template_data['userArticleData']->group_id = $this->input->post('group_id');
            $template_data['userArticleData']->visible = 0;
            $template_data['userArticleData']->save();

            // Send email to admin
            Actions::Add($userId, 'Редактирование статьи - ' . $template_data['userArticleData']->name, base_url() . 'admin/articles/edit/' . $template_data['userArticleData']->id);
            // Send email to admin
            redirect(base_url() . 'profile');
        }
    }

    public function wedding_add() {

        // Add new user article
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }

        $template_data = array();
        $template_data['meta_title'] = 'Личный кабинет - Добавление свадьбы';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, добавление свадьбы';
        $template_data['meta_keywords'] = 'свадьба, вальс';

        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);



        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');

        $this->form_validation->set_rules('a_content', 'Текст статьи', 'trim|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_wedding_add.php', $template_data);
        } else {
            $userId = Auth::getUserId();
            $newWedding = new Weddings();
            $newWedding->name = $this->input->post('name');
            $newWedding->wedding_date = $this->input->post('wedding_date');
            $newWedding->content = $this->input->post('a_content');
            $newWedding->user_id = $userId;
            $newWedding->visible = 0;
            $newWedding->save();

            // Send email to admin
            Actions::Add($userId, 'Новая свадьба - ' . $newWedding->name, base_url() . 'admin/weddings/edit/' . $newWedding->id);
            // Send email to admin
            redirect(base_url() . 'profile');
        }
    }

    public function firms_add() {
        // Add new firm for user
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }

        $template_data = array();
        $template_data['meta_title'] = 'Личный кабинет - Добавление фирмы';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, добавление фирмы';
        $template_data['meta_keywords'] = 'свадьба, вальс';

        $template_data['tpl_cat_list'] = Doctrine_Query::create()
                ->select('id, name')
                ->from('firms_cat')
                ->where('visible =?', 1)
                ->andWhere('parent_id >?', 0)
                ->orderBy('name ASC')
                ->execute();

        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание фирмы', 'trim|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст фирмы', 'trim|xss_clean');
        $this->form_validation->set_rules('contacts', 'Контактные данные', 'trim|xss_clean');
        $this->form_validation->set_rules('catalog_id', 'Раздел', 'trim|xss_clean|is_natural');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_firms_add.php', $template_data);
        } else {
            $selectedCatId = $this->input->post('catalog_id');
            $catData = Doctrine::getTable('firms_cat')->findOneBy('id', $selectedCatId);
            if ($catData == NULL or $catData->visible == 0 or $catData->parent_id == 0) {
                redirect(base_url() . 'profile');
            }

            $newFirm = new Firms();
            $newFirm->name = $this->input->post('name');
            $newFirm->description = $this->input->post('description');
            $newFirm->content = $this->input->post('a_content');
            $newFirm->contacts = $this->input->post('contacts');
            $newFirm->visible = 0;
            if (Auth::isAuthorized() == FALSE) {
                $userId = 0;
            } else {
                $userId = Auth::getUserId();
            }
            $newFirm->user_id = $userId;
            $newFirm->save();

            $newFirToCat = new Firms_to_cat();
            $newFirToCat->firms_cat_id = $catData->id;
            $newFirToCat->firm_id = $newFirm->id;
            $newFirToCat->save();

            // Send email to admin
            Actions::Add($userId, 'Новая фирма - ' . $newFirm->name, base_url() . 'admin/firms/edit/' . $newFirm->id);
            // END: Send email to admin

            if (Auth::isAuthorized() == FALSE) {
                $this->parser->parse('profile_firms_add_done.php', $template_data);
            } else {
                redirect(base_url() . 'profile');
            }
        }
    }
    
    public function request_firm($firm_id) {
        // Request from user to change firm
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }
        
        
        $template_data = array();
        $template_data['firmData'] = Doctrine::getTable('firms')->findOneBy('id', $firm_id);
        $userId = Auth::getUserId();
        if($template_data['firmData'] == NULL or $template_data['firmData']->user_id != $userId){
            redirect(base_url() . 'profile');
        }
        
        $template_data['meta_title'] = 'Личный кабинет - Запрос на и зменение фирмы';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, изменение фирмы';
        $template_data['meta_keywords'] = 'свадьба, вальс';
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $this->form_validation->set_rules('subject', 'Тема запроса', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('name', 'Фамилия И.О.', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|max_length[128]|xss_clean');
        $this->form_validation->set_rules('phone', 'Телефон', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Ваше сообщение', 'trim|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_firms_request.php', $template_data);
        } else {
            $newRequest = new Requests_list();
            $newRequest->subject = $this->input->post('subject');
            $newRequest->name = $this->input->post('name');
            $newRequest->email = $this->input->post('email');
            $newRequest->phone = $this->input->post('phone');
            $newRequest->content = $this->input->post('a_content');
            $newRequest->obj_type = 0;
            $newRequest->firms_id = $template_data['firmData']->id;
            $newRequest->user_id = $userId;
            $newRequest->save();
            
            $this->session->set_flashdata('profile_flash', 'Ваш запрос на изменение фирмы успешно отправлен.');
            
            Actions::Add($userId, 'Запрос для фирмы - ' . $newRequest->subject, base_url() . 'admin/requests/view/' . $newRequest->id);
            
            redirect(base_url() . 'profile');
        }
    }
    
    public function request_adverts($adverts_id) {
        // Request from user to change adverts
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }
        
        
        $template_data = array();
        $template_data['advertData'] = Doctrine::getTable('adverts')->findOneBy('id', $adverts_id);
        $userId = Auth::getUserId();
        if($template_data['advertData'] == NULL or $template_data['advertData']->user_id != $userId){
            redirect(base_url() . 'profile');
        }
        
        $template_data['meta_title'] = 'Личный кабинет - Запрос на и зменение объявления';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, изменение объявления';
        $template_data['meta_keywords'] = 'свадьба, вальс';
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $this->form_validation->set_rules('subject', 'Тема запроса', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('name', 'Фамилия И.О.', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|max_length[128]|xss_clean');
        $this->form_validation->set_rules('phone', 'Телефон', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Ваше сообщение', 'trim|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_adverts_request.php', $template_data);
        } else {
            $newRequest = new Requests_list();
            $newRequest->subject = $this->input->post('subject');
            $newRequest->name = $this->input->post('name');
            $newRequest->email = $this->input->post('email');
            $newRequest->phone = $this->input->post('phone');
            $newRequest->content = $this->input->post('a_content');
            $newRequest->obj_type = 2;
            $newRequest->adverts_id = $template_data['advertData']->id;
            $newRequest->user_id = $userId;
            $newRequest->save();
            
            $this->session->set_flashdata('profile_flash', 'Ваш запрос на изменение объявления успешно отправлен.');
            
            Actions::Add($userId, 'Запрос для объявления - ' . $newRequest->subject, base_url() . 'admin/requests/view/' . $newRequest->id);
            
            redirect(base_url() . 'profile');
        }
    }

    public function request_articles($articleId) {
        if (Auth::isAuthorized() == FALSE) {
            redirect(base_url() . 'profile/login');
        }

        $template_data = array();
        $template_data['articleData'] = Doctrine::getTable('articles')->findOneBy('id', $articleId);
        $userId = Auth::getUserId();
        if($template_data['articleData'] == NULL or $template_data['articleData']->user_id != $userId){
            redirect(base_url() . 'profile');
        }
        
        $template_data['meta_title'] = 'Личный кабинет - Запрос на и зменение статьи';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, изменение статьи';
        $template_data['meta_keywords'] = 'свадьба, вальс';
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $this->form_validation->set_rules('subject', 'Тема запроса', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('name', 'Фамилия И.О.', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|max_length[128]|xss_clean');
        $this->form_validation->set_rules('phone', 'Телефон', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Ваше сообщение', 'trim|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_articles_request.php', $template_data);
        } else {
            $newRequest = new Requests_list();
            $newRequest->subject = $this->input->post('subject');
            $newRequest->name = $this->input->post('name');
            $newRequest->email = $this->input->post('email');
            $newRequest->phone = $this->input->post('phone');
            $newRequest->content = $this->input->post('a_content');
            $newRequest->obj_type = 3;
            $newRequest->articles_id = $template_data['articleData']->id;
            $newRequest->user_id = $userId;
            $newRequest->save();
            
            $this->session->set_flashdata('profile_flash', 'Ваш запрос на изменение статьи успешно отправлен.');
            
            Actions::Add($userId, 'Запрос для статьи - ' . $newRequest->subject, base_url() . 'admin/requests/view/' . $newRequest->id);
            
            redirect(base_url() . 'profile');
        }
    }
    
    public function adverts_add() {
        // Add adverrts by user
        // Asd
        if (Auth::isAuthorized() == FALSE) {
            $userId = 0;
        } else {
            $userId = Auth::getUserId();
        }
        
        
        $template_data = array();
        $template_data['advertsGroupsList'] = Doctrine_Query::create()
                ->select('id, name')
                ->from('adverts_groups')
                ->where('visible =?', 1)
                ->andWhere('can_add =?', 1)
                ->orderBy('name ASC')
                ->execute();
        
        $template_data['advertsTypesList'] = Doctrine_Query::create()
                ->select('id, name')
                ->from('adverts_types')
                ->where('visible =?', 1)
                ->andWhere('can_add =?', 1)
                ->orderBy('name ASC')
                ->execute();
        
        $template_data['meta_title'] = 'Личный кабинет - Добавление объявления';
        $template_data['meta_description'] = 'Личный кабинет пользоваетля, добавление объявления';
        $template_data['meta_keywords'] = 'свадьба, вальс';
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $this->form_validation->set_rules('name', 'Название объявления', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('author_name', 'Фамилия И.О.', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('author_email', 'E-mail', 'trim|valid_email|max_length[128]|xss_clean');
        $this->form_validation->set_rules('author_phone', 'Телефон', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('author_icq', 'ICQ', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('author_city', 'Город', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('site_url', 'Ссылка на сайт', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст объявления', 'trim|max_length[500]|xss_clean');
        // End form rules

        if ($this->form_validation->run() == FALSE) {
            $this->parser->parse('profile_adverts_add.php', $template_data);
        } else {
            $newAdvert = new Adverts();
            $newAdvert->user_id = $userId;
            $newAdvert->name = $this->input->post('name');
            $newAdvert->author_name = $this->input->post('author_name');
            $newAdvert->author_email = $this->input->post('author_email');
            $newAdvert->author_phone = $this->input->post('author_phone');
            $newAdvert->author_icq = $this->input->post('author_icq');
            $newAdvert->author_city = $this->input->post('author_city');
            $newAdvert->site_url = $this->input->post('site_url');
            $newAdvert->content = $this->input->post('a_content');
            $newAdvert->group_id = $this->input->post('group_id');
            $newAdvert->price = $this->input->post('price');
            $newAdvert->type_id = $this->input->post('type_id');
            $newAdvert->paid = 0;
            $newAdvert->visible = 0;
            $newAdvert->can_edit = 0;
            $newAdvert->user_id = $userId;
            $newAdvert->save();
            
            $this->session->set_flashdata('profile_flash', 'Новое объявление успешно добавленно.');
            
            Actions::Add($userId, 'Новое объявление - ' . $newAdvert->name, base_url() . 'admin/adverts/edit/' . $newAdvert->id);
            
            if (Auth::isAuthorized() == FALSE) {
                redirect(base_url() . 'adverts');
            } else {
                redirect(base_url() . 'profile');
            }
        }
    }

}

?>