<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_controller extends CI_Controller{
    public function index(){
        // By default just redirect to main site
        redirect(base_url());
    }
    
    public function login(){
        // Show login form, if user logged then refirect him to loggedin page
        $template_data = array();
        $template_data['error_message'] = '';
        $redirect_to_url = $this->session->userdata('login_redir_url');
        if(Auth::isAuthorized() === TRUE)
            redirect(base_url().'loggedin');
    
        $this->form_validation->set_rules('login', 'Логин', 'trim|required|min_length[3]|max_length[128]|xss_clean');
        $this->form_validation->set_rules('password', 'Пароль', 'trim|required|min_length[4]|max_length[128]|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('auth/login', $template_data);
        } else {
            if(Auth::login($this->input->post('login'), $this->input->post('password')) === TRUE){
                if($redirect_to_url === FALSE){
                    redirect(base_url());
                } else {
                    $this->session->unset_userdata('login_redir_url');
                    redirect($redirect_to_url);
                }
            } else {
                $template_data['error_message'] = 'Неверный логин или пароль.';
                $this->load->view('auth/login', $template_data);
            }
        }        
    }
    
    public function logout(){
        // Log out user and redirect to base site
        Auth::logout(base_url());
    }
}

?>