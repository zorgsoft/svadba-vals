<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Firms_cats extends CI_Controller {
    public function index($catUrl) {
        // Show cat
        $catData =  Doctrine::getTable('firms_cat')->findOneBy('url', $catUrl);
        if($catData == NULL or $catData->visible == 0){
            show_404(base_url().'cat/'.$catUrl, FALSE);
        }
        
        $template_data = array();
        $template_data['meta_title'] = $catData->meta_title;
        $template_data['meta_description'] = $catData->meta_description;
        $template_data['meta_keywords'] = $catData->meta_keywords;
        
        $template_data['content_cat'] = $catData;
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right_cat.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('cat.php', $template_data);
    }
    
    public function all(){
        // Show all top level cats list
        $catListAll = Doctrine_Query::create()
            ->select('*')
            ->from('firms_cat')
            ->where('visible =?', 1)
            ->andWhere('parent_id =?', 0)
            ->orderBy('zindex ASC')
            ->execute();
            
        $template_data = array();
        $template_data['meta_title'] = 'Свадьба Вальс - Каталог фирм';
        $template_data['meta_description'] = 'Свадьба Вальс - Каталог фирм';
        $template_data['meta_keywords'] = 'Свадьба Вальс, Каталог фирм';
        
        $template_data['cat_list_all'] = $catListAll;
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('cat_all.php', $template_data);
    }
}
?>