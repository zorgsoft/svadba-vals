<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Article extends CI_Controller {
    public function index($articleUrl) {
        // Article show by url
        $articleData =  Doctrine::getTable('articles')->findOneBy('url', $articleUrl);
        if($articleData == NULL or $articleData->visible == 0){
            show_404(base_url().'article/'.$articleUrl, FALSE);
        }
        
        $template_data = array();
        $template_data['meta_title'] = $articleData->meta_title;
        $template_data['meta_description'] = $articleData->meta_description;
        $template_data['meta_keywords'] = $articleData->meta_keywords;
        
        $template_data['content_article'] = $articleData;
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('article.php', $template_data);
    }
}
?>