<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* FeedBack main class for users
*/
class Show_feedback extends CI_Controller
{
	public function index(){
		$template_data = array();
		$template_data['meta_title'] = 'Обратная связь';
        $template_data['meta_description'] = 'Свадьба Вальс - Обратная связь';
        $template_data['meta_keywords'] = 'Обратная связь, оствать сообщение, связь с администрацией';

        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);

        $this->form_validation->set_rules('name', 'Фамилия И.О.', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|max_length[128]|xss_clean');
        $this->form_validation->set_rules('phone', 'Телефон', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Ваше сообщение', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cap', 'Код проверки', 'trim|required|max_length[128]|xss_clean|callback_cap_check');

        if ($this->form_validation->run() == FALSE) {
        	libCap::createCap();
        	$template_data['capImage'] = $this->session->userdata('cap_image');
            $this->parser->parse('feedback.php', $template_data);
        } else {
        	$newFeedBack = new Feedback();
        	$newFeedBack->name = $this->input->post('name');
        	$newFeedBack->email = $this->input->post('email');
        	$newFeedBack->phone = $this->input->post('phone');
        	$newFeedBack->content = $this->input->post('a_content');
        	$newFeedBack->save();

        	Actions::Add(0, 'Новое сообщение с сайта - ' . $newFeedBack->name, base_url() . 'admin/feedback/' . $newFeedBack->id);
        	libCap::removeCap();
        	$this->parser->parse('feedback_done.php', $template_data);
        }
	}

	public function cap_check($capUserCode){
		// Check captcha code
		$capCode = $this->session->userdata('cap_code');
		$capUserCode = trim($capUserCode);
		if(strtolower($capCode) == strtolower($capUserCode)){
			return TRUE;
		} else {
			$this->form_validation->set_message('cap_check', 'Введённый вами код проверки не верен');
			return FALSE;
		}
	}
}
?>