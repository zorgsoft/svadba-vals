<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bgo extends CI_Controller {
    public function index($shortUrl) {
        // Redirect banner short url
        $bannerData = Doctrine::getTable('banners')->findOneBy('url', $shortUrl);
        if($bannerData != NULL) {
            redirect($bannerData->site_url);
        } else {
            redirect(base_url(), 'location', 301);
        }
    }
}
?>