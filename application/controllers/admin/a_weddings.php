<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class A_weddings extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin/articles');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function index($group_id = NULL) {
        // Main articles function for show articles list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу со статьями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Свадьбы';
        $template_data['page_head'] = 'Свадьбы';
        $template_data['menuSelected'] = 'Свадьбы';
        $template_data['subSelected'] = 'Список свадеб';
        
        
        // Get list of articles
        $articles_list_q = Doctrine_Query::create()
            ->select('*')
            ->from('articles')
            ->orderBy('zindex ASC');
            
        if($group_id != NULL){
            $group_data = Doctrine::getTable('articles_groups')->findOneBy('id', $group_id);
            if($group_data != NULL){
                $template_data['page_head'] = $template_data['page_head'] . ' для группы - ' . $group_data->name;
                $articles_list_q->where('group_id = ?', $group_data->id);
            }
        }
        $template_data['articles_list'] = $articles_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_articles.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function add() {
        // Add article function
        if(Auth::canAccess(R_USER_ARTICLES_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет добавлять статьи.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавление статьи';
        $template_data['page_head'] = 'Добавление статьи';
        $template_data['menuSelected'] = 'Статьи';
        $template_data['subSelected'] = 'Добавить статью';
        
        $template_data['articles_list'] = Doctrine::getTable('articles')->findAll();
        $template_data['articles_groups_list'] = Doctrine::getTable('articles_groups')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['roles_list'] = Doctrine::getTable('roles')->findAll();
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[512]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст статьи', 'trim|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка этой статьи', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        $this->form_validation->set_rules('menu_name', 'Название меню', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('menu_zindex', 'Позиция меню (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('menu_visible', '', 'xss_clean');
        // End form rules
        
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_articles_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_article = new Articles();
            $new_article->name = $this->input->post('name');
            $new_article->url = $this->input->post('url');
            $new_article->meta_title = $this->input->post('meta_title');
            $new_article->meta_description = $this->input->post('meta_description');
            $new_article->meta_keywords = $this->input->post('meta_keywords');
            $new_article->description = $this->input->post('description');
            $new_article->content = $this->input->post('a_content');
            $new_article->parent_id = $this->input->post('parent_id');
            $new_article->group_id = $this->input->post('group_id');
            $new_article->user_id = $this->input->post('user_id');
            $new_article->gallery_id = $this->input->post('gallery_id');
            $new_article->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_article->zindex = $this->input->post('zindex');
            $new_article->menu_name = $this->input->post('menu_name');
            $new_article->menu_visible = str_replace('on', '1', $this->input->post('menu_visible'));
            $new_article->menu_zindex = $this->input->post('menu_zindex');
            $new_article->access_rights_id = $this->input->post('access_rights_id');
            $new_article->save();
            redirect(base_url() . 'admin/articles');
        }
    }
    
    public function edit($article_id) {
        // Edit article function
        if(Auth::canAccess(R_USER_ARTICLES_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет изменять статьи.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        
        // Get article by ID
        $template_data['article_data'] = Doctrine::getTable('articles')->findOneBy('id', $article_id);
        
        // If article not found then redirect to admin articles
        if($template_data['article_data']== NULL){
            redirect(base_url() . 'admin/articles');
        }
        
        
        $template_data['title'] = 'Панель администрирования - Редактирование статьи';
        $template_data['page_head'] = 'Редактирование статьи';
        $template_data['menuSelected'] = 'Статьи';
        $template_data['subSelected'] = 'Редактирование статьи';
        
        $template_data['articles_list'] = Doctrine::getTable('articles')->findAll();
        $template_data['articles_groups_list'] = Doctrine::getTable('articles_groups')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['roles_list'] = Doctrine::getTable('roles')->findAll();
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[512]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст статьи', 'trim|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка этой статьи', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        $this->form_validation->set_rules('menu_name', 'Название меню', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('menu_zindex', 'Позиция меню (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('menu_visible', '', 'xss_clean');
        // End form rules
        
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_articles_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_article = $template_data['article_data'];
            $new_article->name = $this->input->post('name');
            $new_article->url = $this->input->post('url');
            $new_article->meta_title = $this->input->post('meta_title');
            $new_article->meta_description = $this->input->post('meta_description');
            $new_article->meta_keywords = $this->input->post('meta_keywords');
            $new_article->description = $this->input->post('description');
            $new_article->content = $this->input->post('a_content');
            $new_article->parent_id = $this->input->post('parent_id');
            $new_article->group_id = $this->input->post('group_id');
            $new_article->user_id = $this->input->post('user_id');
            $new_article->gallery_id = $this->input->post('gallery_id');
            $new_article->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_article->zindex = $this->input->post('zindex');
            $new_article->access_rights_id = $this->input->post('access_rights_id');
            $new_article->menu_name = $this->input->post('menu_name');
            $new_article->menu_visible = str_replace('on', '1', $this->input->post('menu_visible'));
            $new_article->menu_zindex = $this->input->post('menu_zindex');
            $new_article->save();
            redirect(base_url() . 'admin/articles');
        }
    }
    
    public function hide($article_id){
        // Hide article
        if(Auth::canAccess(R_USER_ARTICLES_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет изменять статьи.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $article_data = Doctrine::getTable('articles')->findOneBy('id', $article_id);
        if($article_data != NULL){
            $article_data->visible = 0;
            $article_data->save();
        }
        
        redirect(base_url() . 'admin/articles');
    }
    
    public function show($article_id){
        // Show article
        if(Auth::canAccess(R_USER_ARTICLES_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет изменять статьи.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $article_data = Doctrine::getTable('articles')->findOneBy('id', $article_id);
        if($article_data != NULL){
            $article_data->visible = 1;
            $article_data->save();
        }
        
        redirect(base_url() . 'admin/articles');
    }
    
    public function delete($article_id){
        // Delete article
        if(Auth::canAccess(R_USER_ARTICLES_DELETE) === FALSE){
            exit('Уровень вашего доступа не позволяет удалить статьи.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $article_data = Doctrine::getTable('articles')->findOneBy('id', $article_id);
        if($article_data != NULL){
            $article_data->delete();
        }
        
        redirect(base_url() . 'admin/articles');
    }
    
    public function groups() {
        // Show groups list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с групами статей.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Группы статей';
        $template_data['page_head'] = 'Группы статей';
        $template_data['menuSelected'] = 'Статьи';
        $template_data['subSelected'] = 'Группы статей';
        
        // Get list of groups
        $articles_groups_list_q = Doctrine_Query::create()
            ->select('*')
            ->from('articles_groups')
            ->orderBy('name ASC');
            
        $template_data['articles_groups_list'] = $articles_groups_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_articles_groups.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }


    public function groups_add() {
        // Add article group function
        if(Auth::canAccess(R_USER_ARTICLES_GROUPS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет добавлять группу статей.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавление группы статьи';
        $template_data['page_head'] = 'Добавление группы статьи';
        $template_data['menuSelected'] = 'Статьи';
        $template_data['subSelected'] = 'Добавить группу';
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст статьи', 'trim|xss_clean');
        // End form rules
        
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_articles_groups_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_article_groups = new Articles_groups();
            $new_article_groups->name = $this->input->post('name');
            $new_article_groups->url = $this->input->post('url');
            $new_article_groups->meta_title = $this->input->post('meta_title');
            $new_article_groups->meta_description = $this->input->post('meta_description');
            $new_article_groups->meta_keywords = $this->input->post('meta_keywords');
            $new_article_groups->content = $this->input->post('a_content');
            $new_article_groups->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_article_groups->save();
            redirect(base_url() . 'admin/articles/groups');
        }
    }
    
    public function groups_edit($article_groups_id) {
        // Edit group
          if(Auth::canAccess(R_USER_ARTICLES_GROUPS_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет изменять группы.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        
        // Get article by ID
        $template_data['article_groups_data'] = Doctrine::getTable('articles_groups')->findOneBy('id', $article_groups_id);
        
        // If article not found then redirect to admin articles
        if($template_data['article_groups_data'] == NULL){
            redirect(base_url() . 'admin/articles/groups');
        }
        
        
        $template_data['title'] = 'Панель администрирования - Редактирование группы статей';
        $template_data['page_head'] = 'Редактирование группы статей';
        $template_data['menuSelected'] = 'Статьи';
        $template_data['subSelected'] = 'Группы статей';
        
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст статьи', 'trim|xss_clean');
        // End form rules
        
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_articles_groups_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_article_groups = $template_data['article_groups_data'];
            $new_article_groups->name = $this->input->post('name');
            $new_article_groups->url = $this->input->post('url');
            $new_article_groups->meta_title = $this->input->post('meta_title');
            $new_article_groups->meta_description = $this->input->post('meta_description');
            $new_article_groups->meta_keywords = $this->input->post('meta_keywords');
            $new_article_groups->content = $this->input->post('a_content');
            $new_article_groups->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_article_groups->save();
            redirect(base_url() . 'admin/articles/groups');
        }
  }

}

?>