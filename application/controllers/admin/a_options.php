<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class A_options extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin/options');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function index() {
        // Users actions log list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу со статьями.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function log() {
        // Users actions log list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу со статьями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Действия пользователей';
        $template_data['page_head'] = 'Действия пользователей';
        $template_data['menuSelected'] = 'Настройки';
        $template_data['subSelected'] = 'Последние действия';
        
        $template_data['actionsList'] = Doctrine_Query::create()
            ->select('*')
            ->from('actions_list')
            ->orderBy('created_at DESC')
            ->limit(50)
            ->execute();
            
        $template_data['content'] = $this->parser->parse('admin/content_log.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function log_del($logItemId){
        // Delete log item
        $logData =  Doctrine::getTable('actions_list')->findOneBy('id', $logItemId);
        if($logData != NULL){
            $logData->delete();
        }
        redirect(base_url() . 'admin/options/log');
    }
}
?>