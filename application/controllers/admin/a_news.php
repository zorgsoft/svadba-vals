<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class A_news extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin/adverts');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function index() {
        // Main function to show news list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с новостями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Список новостей';
        $template_data['page_head'] = 'Новости';
        $template_data['menuSelected'] = 'Начало';
        $template_data['subSelected'] = 'Новости';
        
        $news_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('news')
                ->orderBy('zindex ASC');
        $template_data['news_list'] = $news_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_news.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function add() {
        // Add new news
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с новостями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        
    }
}
?>