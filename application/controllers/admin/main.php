<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main  extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function index(){
        // Main admin, show at start
        $template_data = array();
        // Set title and page top header text       
        $template_data['title'] = 'Панель администрирования';
        $template_data['page_head'] = 'Панель администрирования';
        // Menu names
        $template_data['menuSelected'] = 'Начало';
        $template_data['subSelected'] = 'Добро пожаловать';
        
        $template_data['content'] = $this->parser->parse('admin/content_main.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
}
?>