<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class A_banners extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (Auth::isAuthorized() === FALSE) {
            $this->session->set_userdata('login_redir_url', base_url() . 'admin/adverts');
            redirect(base_url() . 'login');
        }

        if (Auth::canAccess(R_USER_ADMIN) === FALSE) {
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="' . base_url() . '">На главную.</a>');
        }
    }

    public function index() {
        // Main function banners list
        if (Auth::canAccess(R_USER_ADMIN) === FALSE) {
            exit('Уровень вашего доступа не позволяет работу с банерами.<br/><a href="' . base_url() . '">На главную.</a>');
        }

        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Список банеров';
        $template_data['page_head'] = 'Банеры';
        $template_data['menuSelected'] = 'Банеры';
        $template_data['subSelected'] = 'Банеры';

        $banners_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('banners')
                ->orderBy('zindex ASC');
        $template_data['banners_list'] = $banners_list_q->execute();

        $template_data['content'] = $this->parser->parse('admin/content_banners.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function add(){
        // Add banner
        if(Auth::canAccess(R_USER_BANNERS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с банерами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавлени банера';
        $template_data['page_head'] = 'Добавление банера';
        $template_data['menuSelected'] = 'Банеры';
        $template_data['subSelected'] = 'Добавить';
        
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('title', 'Заголовок', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'Краткая URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('site_url', 'Полная URL ссылка', 'trim|required|max_length[256]|xss_clean');
        //$this->form_validation->set_rules('paid_sum', 'Сумма', 'trim|integer|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_banners_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_banner = new Banners();
            $new_banner->name = $this->input->post('name');
            $new_banner->title = $this->input->post('title');
            $new_banner->url = $this->input->post('url');
            $new_banner->site_url = $this->input->post('site_url');
            $new_banner->user_id = $this->input->post('user_id');
            $new_banner->paid = str_replace('on', '1', $this->input->post('paid'));
            $format_date_time = strtotime($this->input->post('paid_to'));
            $new_banner->paid_to = date('Y-m-d', $format_date_time);
            $new_banner->paid_sum = $this->input->post('paid_sum');
            $new_banner->paid_type = $this->input->post('paid_type');
            $new_banner->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_banner->zindex = $this->input->post('zindex');
            
            // Banner file upload
            $config['upload_path'] = 'resources/banners/';
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['max_size']	= '10000';
	    $config['max_width']  = '1024';
	    $config['max_height']  = '768';
            $config['overwrite'] = FALSE;
            $config['remove_spaces'] = true;
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload', $config);
            $filed_name = "banner_file";

	    if ( ! $this->upload->do_upload($filed_name)){
                $new_banner->file_name = '';
            } else {
                $data = $this->upload->data();
                $new_banner->file_name = $data['file_name'];
            }
            // End file upload
            
            $new_banner->save();
            
            redirect(base_url() . 'admin/banners');
        }
    }
    
    public function edit($banner_id){
        // Banner edit
        if(Auth::canAccess(R_USER_BANNERS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с банерами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $banner_data = Doctrine::getTable('Banners')->findOneBy('id', $banner_id);
        if($banner_data == NULL){
            redirect(base_url() . 'admin/banners');
        }
        
        $template_data = array();
        $template_data['banner_data'] = $banner_data;
        $template_data['title'] = 'Панель администрирования - Редактирование банера';
        $template_data['page_head'] = 'Редактирование банера';
        $template_data['menuSelected'] = 'Банеры';
        $template_data['subSelected'] = 'Редактировать';
        
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('title', 'Заголовок', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'Краткая URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('site_url', 'Полная URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_banners_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $banner_data->name = $this->input->post('name');
            $banner_data->title = $this->input->post('title');
            $banner_data->url = $this->input->post('url');
            $banner_data->site_url = $this->input->post('site_url');
            $banner_data->user_id = $this->input->post('user_id');
            $banner_data->paid = str_replace('on', '1', $this->input->post('paid'));
            $format_date_time = strtotime($this->input->post('paid_to'));
            $banner_data->paid_to = date('Y-m-d', $format_date_time);
            $banner_data->paid_sum = $this->input->post('paid_sum');
            $banner_data->paid_type = $this->input->post('paid_type');
            $banner_data->visible = str_replace('on', '1', $this->input->post('visible'));
            $banner_data->zindex = $this->input->post('zindex');
            $banner_data->save();
            redirect(base_url() . 'admin/banners');
        }
    }
    
    public function image_del(){
        // Ajax delete image from banner
        if(Auth::canAccess(R_USER_BANNERS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с банерами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $banner_id = $this->input->post('banner_id');
        
        $banner_data = Doctrine::getTable('Banners')->findOneBy('id', $banner_id);
        
        if($banner_data != NULL AND file_exists('resources/banners/' . $banner_data->file_name)){
            unlink('resources/banners/' . $banner_data->file_name);
            $banner_data->file_name = '';
            $banner_data->save();
        }
    }
    
    public function image_add($banner_id){
        // Ajax add image to banner
        if(Auth::canAccess(R_USER_BANNERS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с банерами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $banner_data = Doctrine::getTable('Banners')->findOneBy('id', $banner_id);
        if($banner_data != NULL){
            if(trim($banner_data->file_name) != '' AND file_exists('resources/banners/' . $banner_data->file_name)){
                unlink('resources/banners/' . $banner_data->file_name);
            }
            
            // Banner file upload
            $config['upload_path'] = 'resources/banners/';
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['max_size']	= '10000';
	    $config['max_width']  = '1024';
	    $config['max_height']  = '768';
            $config['overwrite'] = FALSE;
            $config['remove_spaces'] = true;
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload', $config);
            $filed_name = "image_file";

	    if ( ! $this->upload->do_upload($filed_name)){
                $banner_data->file_name = '';
                $banner_data->save();
                echo '';
            } else {
                $data = $this->upload->data();
                $banner_data->file_name = $data['file_name'];
                $banner_data->save();
                echo '<ul class="media-grid">
		    <li>
			 <a>
			    <img class="thumbnail" src="'.base_url().'resources/banners/'.$banner_data->file_name.'">
			 </a>
		    </li>
		</ul>';
            }
            // End file upload

        }
    }
    
    public function delete($banner_id){
        // Delete banner
        if(Auth::canAccess(R_USER_BANNERS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с банерами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $banner_data = Doctrine::getTable('Banners')->findOneBy('id', $banner_id);
        
        if($banner_data != NULL){
            if(trim($banner_data->file_name) !='' AND file_exists('resources/banners/' . $banner_data->file_name)){
                @unlink('resources/banners/' . $banner_data->file_name);
            }
            $banner_data->delete();
        }
        redirect(base_url() . 'admin/banners');
    }

}

?>