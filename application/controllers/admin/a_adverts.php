<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class A_adverts extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin/adverts');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function index() {
        // Main function adverts list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с объявлениями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Список объявлений';
        $template_data['page_head'] = 'Объявления';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Объявления';
        
        $adverts_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('adverts')
                ->orderBy('zindex ASC');
        $template_data['adverts_list'] = $adverts_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_adverts.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function cat($adverts_cat_url = NULL) {
        // Main adverts catalog list function
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Список разделов объявлений';
        $template_data['page_head'] = 'Объявления';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Разделы';
        
        $adverts_cat_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('adverts_groups')
                ->orderBy('zindex ASC');
        
        $template_data['adverts_cat_parent'] =  NULL;
        
        if($adverts_cat_url != NULL){
            $adverts_cat_data = Doctrine::getTable('adverts_groups')->findOneBy('url', $adverts_cat_url);
            if($adverts_cat_data != NULL){
                $template_data['adverts_cat_parent'] = $adverts_cat_data;
                $adverts_cat_list_q->where('parent_id =? ', $adverts_cat_data->id);
            } else {
                $adverts_cat_list_q->where('parent_id =? ', 0);
            }
        } else {
            $adverts_cat_list_q->where('parent_id =? ', 0);
        }
        
        $template_data['adverts_cat_list'] = $adverts_cat_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_adverts_cat.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function cat_add() {
        // Add adverts catalog
        if(Auth::canAccess(R_USER_ADVERTS_GROUPS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавлени раздела объявлений';
        $template_data['page_head'] = 'Добавление раздела объявлений';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Разделы';
        
        $template_data['adverts_cat_list'] = Doctrine::getTable('adverts_groups')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_adverts_cat_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_adverts_cat = new Adverts_groups();
            $new_adverts_cat->name = $this->input->post('name');
            $new_adverts_cat->url = $this->input->post('url');
            $new_adverts_cat->meta_title = $this->input->post('meta_title');
            $new_adverts_cat->meta_description = $this->input->post('meta_description');
            $new_adverts_cat->meta_keywords = $this->input->post('meta_keywords');
            $new_adverts_cat->content = $this->input->post('a_content');
            $new_adverts_cat->parent_id = $this->input->post('parent_id');
            $new_adverts_cat->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_adverts_cat->can_add = str_replace('on', '1', $this->input->post('can_add'));
            $new_adverts_cat->zindex = $this->input->post('zindex');
            $new_adverts_cat->save();
            redirect(base_url() . 'admin/adverts/cat');
        }
    }
    
    public function cat_edit($cat_id = NULL) {
        // Edit adverts catalog
        if(Auth::canAccess(R_USER_ADVERTS_GROUPS_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Редактирование раздела объявлений';
        $template_data['page_head'] = 'Редактирование раздела объявлений';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Разделы';
        
        $template_data['adverts_cat_list'] = Doctrine::getTable('adverts_groups')->findAll();
        
        // Get adverts catalog
        $template_data['adverts_cat'] = Doctrine::getTable('adverts_groups')->findOneBy('id', $cat_id);
        if($template_data['adverts_cat'] == NULL){
            redirect(base_url() . 'admin/adverts/cat');
        }
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_adverts_cat_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $template_data['adverts_cat']->name = $this->input->post('name');
            $template_data['adverts_cat']->url = $this->input->post('url');
            $template_data['adverts_cat']->meta_title = $this->input->post('meta_title');
            $template_data['adverts_cat']->meta_description = $this->input->post('meta_description');
            $template_data['adverts_cat']->meta_keywords = $this->input->post('meta_keywords');
            $template_data['adverts_cat']->content = $this->input->post('a_content');
            $template_data['adverts_cat']->parent_id = $this->input->post('parent_id');
            $template_data['adverts_cat']->visible = str_replace('on', '1', $this->input->post('visible'));
            $template_data['adverts_cat']->can_add = str_replace('on', '1', $this->input->post('can_add'));
            $template_data['adverts_cat']->zindex = $this->input->post('zindex');
            $template_data['adverts_cat']->save();
            redirect(base_url() . 'admin/adverts/cat');
        }
    }
    
    public function cat_delete($cat_id = NULL) {
        // Delete adverts catalog
        if(Auth::canAccess(R_USER_ADVERTS_GROUPS_DELETE) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $cat_data = Doctrine::getTable('adverts_groups')->findOneBy('id', $cat_id);
        if($cat_data != NULL)
            $cat_data->delete ();
        
        redirect(base_url() . 'admin/adverts/cat');
    }
    
    public function types($adverts_cat_url = NULL) {
        // Main adverts types list function
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с типами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Список типов объявлений';
        $template_data['page_head'] = 'Объявления';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Типы';
        
        $adverts_types_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('adverts_types')
                ->orderBy('zindex ASC');
        $template_data['adverts_types_list'] = $adverts_types_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_adverts_types.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }

    public function types_add() {
        // Add adverts types
        if(Auth::canAccess(R_USER_ADVERTS_TYPES_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с типами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавлени типа объявлений';
        $template_data['page_head'] = 'Добавление типа объявлений';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Типы';
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_adverts_types_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_adverts_types = new Adverts_types();
            $new_adverts_types->name = $this->input->post('name');
            $new_adverts_types->url = $this->input->post('url');
            $new_adverts_types->meta_title = $this->input->post('meta_title');
            $new_adverts_types->meta_description = $this->input->post('meta_description');
            $new_adverts_types->meta_keywords = $this->input->post('meta_keywords');
            $new_adverts_types->content = $this->input->post('a_content');
            $new_adverts_types->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_adverts_types->can_add = str_replace('on', '1', $this->input->post('can_add'));
            $new_adverts_types->zindex = $this->input->post('zindex');
            $new_adverts_types->save();
            redirect(base_url() . 'admin/adverts/types');
        }
    }
    
    public function types_edit($cat_id = NULL) {
        // Edit adverts types
        if(Auth::canAccess(R_USER_ADVERTS_TYPES_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с типами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Редактирование типа объявлений';
        $template_data['page_head'] = 'Редактирование типа объявлений';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Типы';
        
        
        // Get adverts types
        $template_data['adverts_types'] = Doctrine::getTable('adverts_types')->findOneBy('id', $cat_id);
        if($template_data['adverts_types'] == NULL){
            redirect(base_url() . 'admin/adverts/types');
        }
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_adverts_types_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $template_data['adverts_types']->name = $this->input->post('name');
            $template_data['adverts_types']->url = $this->input->post('url');
            $template_data['adverts_types']->meta_title = $this->input->post('meta_title');
            $template_data['adverts_types']->meta_description = $this->input->post('meta_description');
            $template_data['adverts_types']->meta_keywords = $this->input->post('meta_keywords');
            $template_data['adverts_types']->content = $this->input->post('a_content');
            $template_data['adverts_types']->visible = str_replace('on', '1', $this->input->post('visible'));
            $template_data['adverts_types']->can_add = str_replace('on', '1', $this->input->post('can_add'));
            $template_data['adverts_types']->zindex = $this->input->post('zindex');
            $template_data['adverts_types']->save();
            redirect(base_url() . 'admin/adverts/types');
        }
    }
    
    public function types_delete($type_id = NULL) {
        // Delete adverts catalog
        if(Auth::canAccess(R_USER_ADVERTS_TYPES_DELETE) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с типами объявлений.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $types_data = Doctrine::getTable('adverts_types')->findOneBy('id', $type_id);
        if($types_data != NULL)
            $types_data->delete ();
        
        redirect(base_url() . 'admin/adverts/types');
    }
    
    public function add() {
        // Add new adverts
        if(Auth::canAccess(R_USER_ADVERTS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с объявлениями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавление объявления';
        $template_data['page_head'] = 'Добавление объявления';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Объявления';
        
        $template_data['adverts_groups_list'] = Doctrine::getTable('adverts_groups')->findAll();
        $template_data['adverts_types_list'] = Doctrine::getTable('adverts_types')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        $template_data['cities_list'] = Doctrine::getTable('cities')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('author_name', 'Имя автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_email', 'E-mail автора', 'trim|valid_email|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_phone', 'Телефон автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_icq', 'ICQ автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_city', 'Город автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[254]|xss_clean');
        $this->form_validation->set_rules('site_url', 'Ссылка на сайт', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_adverts_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_adverts = new Adverts();
            $new_adverts->name = $this->input->post('name');
            $new_adverts->meta_title = $this->input->post('meta_title');
            $new_adverts->meta_description = $this->input->post('meta_description');
            $new_adverts->meta_keywords = $this->input->post('meta_keywords');
            $new_adverts->content = $this->input->post('a_content');
            $new_adverts->url = $this->input->post('url');
            $new_adverts->site_url = $this->input->post('site_url');
            $new_adverts->group_id = $this->input->post('group_id');
            $new_adverts->type_id = $this->input->post('type_id');
            $new_adverts->gallery_id = $this->input->post('gallery_id');
            $new_adverts->user_id = $this->input->post('user_id');
            $new_adverts->city_id = $this->input->post('city_id');
            $new_adverts->author_name = $this->input->post('author_name');
            $new_adverts->author_email = $this->input->post('author_email');
            $new_adverts->author_phone = $this->input->post('author_phone');
            $new_adverts->author_icq = $this->input->post('author_icq');
            $new_adverts->author_city = $this->input->post('author_city');
            $new_adverts->price = $this->input->post('price');
            $new_adverts->paid = str_replace('on', '1', $this->input->post('paid'));
            $format_date_time = strtotime($this->input->post('paid_to'));
            $new_firm->paid_to = date('Y-m-d', $format_date_time);
            $new_adverts->paid_sum = $this->input->post('paid_sum');
            $new_adverts->paid_type = $this->input->post('paid_type');
            $new_adverts->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_adverts->can_edit = str_replace('on', '1', $this->input->post('can_edit'));
            $new_adverts->zindex = $this->input->post('zindex');
            $new_adverts->save();
            redirect(base_url() . 'admin/adverts');
        }
    }
    
    public function edit($cat_id = NULL) {
        // Edit adverts
        if(Auth::canAccess(R_USER_ADVERTS_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с объявлениями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Редактирование объявления';
        $template_data['page_head'] = 'Редактирование объявления';
        $template_data['menuSelected'] = 'Объявления';
        $template_data['subSelected'] = 'Объявления';
        
        $template_data['adverts_groups_list'] = Doctrine::getTable('adverts_groups')->findAll();
        $template_data['adverts_types_list'] = Doctrine::getTable('adverts_types')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        $template_data['cities_list'] = Doctrine::getTable('cities')->findAll();
        
        // Get adverts data
        $template_data['adverts_data'] = Doctrine::getTable('adverts')->findOneBy('id', $cat_id);
        if($template_data['adverts_data'] == NULL){
            redirect(base_url() . 'admin/adverts');
        }
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('author_name', 'Имя автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_email', 'E-mail автора', 'trim|valid_email|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_phone', 'Телефон автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_icq', 'ICQ автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('author_city', 'Город автора', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[254]|xss_clean');
        $this->form_validation->set_rules('site_url', 'Ссылка на сайт', 'trim|max_length[254]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_adverts_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $template_data['adverts_data']->name = $this->input->post('name');
            $template_data['adverts_data']->meta_title = $this->input->post('meta_title');
            $template_data['adverts_data']->meta_description = $this->input->post('meta_description');
            $template_data['adverts_data']->meta_keywords = $this->input->post('meta_keywords');
            $template_data['adverts_data']->content = $this->input->post('a_content');
            $template_data['adverts_data']->url = $this->input->post('url');
            $template_data['adverts_data']->site_url = $this->input->post('site_url');
            $template_data['adverts_data']->group_id = $this->input->post('group_id');
            $template_data['adverts_data']->type_id = $this->input->post('type_id');
            $template_data['adverts_data']->gallery_id = $this->input->post('gallery_id');
            $template_data['adverts_data']->user_id = $this->input->post('user_id');
            $template_data['adverts_data']->city_id = $this->input->post('city_id');
            $template_data['adverts_data']->author_name = $this->input->post('author_name');
            $template_data['adverts_data']->author_email = $this->input->post('author_email');
            $template_data['adverts_data']->author_phone = $this->input->post('author_phone');
            $template_data['adverts_data']->author_icq = $this->input->post('author_icq');
            $template_data['adverts_data']->author_city = $this->input->post('author_city');
            $template_data['adverts_data']->price = $this->input->post('price');
            $template_data['adverts_data']->paid = str_replace('on', '1', $this->input->post('paid'));
            $format_date_time = strtotime($this->input->post('paid_to'));
            $template_data['adverts_data']->paid_to = date('Y-m-d', $format_date_time);
            $template_data['adverts_data']->paid_sum = $this->input->post('paid_sum');
            $template_data['adverts_data']->paid_type = $this->input->post('paid_type');
            $template_data['adverts_data']->visible = str_replace('on', '1', $this->input->post('visible'));
            $template_data['adverts_data']->can_edit = str_replace('on', '1', $this->input->post('can_edit'));
            $template_data['adverts_data']->zindex = $this->input->post('zindex');
            $template_data['adverts_data']->save();
            redirect(base_url() . 'admin/adverts');
        }
    }
    
    public function delete($advert_id = NULL) {
        // Delete adverts catalog
        if(Auth::canAccess(R_USER_ADVERTS_DELETE) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с объявлениями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $adverts_data = Doctrine::getTable('adverts')->findOneBy('id', $advert_id);
        if($adverts_data != NULL)
            $adverts_data->delete ();
        
        redirect(base_url() . 'admin/adverts');
    }
}
?>