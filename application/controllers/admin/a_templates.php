<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class A_templates extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin/templates');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }

    public function index() {
        // Main templates function for show templates list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с шаблонами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Шаблоны';
        $template_data['page_head'] = 'Настройки';
        $template_data['menuSelected'] = 'Настройки';
        $template_data['subSelected'] = 'Шаблоны';
        
        // Get list of templates
        $template_data['templates_list'] = Doctrine::getTable('template_pages')->findAll();
        
        $template_data['content'] = $this->parser->parse('admin/content_templates.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
}
?>