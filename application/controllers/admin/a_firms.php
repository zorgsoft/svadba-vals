<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class A_firms extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin/firms');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function index() {
        // Main firms list function
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Список фирм';
        $template_data['page_head'] = 'Фирмы';
        $template_data['menuSelected'] = 'Фирмы';
        $template_data['subSelected'] = 'Фирмы';
        
        $firms_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('firms')
                ->orderBy('zindex ASC');
        $template_data['firms_list'] = $firms_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_firms.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function cat($firm_url = NULL) {
        // Main firms catalog list function
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами фирм.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Список разделов фирм';
        $template_data['page_head'] = 'Фирмы';
        $template_data['menuSelected'] = 'Фирмы';
        $template_data['subSelected'] = 'Разделы';
        
        $firms_cat_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('firms_cat')
                ->orderBy('zindex ASC');
        
        $template_data['firms_cat_parent'] =  NULL;
        
        if($firm_url != NULL){
            $firm_data = Doctrine::getTable('firms_cat')->findOneBy('url', $firm_url);
            if($firm_data != NULL){
                $template_data['firms_cat_parent'] = $firm_data;
                $firms_cat_list_q->where('parent_id =? ', $firm_data->id);
            } else {
                $firms_cat_list_q->where('parent_id =? ', 0);
            }
        } else {
            $firms_cat_list_q->where('parent_id =? ', 0);
        }
        
        $template_data['firms_cat_list'] = $firms_cat_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_firms_cat.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function cat_add() {
        // Add firm catalog
        if(Auth::canAccess(R_USER_FIRMS_GROUPS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами фирм.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавлени раздела фирм';
        $template_data['page_head'] = 'Добавление раздела фирм';
        $template_data['menuSelected'] = 'Фирмы';
        $template_data['subSelected'] = 'Разделы';
        
        $template_data['firms_cat_list'] = Doctrine::getTable('firms_cat')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['banners_list'] = Doctrine::getTable('banners')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        $this->form_validation->set_rules('menu_name', 'Название меню', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('menu_zindex', 'Позиция меню (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('menu_visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_firms_cat_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_firm_cat = new Firms_cat();
            $new_firm_cat->name = $this->input->post('name');
            $new_firm_cat->url = $this->input->post('url');
            $new_firm_cat->meta_title = $this->input->post('meta_title');
            $new_firm_cat->meta_description = $this->input->post('meta_description');
            $new_firm_cat->meta_keywords = $this->input->post('meta_keywords');
            $new_firm_cat->description = $this->input->post('description');
            $new_firm_cat->content = $this->input->post('a_content');
            $new_firm_cat->parent_id = $this->input->post('parent_id');
            $new_firm_cat->gallery_id = $this->input->post('gallery_id');
            $new_firm_cat->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_firm_cat->zindex = $this->input->post('zindex');
            $new_firm_cat->banner_id = $this->input->post('banner_id');
            $new_firm_cat->menu_name = $this->input->post('menu_name');
            $new_firm_cat->menu_visible = str_replace('on', '1', $this->input->post('menu_visible'));
            $new_firm_cat->menu_zindex = $this->input->post('menu_zindex');
            $new_firm_cat->save();
            redirect(base_url() . 'admin/firms/cat');
        }
    }
    
    public function cat_edit($cat_id = NULL) {
        // Edit firm catalog
        if(Auth::canAccess(R_USER_FIRMS_GROUPS_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами фирм.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Редактирование раздела фирм';
        $template_data['page_head'] = 'Редактирование раздела фирм';
        $template_data['menuSelected'] = 'Фирмы';
        $template_data['subSelected'] = 'Разделы';
        
        $template_data['firms_cat_list'] = Doctrine::getTable('firms_cat')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['banners_list'] = Doctrine::getTable('banners')->findAll();
        
        // Get firm catalog
        $template_data['firms_cat'] = Doctrine::getTable('firms_cat')->findOneBy('id', $cat_id);
        if($template_data['firms_cat'] == NULL){
            redirect(base_url() . 'admin/firms/cat');
        }
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        $this->form_validation->set_rules('menu_name', 'Название меню', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('menu_zindex', 'Позиция меню (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('menu_visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_firms_cat_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $template_data['firms_cat']->name = $this->input->post('name');
            $template_data['firms_cat']->url = $this->input->post('url');
            $template_data['firms_cat']->meta_title = $this->input->post('meta_title');
            $template_data['firms_cat']->meta_description = $this->input->post('meta_description');
            $template_data['firms_cat']->meta_keywords = $this->input->post('meta_keywords');
            $template_data['firms_cat']->description = $this->input->post('description');
            $template_data['firms_cat']->content = $this->input->post('a_content');
            $template_data['firms_cat']->parent_id = $this->input->post('parent_id');
            $template_data['firms_cat']->gallery_id = $this->input->post('gallery_id');
            $template_data['firms_cat']->visible = str_replace('on', '1', $this->input->post('visible'));
            $template_data['firms_cat']->zindex = $this->input->post('zindex');
            $template_data['firms_cat']->banner_id = $this->input->post('banner_id');
            $template_data['firms_cat']->menu_name = $this->input->post('menu_name');
            $template_data['firms_cat']->menu_visible = str_replace('on', '1', $this->input->post('menu_visible'));
            $template_data['firms_cat']->menu_zindex = $this->input->post('menu_zindex');
            $template_data['firms_cat']->save();
            redirect(base_url() . 'admin/firms/cat');
        }
    }
    
    public function cat_delete($cat_id = NULL) {
        // Delete firm catalog
        if(Auth::canAccess(R_USER_FIRMS_GROUPS_DELETE) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с разделами фирм.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $cat_data = Doctrine::getTable('firms_cat')->findOneBy('id', $cat_id);
        if($cat_data != NULL)
            $cat_data->delete ();
        
        redirect(base_url() . 'admin/firms/cat');
    }
    
    public function add() {
        // Add new firm
        if(Auth::canAccess(R_USER_FIRMS_ADD) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавление фирмы';
        $template_data['page_head'] = 'Добавление фирмы';
        $template_data['menuSelected'] = 'Фирмы';
        $template_data['subSelected'] = 'Фирмы';
        
        $template_data['firms_cat_list'] = Doctrine::getTable('firms_cat')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        $template_data['cities_list'] = Doctrine::getTable('cities')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('tags', 'Тэги (метки)', 'trim|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[1024]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|xss_clean');
        $this->form_validation->set_rules('contacts', 'Контакты', 'trim|max_length[1024]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        $this->form_validation->set_rules('menu_name', 'Название меню', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('menu_zindex', 'Позиция меню (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('menu_visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_firms_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $new_firm = new Firms();
            $new_firm->name = $this->input->post('name');
            $new_firm->url = $this->input->post('url');
            $new_firm->meta_title = $this->input->post('meta_title');
            $new_firm->meta_description = $this->input->post('meta_description');
            $new_firm->meta_keywords = $this->input->post('meta_keywords');
            $new_firm->description = $this->input->post('description');
            $new_firm->tags = $this->input->post('tags');
            $new_firm->content = $this->input->post('a_content');
            $new_firm->contacts = $this->input->post('contacts');
            //$new_firm->cat_id = $this->input->post('cat_id');
            $new_firm->gallery_id = $this->input->post('gallery_id');
            $new_firm->visible = str_replace('on', '1', $this->input->post('visible'));
            $new_firm->zindex = $this->input->post('zindex');
            $new_firm->menu_name = $this->input->post('menu_name');
            $new_firm->menu_visible = str_replace('on', '1', $this->input->post('menu_visible'));
            $new_firm->paid = str_replace('on', '1', $this->input->post('paid'));
            $new_firm->paid_sum = $this->input->post('paid_sum');
            $format_date_time = strtotime($this->input->post('paid_to'));
            $new_firm->paid_to = date('Y-m-d', $format_date_time);
            $new_firm->user_id = $this->input->post('user_id');
            $new_firm->menu_zindex = $this->input->post('menu_zindex');
            $new_firm->save();
            
            $new_firms_adds = new Firms_adds();
            $new_firms_adds->cities_id = $this->input->post('adds_cities_id');
            $new_firms_adds->firm_id = $new_firm->id;
            $new_firms_adds->emails = $this->input->post('adds_emails');
            $new_firms_adds->url = $this->input->post('adds_url');
            $new_firms_adds->phones = $this->input->post('adds_phones');
            $new_firms_adds->address = $this->input->post('adds_address');
            $new_firms_adds->can_edit = 1;
            $new_firms_adds->save();
            
            redirect(base_url() . 'admin/firms/edit/'.$new_firm->id);
        }        
    }
    
    public function edit($firm_id = NULL) {
        // Edit firm
        if(Auth::canAccess(R_USER_FIRMS_EDIT) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Редактирование фирмы';
        $template_data['page_head'] = 'Редактирование фирмы';
        $template_data['menuSelected'] = 'Фирмы';
        $template_data['subSelected'] = 'Фирмы';
        
        $template_data['firms_cat_list'] = Doctrine::getTable('firms_cat')->findAll();
        $template_data['cities_list'] = Doctrine::getTable('cities')->findAll();
        $template_data['gallerys_list'] = Doctrine::getTable('gallery')->findAll();
        $template_data['users_list'] = Doctrine::getTable('users')->findAll();
        
        $template_data['firm_data'] = Doctrine::getTable('firms')->findOneBy('id', $firm_id);
        if($template_data['firm_data'] == NULL){
            redirect(base_url() . 'admin/firms');
        }
        
        // Start form rules
        $this->form_validation->set_rules('name', 'Название', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('tags', 'Тэги (метки)', 'trim|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|max_length[1024]|xss_clean');
        $this->form_validation->set_rules('a_content', 'Текст', 'trim|xss_clean');
        $this->form_validation->set_rules('contacts', 'Контакты', 'trim|max_length[1024]|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');
        $this->form_validation->set_rules('menu_name', 'Название меню', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('menu_zindex', 'Позиция меню (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('menu_visible', '', 'xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_firms_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $template_data['firm_data']->name = $this->input->post('name');
            $template_data['firm_data']->url = $this->input->post('url');
            $template_data['firm_data']->meta_title = $this->input->post('meta_title');
            $template_data['firm_data']->meta_description = $this->input->post('meta_description');
            $template_data['firm_data']->meta_keywords = $this->input->post('meta_keywords');
            $template_data['firm_data']->tags = $this->input->post('tags');
            $template_data['firm_data']->description = $this->input->post('description');
            $template_data['firm_data']->content = $this->input->post('a_content');
            $template_data['firm_data']->contacts = $this->input->post('contacts');
            //$template_data['firm_data']->cat_id = $this->input->post('cat_id');
            $template_data['firm_data']->gallery_id = $this->input->post('gallery_id');
            $template_data['firm_data']->visible = str_replace('on', '1', $this->input->post('visible'));
            $template_data['firm_data']->zindex = $this->input->post('zindex');
            $template_data['firm_data']->menu_name = $this->input->post('menu_name');
            $template_data['firm_data']->menu_visible = str_replace('on', '1', $this->input->post('menu_visible'));
            $template_data['firm_data']->paid = str_replace('on', '1', $this->input->post('paid'));
            $template_data['firm_data']->paid_sum = $this->input->post('paid_sum');
            $format_date_time = strtotime($this->input->post('paid_to'));
            $template_data['firm_data']->paid_to = date('Y-m-d', $format_date_time);
            $template_data['firm_data']->user_id = $this->input->post('user_id');
            $template_data['firm_data']->menu_zindex = $this->input->post('menu_zindex');
            $template_data['firm_data']->save();
            redirect(base_url() . 'admin/firms');
        }
    }
    
    public function ajax_update_firms_adds_list() {
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        $firms_id = $this->input->post('firms_id');
        $firms_data = Doctrine::getTable('firms')->findOneBy('id', $firms_id);
        if($firms_data != NULL and $firms_data->Addresses->count() > 0){
            $template_data = array();
            $template_data['cities_list'] = Doctrine::getTable('cities')->findAll();
            $template_data['adds_list'] = $firms_data->Addresses;
            $this->parser->parse('admin/tpl_ajax_firms_adds_list.php', $template_data);
        }
    }
    
    public function ajax_firms_adds_save( ) {
        // Save address for firm
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $adds_id = $this->input->post('adds_id');
        $add_data['adds_cities_id'] = $this->input->post('adds_cities_id');
        $add_data['adds_emails'] = $this->input->post('adds_emails');
        $add_data['adds_url'] = $this->input->post('adds_url');
        $add_data['adds_phones'] = $this->input->post('adds_phones');
        $add_data['adds_address'] = $this->input->post('adds_address');
        $add_data['adds_can_edit'] = $this->input->post('adds_can_edit');
        
        $adds_data = Doctrine::getTable('firms_adds')->findOneBy('id', $adds_id);
        if($adds_data != NULL){
            $adds_data->cities_id = $add_data['adds_cities_id'];
            $adds_data->emails = $add_data['adds_emails'];
            $adds_data->url = $add_data['adds_url'];
            $adds_data->phones = $add_data['adds_phones'];
            $adds_data->address = $add_data['adds_address'];
            $adds_data->can_edit = $add_data['adds_can_edit'];
            $adds_data->save();
            echo 'Данные сохранены.';
        } else {
            echo 'Ошибка при сохранении.';
        }
    }
    
    public function ajax_firms_adds_del() {
        // Delete address from firm
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        $adds_id = $this->input->post('adds_id');
        $adds_data = Doctrine::getTable('firms_adds')->findOneBy('id', $adds_id);
        if($adds_data != NULL){
            $adds_data->delete();
        }
    }
    
    public function ajax_firms_adds_add() {
        // Add new address to firm
        $firms_id = $this->input->post('firms_id');
        $firm_data = Doctrine::getTable('firms')->findOneBy('id', $firms_id);
        if($firm_data != NULL){
            $new_adds = new Firms_adds();
            $new_adds->cities_id = 0;
            $new_adds->firm_id = $firm_data->id;
            $new_adds->emails = 'Введите email адреса разделяемые запятой';
            $new_adds->url = base_url();
            $new_adds->phones = 'Введите телефоны разделяемые запятой';
            $new_adds->address = 'Введите полный адрес фирмы';
            $new_adds->can_edit = 1;
            $new_adds->save();
            echo 'OK';
        } else {
            echo 'Firm ' . $firms_id . ' not found';
        }
    }
    
    public function delete($firm_id = NULL) {
        // Delete firm
        if(Auth::canAccess(R_USER_FIRMS_DELETE) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $firm_data = Doctrine::getTable('firms')->findOneBy('id', $firm_id);
        if($firm_data != NULL){
            if(trim($firm_data->img_logo) !='' AND file_exists('resources/firmlogos/' . $firm_data->img_logo)){
                @unlink('resources/firmlogos/' . $firm_data->img_logo);
            }
            $firm_data->delete ();
        }
        
        redirect(base_url() . 'admin/firms');
    }
    
    
    public function ajax_add_cat() {
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        } else {
            $firms_id = $this->input->post('firms_id');
            $firms_cat_id = $this->input->post('firms_cat_id');
            
            $firms_data = Doctrine::getTable('firms')->findOneBy('id', $firms_id);
            $firms_cat_data = Doctrine::getTable('firms_cat')->findOneBy('id', $firms_cat_id);
            if($firms_data != NULL and $firms_cat_data != NULL){
                $new_firm_to_cat_link = new Firms_to_cat();
                $new_firm_to_cat_link->firm_id = $firms_id;
                $new_firm_to_cat_link->firms_cat_id = $firms_cat_id;
                $new_firm_to_cat_link->save();
            }
        }
    }
    
    public function ajax_update_firms_to_cat_list(){
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        } else {
            $firms_id = $this->input->post('firms_id');
            
            $firms_to_cat_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('firms_to_cat')
                ->where('firm_id = ?', $firms_id)
                ->orderBy('id ASC');
            $firms_to_cat_list = $firms_to_cat_list_q->execute();
            if($firms_to_cat_list->count() > 0){
                foreach($firms_to_cat_list as $firms_to_cat_item){
                    echo '<option value="'.$firms_to_cat_item->id.'">'.$firms_to_cat_item->Firms_cat->name.'</option>';
                }
            } else {
                echo '';
            }
        }
    }
    
    public function ajax_del_cat() {
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        } else {
            $firms_cat_id = $this->input->post('firms_cat_id');
            $firms_to_cat_item = Doctrine::getTable('firms_to_cat')->findOneBy('id', $firms_cat_id);
            if($firms_to_cat_item != NULL) {
                $firms_to_cat_item->delete();
            }
        }
    }
    
    public function ajax_update_firms_to_city_list() {
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        } else {
            $firms_id = $this->input->post('firms_id');
            $firms_to_city_list_q = Doctrine_Query::create()
                ->select('*')
                ->from('cities_firms')
                ->where('firm_id = ?', $firms_id)
                ->orderBy('id ASC');
            $firms_to_city_list = $firms_to_city_list_q->execute();
            if($firms_to_city_list->count() > 0){
                foreach($firms_to_city_list as $firms_to_city_item){
                    echo '<option value="'.$firms_to_city_item->id.'">'.$firms_to_city_item->City->name.'</option>';
                }
            } else {
                echo '';
            }
        }
    }
    
    public function ajax_add_city_cat() {
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        } else {
            $firms_id = $this->input->post('firms_id');
            $firms_cat_id = $this->input->post('firms_cat_id');
            
            $firms_data = Doctrine::getTable('firms')->findOneBy('id', $firms_id);
            $firms_cat_data = Doctrine::getTable('cities')->findOneBy('id', $firms_cat_id);
            if($firms_data != NULL and $firms_cat_data != NULL){
                $new_firm_to_cat_link = new Cities_firms();
                $new_firm_to_cat_link->firm_id = $firms_id;
                $new_firm_to_cat_link->cities_id = $firms_cat_id;
                $new_firm_to_cat_link->save();
            }
        }
    }
    
    public function ajax_del_city() {
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        } else {
            $firms_cat_id = $this->input->post('firms_cat_id');
            $firms_to_cat_item = Doctrine::getTable('cities_firms')->findOneBy('id', $firms_cat_id);
            if($firms_to_cat_item != NULL) {
                $firms_to_cat_item->delete();
            }
        }
    }
    
    public function ajaxImageUpload() {
        // Upload image
        $firms_id = $this->input->post('firms_id');
        $config['upload_path'] = 'images/gallery/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1024';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = true;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        
        $filed_name = "image_file";
        
        if (!$this->upload->do_upload($filed_name)) {
            echo $this->upload->display_errors('Ошибка: ' + $config['upload_path'] + ' - ', '!');
        } else {
            $data = $this->upload->data();
            $firm_data = Doctrine::getTable('firms')->finOneBy('id', $firms_id);
            if($firm_data != NULL){
                $firm_data->img_logo = $data['file_name'];
                $firm_data->save();
                echo 'OK';
            } else {
                $photo_path = 'images/gallery/';
                $photo_file = $photo_path . $data['file_name'];
                
                @unlink($photo_file);
                echo 'Ошибка при сохранении в базу, файл изображения будет удалён!';
            }
        }        
    }
    
    
    public function image_add($firmId){
        // Ajax add image to firm
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $firmData = Doctrine::getTable('firms')->findOneBy('id', $firmId);
        if($firmData != NULL){
            if(trim($firmData->img_logo) != '' AND file_exists('resources/firmlogos/' . $firmData->img_logo)){
                unlink('resources/firmlogos/' . $firmData->img_logo);
            }
            
            // Logo file upload
            $config['upload_path'] = 'resources/firmlogos/';
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['max_size']	= '1024';
	    $config['max_width']  = '1024';
	    $config['max_height']  = '768';
            $config['overwrite'] = FALSE;
            $config['remove_spaces'] = true;
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload', $config);
            $filed_name = "image_file";

	    if ( ! $this->upload->do_upload($filed_name)){
                $firmData->img_logo = '';
                $firmData->save();
                echo '';
            } else {
                $data = $this->upload->data();
                $firmData->img_logo = $data['file_name'];
                $firmData->save();
                echo '<ul class="media-grid">
		    <li>
			 <a>
			    <img class="thumbnail" src="'.base_url().'resources/firmlogos/'.$firmData->img_logo.'">
			 </a>
		    </li>
		</ul>';
            }
            // End file upload

        }
    }
    
    public function image_del(){
        // Ajax delete image logo from firm
        if(Auth::canAccess(R_USER_FIRMS) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с фирмами.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $banner_id = $this->input->post('banner_id');
        
        $banner_data = Doctrine::getTable('firms')->findOneBy('id', $banner_id);
        
        if($banner_data != NULL AND file_exists('resources/firmlogos/' . $banner_data->img_logo)){
            unlink('resources/firmlogos/' . $banner_data->img_logo);
            $banner_data->img_logo = '';
            $banner_data->save();
        }
    }
    
}
?>
