<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class A_users extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(Auth::isAuthorized() === FALSE){
            $this->session->set_userdata('login_redir_url', base_url().'admin/articles');
            redirect(base_url().'login');
        }
        
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="'.base_url().'">На главную.</a>');
        }
    }
    
    public function index(){
        // Show users list
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с пользователями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Пользователи';
        $template_data['page_head'] = 'Пользователи';
        $template_data['menuSelected'] = 'Пользователи';
        $template_data['subSelected'] = 'Список пользователей';
        
        $users_list_q = Doctrine_Query::create()
            ->select('*')
            ->from('users')
            ->orderBy('login ASC, roles_id ASC');
        $template_data['users_list'] = $users_list_q->execute();
        
        $template_data['content'] = $this->parser->parse('admin/content_users.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }
    
    public function add() {
        // Add new user
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с пользователями.<br/><a href="'.base_url().'">На главную.</a>');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Добавление пользователя';
        $template_data['page_head'] = 'Добавление пользователя';
        $template_data['menuSelected'] = 'Пользователи';
        $template_data['subSelected'] = 'Добавить пользователя';
        
        $template_data['roles_list'] = Doctrine::getTable('roles')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('login', 'Логин', 'trim|required|max_length[128]|xss_clean|callback_username_check');
        $this->form_validation->set_rules('password', 'Пароль', 'trim|required|max_length[128]|xss_clean');
        $this->form_validation->set_rules('name', 'Фамилия И.О.', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|max_length[128]|xss_clean');
        $this->form_validation->set_rules('phone', 'Телефон', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('comment', 'Комментарий', 'trim|xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_users_add.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            $newUser = new Users();
            $newUser->login = $this->input->post('login');
            $newUser->password = $this->input->post('password');
            $newUser->name = $this->input->post('name');
            $newUser->email = $this->input->post('email');
            $newUser->phone = $this->input->post('phone');
            $newUser->comment = $this->input->post('comment');
            $newUser->roles_id = $this->input->post('roles_id');
            $newUser->save();
            
            redirect(base_url() . 'admin/users');
        }
    }
    
    public function edit($userId) {
        // Edit user
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с пользователями.<br/><a href="'.base_url().'">На главную.</a>');
        }

        $userData = Doctrine::getTable('users')->findOneBy('id', $userId);
        if($userData == NULL){
            redirect(base_url() . 'admin/users');
        }
        
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Редактирование пользователя';
        $template_data['page_head'] = 'Редактирование пользователя';
        $template_data['menuSelected'] = 'Пользователи';
        $template_data['subSelected'] = 'Редактирование пользователя';
        
        $template_data['userData'] = $userData;
        
        $template_data['roles_list'] = Doctrine::getTable('roles')->findAll();
        
        // Start form rules
        $this->form_validation->set_rules('password', 'Пароль', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('name', 'Фамилия И.О.', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|max_length[128]|xss_clean');
        $this->form_validation->set_rules('phone', 'Телефон', 'trim|max_length[128]|xss_clean');
        $this->form_validation->set_rules('comment', 'Комментарий', 'trim|xss_clean');
        // End form rules
        
        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_users_edit.php', $template_data, TRUE);
            $this->parser->parse('admin/template.php', $template_data);
        } else {
            
            $newPassword = trim($this->input->post('password'));
            
            if($newPassword != ''){
                $newPassword = $this->input->post('password');
                //$newPassword = md5($newPassword);
                $userData->password = $newPassword;
            }
            
            $userData->name = $this->input->post('name');
            $userData->email = $this->input->post('email');
            $userData->phone = $this->input->post('phone');
            $userData->comment = $this->input->post('comment');
            $userData->roles_id = $this->input->post('roles_id');
            $userData->save();
            
            redirect(base_url() . 'admin/users');
        }
    }
    
    public function delete($userId) {
        // Delete user
        if(Auth::canAccess(R_USER_ADMIN) === FALSE){
            exit('Уровень вашего доступа не позволяет работу с пользователями.<br/><a href="'.base_url().'">На главную.</a>');
        } else {
            if(Auth::getUserId() == trim($userId)){
                exit('Нельзя удалять самого себя.<br/><a href="'.base_url().'admin/users">Назад.</a>');
            } else {
                $userData = Doctrine::getTable('users')->findOneBy('id', $userId);
                if($userData != NULL){
                    $userData->delete();
                }
            }
            redirect(base_url() . 'admin/users');
        }
    }
    
    public function username_check($str) {
        $userData = Doctrine::getTable('users')->findOneBy('login', $str);
        if($userData != NULL){
            $this->form_validation->set_message('username_check', 'Пользователь с логином %s уже существует');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
?>