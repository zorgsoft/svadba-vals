<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class A_gallery extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (Auth::isAuthorized() === FALSE) {
            $this->session->set_userdata('login_redir_url', base_url() . 'admin/articles');
            redirect(base_url() . 'login');
        }

        if (Auth::canAccess(R_USER_ADMIN) === FALSE) {
            exit('Уровень вашего доступа не позволяет входить в административную панель.<br/><a href="' . base_url() . '">На главную.</a>');
        }
    }

    public function index() {
        // Main articles function for show articles list
        if (Auth::canAccess(R_USER_ADMIN) === FALSE) {
            exit('Уровень вашего доступа не позволяет работу со статьями.<br/><a href="' . base_url() . '">На главную.</a>');
        }
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Галлереи';
        $template_data['page_head'] = 'Галлереи';
        $template_data['menuSelected'] = 'Галлереи';
        $template_data['subSelected'] = 'Список галлерей';

        $gallery_table = Doctrine::getTable('Gallery');

        $gallery_table->setOption('orderBy', 'zindex ASC');
        $template_data['gallery_list'] = $gallery_table->findAll();


        $template_data['content'] = $this->parser->parse('admin/content_gallery_list.php', $template_data, TRUE);
        $this->parser->parse('admin/template.php', $template_data);
    }

    public function add() {
        // Main articles function for show articles list
        if (Auth::canAccess(R_USER_ADMIN) === FALSE) {
            exit('Уровень вашего доступа не позволяет работу со статьями.<br/><a href="' . base_url() . '">На главную.</a>');
        }
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Галлереи';
        $template_data['page_head'] = 'Галлереи';
        $template_data['menuSelected'] = 'Галлереи';
        $template_data['subSelected'] = 'Добавить галлерею';

        $this->form_validation->set_rules('name', 'Название галлереи', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|xss_clean');
        $this->form_validation->set_rules('content', 'Текст галлереи', 'trim|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка этой галлереи', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция блока (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');


        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_gallery_add', $template_data, true);
            $this->parser->parse('admin/template.php', $template_data);
        } else {

            $newG = libGallery::save($this->input);



            redirect(base_url() . 'admin/gallery/edit/' . $newG->id);
        }
    }

    public function edit($gallery_id) {
        // Main articles function for show articles list
        if (Auth::canAccess(R_USER_ADMIN) === FALSE) {
            exit('Уровень вашего доступа не позволяет работу со статьями.<br/><a href="' . base_url() . '">На главную.</a>');
        }
        $template_data = array();
        $template_data['title'] = 'Панель администрирования - Галлереи';
        $template_data['page_head'] = 'Галлереи';
        $template_data['menuSelected'] = 'Галлереи';
        $template_data['subSelected'] = 'Редактировать галлерею';

        $this->form_validation->set_rules('name', 'Название галлереи', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_title', 'Мета Title', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Мета Description', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('meta_keywords', 'Мета Keywords', 'trim|max_length[256]|xss_clean');
        $this->form_validation->set_rules('description', 'Краткое описание', 'trim|xss_clean');
        $this->form_validation->set_rules('content', 'Текст галлереи', 'trim|xss_clean');
        $this->form_validation->set_rules('url', 'URL ссылка этой галлереи', 'trim|required|max_length[256]|xss_clean');
        $this->form_validation->set_rules('zindex', 'Позиция блока (zindex)', 'trim|is_natural|xss_clean');
        $this->form_validation->set_rules('visible', '', 'xss_clean');

        $gallery_table = Doctrine::getTable('Gallery');

        $template_data['gallery_data'] = $gallery_table->findOneBy('id', $gallery_id);


        if ($this->form_validation->run() == FALSE) {
            $template_data['content'] = $this->parser->parse('admin/content_gallery_edit', $template_data, true);
            $this->parser->parse('admin/template.php', $template_data);
        } else {

            libGallery::save($this->input, $template_data['gallery_data']);



            redirect(base_url() . 'admin/gallery/');
        }
    }
    
    public function delete($gallery_id) {
        
        // Удаление галереи и всех её фотографий
        $gallery_table = Doctrine::getTable('Gallery');
        $gallery_data = $gallery_table->findOneBy('id', $gallery_id);
        if ($gallery_data != null) {
            if ($gallery_data->Photos != null) {
                foreach ($gallery_data->Photos as $photo_item) {
                    $this->photo_delete($photo_item->id, TRUE);
                    $photo_item->delete();
                }
            }
            $gallery_data->delete();
        }
        redirect(base_url() . 'admin/gallery/');
    }  

    public function photoUpload($gallery_id) {
/*        $this->load->library('image_lib');
        $config_pa = 'hardkor/resources/gallery/';
                
        $file1 = trim($this->input->post('filearray'));
        
        $f = fopen('n.txt','a+');
        $thumb_config['image_library'] = 'gd2';
        $thumb_config['source_image'] = $config_pa . $file1;
        $thumb_config['create_thumb'] = TRUE;
        $thumb_config['maintain_ratio'] = TRUE;
        $thumb_config['width'] = 150;
        $thumb_config['height'] = 120;
        $thumb_config['new_image'] = $config_pa . 'thumbs/';

        $this->load->library('image_lib', $thumb_config);
        $this->image_lib->resize();
        $er_t =  $this->image_lib->display_errors();
        fputs($f, $er_t . ' - [' . $file1.']/n');
        fclose(f);*/
        //$f = fopen('n.txt','w+');
/*        $thumb_config['image_library'] = 'gd2';
        $thumb_config['source_image'] = $config['upload_path']. $file1;
        $thumb_config['create_thumb'] = TRUE;
        $thumb_config['maintain_ratio'] = TRUE;
        $thumb_config['width'] = 150;
        $thumb_config['height'] = 120;
        //$thumb_config['new_image'] = $config['upload_path'] . 'thumbs/' ;
        
        $this->load->library('image_lib', $thumb_config);

        if ( ! $this->image_lib->resize()) {
            //$er_t =  $this->image_lib->display_errors();
            //fputs($f,'Ошибка: '.$er_t . ' - ' . $thumb_config['source_image']);
            } else {
            //    $er_t =  $this->image_lib->display_errors();
             //   fputs($f,'Ок: '.$er_t.' - '. $thumb_config['source_image']);
            }
*/        
         $file1 = trim($this->input->post('filearray'));
        
        $photos_table = Doctrine::getTable('Gallery_photos');
        $new_photo = new Gallery_photos();
        $new_photo->name = 'Новое фото';
        $new_photo->file_name = $file1;
		$tmp = explode('.', $file1);
        $new_photo->thumb_name = $tmp[0] . '_thumb.' . $tmp[1];
        $new_photo->visible = 1;
        $new_photo->gallery_id = $gallery_id;
        $total_photos = $photos_table->findBy('gallery_id', $gallery_id);
        $new_photo->zindex = (count($total_photos) + 1);
        $new_photo->save();
    
        
        
    }
    
    
    public function photoThumbsGen($gallery_id) {
        // Generate thumbs
        $images_list = Doctrine::getTable('gallery_photos')->findBy('gallery_id', $gallery_id);
        if (file_exists('resources/gallery/a86185034d.jpg')) echo 'y';
        $images_folder = 'resources/gallery/';
        $f = fopen('n.txt','a');
		$this->load->library('image_lib');
        if($images_list != NULL AND $images_list->count() > 0){
            foreach($images_list as $image_item){
                //$image_item->thumb_name
                if(file_exists($images_folder . $image_item->file_name)){
                    $thumb_config['image_library'] = 'gd2';
                    $thumb_config['source_image'] = $images_folder . $image_item->file_name;
                    $thumb_config['create_thumb'] = TRUE;
                    $thumb_config['maintain_ratio'] = TRUE;
                    $thumb_config['width'] = 150;
                    $thumb_config['height'] = 120;
                    $thumb_config['new_image'] = $images_folder . 'thumbs/' . $image_item->file_name;
                    echo 1;
                    $this->image_lib->initialize($thumb_config);
                    if (!$this->image_lib->resize()) $this->image_lib->display_errors();
					
                }
				else echo 'file not found' . $images_folder . $image_item->file_name . "\n";
            }
        }
    }
    
    
    
    
    
}

?>
