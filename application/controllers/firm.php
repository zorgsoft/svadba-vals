<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Firm extends CI_Controller {
    public function index($firmUrl) {
        // Show firm
        $firmData =  Doctrine::getTable('firms')->findOneBy('url', $firmUrl);
        if($firmData == NULL or $firmData->visible == 0){
            show_404(base_url().'firm/'.$firmUrl, FALSE);
        }
        
        $template_data = array();
        $template_data['meta_title'] = $firmData->meta_title;
        $template_data['meta_description'] = $firmData->meta_description;
        $template_data['meta_keywords'] = $firmData->meta_keywords;
        
        $template_data['content_firm'] = $firmData;
        $template_data['content_cat'] = $firmData->Firms_cats[0]->Firms_cat;
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('firm_right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('firm.php', $template_data);
    }
}
?>