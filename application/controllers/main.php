<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
    public function index() {
        $template_data = array();
        $template_data['meta_title'] = 'Свадьба Вальс';
        $template_data['meta_description'] = 'Описание сайта Свадьба Вальс';
        $template_data['meta_keywords'] = 'ключевые, слова, для, сатйта, свадьба, вальс';
        
        $template_data['content_article'] = Doctrine::getTable('articles')->findOneBy('id', 10);
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('main.php', $template_data);
    }
}
?>