<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adverts_show extends CI_Controller {
    public function index() {
        // Show main adverts catalogs list
        
        $template_data = array();
        $template_data['meta_title'] = 'Доска Объявлений';
        $template_data['meta_description'] = 'Свадьба Вальс - Доска Объявлений';
        $template_data['meta_keywords'] = 'Доска объявлений, объявления, продать, купить';
        
        $template_data['advertsCatTopList'] = Doctrine_Query::create()
            ->select('*')
            ->from('adverts_groups')
            ->where('visible =?', 1)
            ->andWhere('parent_id =?', 0)
            ->orderBy('zindex ASC, name ASC')
            ->execute();
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('adverts_cat.php', $template_data);
    }
    
    public function cat($catUrl) {
        // Show cat by url
        $catData = Doctrine::getTable('adverts_groups')->findOneBy('url', $catUrl);
        if($catData == NULL or $catData->visible == 0){
            show_404(base_url().'adverts/'.$catUrl, FALSE);
        }
        
        
        $template_data = array();
        $template_data['advertsCatData'] = $catData;
        $template_data['meta_title'] = 'Доска Объявлений: '. $catData->meta_title;
        $template_data['meta_description'] = $catData->meta_description;
        $template_data['meta_keywords'] = $catData->meta_keywords;
        
        $template_data['advertsCatList'] = Doctrine_Query::create()
            ->select('*')
            ->from('adverts')
            ->where('visible =?', 1)
            ->andWhere('group_id =?', $catData->id)
            ->orderBy('paid_type DESC, created_at DESC, zindex ASC, name ASC')
            ->execute();
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('adverts_cat_show.php', $template_data);
    }
    
    public function advert($catUrl, $advertUrl){
        // Show adverts
        $advertData = Doctrine::getTable('adverts')->findOneBy('url', $advertUrl);
        $catData = Doctrine::getTable('adverts_groups')->findOneBy('url', $catUrl);
        if($advertData != NULL and $catData != NULL and $advertData->group_id == $catData->id and $advertData->visible == 1 and $catData->visible==1){
            $template_data = array();
            $template_data['advertsCatData'] = $catData;
            $template_data['advertsData'] = $advertData;
            $template_data['meta_title'] = 'Объявление: '. $advertData->meta_title;
            $template_data['meta_description'] = $advertData->meta_description;
            $template_data['meta_keywords'] = $advertData->meta_keywords;
            
            $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
            $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
            $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
            $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
            $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
            $this->parser->parse('adverts_show.php', $template_data);
        } else {
            show_404(base_url().'adverts/'.$catUrl.'/'.$advertUrl, FALSE);
        }
    }
}
?>