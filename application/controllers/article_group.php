<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Article_group extends CI_Controller {
    public function index($groupUrl) {
        // Show articles group
        $groupData = Doctrine::getTable('articles_groups')->findOneBy('url', $groupUrl);
        if($groupData == NULL){
            show_404(base_url().'group/'.$groupUrl, FALSE);
        }
        
        $template_data = array();
        $template_data['meta_title'] = $groupData->meta_title;
        $template_data['meta_description'] = $groupData->meta_description;
        $template_data['meta_keywords'] = $groupData->meta_keywords;
        
        $template_data['content_group'] = $groupData;
        
        $template_data['tpl_header'] = $this->parser->parse('header.php', $template_data, TRUE);
        $template_data['tpl_banners'] = $this->parser->parse('banners_top.php', $template_data, TRUE);
        $template_data['tpl_left'] = $this->parser->parse('left.php', $template_data, TRUE);
        $template_data['tpl_right'] = $this->parser->parse('right.php', $template_data, TRUE);
        $template_data['tpl_futter'] = $this->parser->parse('futter.php', $template_data, TRUE);
        $this->parser->parse('group.php', $template_data);
    }
}
?>