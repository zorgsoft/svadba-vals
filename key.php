<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>jQuery Test</title>

<script src="http://192.168.0.105/hardkor/resources/sys/js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="http://192.168.0.105/hardkor/resources/sys/js/jquery-fieldselection.js" type="text/javascript"></script>

<style type="text/css">

body {
 background-color: #fff;
 margin: 10px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
 text-align:center;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px dashed #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

.text_a {
    width: 900px;
    margin: 0px auto;
    padding: 15px;
    border:1px dashed #ddd;
    height: 180px;
    min-width: 150px;
    min-height: 120px;
    background-color: #FAFAFA;
    font-family: serif;
    font-size: 22px;
}

.top_panel {
    width: 922px;
    margin-left: auto;
    margin-right: auto;
    margin-top: auto;
    margin-bottom: 5px;
    padding: 0px;
    border:1px dashed #ddd;
    height: 28px;
    min-width: 150px;
    background-color: #FAFAFA;
    font-family: serif;
    font-size: 18px;
    text-align: right;
    padding: 4px;
}

.keyboard {
    width: 922px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 15px;
    margin-bottom: auto;
    padding: 0px;
    border:1px dashed #ddd;
    height: auto;
    min-width: 150px;
    background-color: #FAFAFA;
    font-family: serif;
    font-size: 18px;
    text-align: center;
    padding: 4px;
}

.top_button {
    display: inline-block;
    width: 26px;
    height: 26px;
    font-family: cursive;
    font-size: 16px;
    text-decoration: none;
    color: #000000;
    background-color: #FFFFFF;
    border:1px dashed #ddd;
    text-align: center;
    margin-left: 0px;
    margin-right: 2px;
    padding: 0px;
}
</style>
</head>
<body>

<h1>jQuery Test!</h1>
<br/><br/>
<form>
    <div class="top_panel">
        <a href='#' onclick='javascript: fontPlus();' class='top_button'>A+</a>
        <a href='#' onclick='javascript: fontMinus();' class='top_button'>A-</a>
    </div>
    <textarea name='text' id='text' class='text_a'></textarea>
    <div class="keyboard">
        <a href='#' onclick="javascript: insertAtCaret('text', '`');" class='top_button'>`</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '1');" class='top_button'>1</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '2');" class='top_button'>2</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '3');" class='top_button'>3</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '4');" class='top_button'>4</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '5');" class='top_button'>5</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '6');" class='top_button'>6</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '7');" class='top_button'>7</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '8');" class='top_button'>8</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '9');" class='top_button'>9</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '0');" class='top_button'>0</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '-');" class='top_button'>-</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '=');" class='top_button'>=</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '\');" class='top_button'>\</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '<-');" class='top_button'><-</a>
        <br/>
        <a href='#' onclick="javascript: insertAtCaret('text', 'q');" class='top_button'>q</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'w');" class='top_button'>w</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'e');" class='top_button'>e</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'r');" class='top_button'>r</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 't');" class='top_button'>t</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'y');" class='top_button'>y</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'u');" class='top_button'>u</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'i');" class='top_button'>i</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'o');" class='top_button'>o</a>
        <a href='#' onclick="javascript: insertAtCaret('text', 'p');" class='top_button'>p</a>
        <a href='#' onclick="javascript: insertAtCaret('text', '[');" class='top_button'>[</a>
        <a href='#' onclick="javascript: insertAtCaret('text', ']');" class='top_button'>]</a>
    </div>
</form>


<script type='text/javascript'>
    var fontSize = 22;
    function fontPlus(){
        if(fontSize < 47){
            fontSize = fontSize + 2;
        }
        $('#text').css('font-size', fontSize + 'px')
    }
    
    function fontMinus(){
        if(fontSize > 12){
            fontSize = fontSize - 2;
        }
        $('#text').css('font-size', fontSize + 'px')
    }


function insertAtCaret(areaId,text) { var txtarea = document.getElementById(areaId); var scrollPos = txtarea.scrollTop; var strPos = 0; var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? "ff" : (document.selection ? "ie" : false ) ); if (br == "ie") { txtarea.focus(); var range = document.selection.createRange(); range.moveStart ('character', -txtarea.value.length); strPos = range.text.length; } else if (br == "ff") strPos = txtarea.selectionStart; var front = (txtarea.value).substring(0,strPos); var back = (txtarea.value).substring(strPos,txtarea.value.length); txtarea.value=front+text+back; strPos = strPos + text.length; if (br == "ie") { txtarea.focus(); var range = document.selection.createRange(); range.moveStart ('character', -txtarea.value.length); range.moveStart ('character', strPos); range.moveEnd ('character', 0); range.select(); } else if (br == "ff") { txtarea.selectionStart = strPos; txtarea.selectionEnd = strPos; txtarea.focus(); } txtarea.scrollTop = scrollPos; }
    
    function charPut(charToPut){
        insertAtCaret('text', charToPut);
    }
</script>

</body>
</html>