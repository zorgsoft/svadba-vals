<ul id="menu">
    <?php foreach($catList as $catListItem): ?>
    <?php if(isset($content_cat)){
        $parentCat = Data::getCatTopParent($content_cat->id);
        $contentCat = $content_cat;
    } else { $parentCat = NULL; $contentCat = NULL; }
    ?>
    <?php if(($parentCat != NULL and $parentCat->id == $catListItem->id) or (isset($content_cat) == TRUE and $content_cat->id == $catListItem->id)): ?>
    <li><a href="#" onclick="openMenu(this);return false"><?=$catListItem->name?></a>
    <?php else: ?>
    <li class="dop1"><a href="#" onclick="openMenu(this);return false"><?=$catListItem->name?></a>
    <?php endif; ?>
        <?php $subCats = Data::getSubListForCat($catListItem->id); ?>
        <?php if($subCats !=NULL): ?>
        <?php if(($parentCat != NULL and $parentCat->id == $catListItem->id) or (isset($content_cat) == TRUE and $content_cat->id == $catListItem->id)): ?>
        <ul id="dop" style="display: block;">
        <?php else: ?>
        <ul id="dop" style="display: none;">
        <?php endif; ?>
            <?php foreach($subCats as $sybCatItem): ?>
            <?php if($contentCat != NULL and $contentCat->id == $sybCatItem->id): ?>
            <li>[ <?=$sybCatItem->name?> ]</li>
            <?php else: ?>
            <li><a href="<?=base_url()?>cat/<?=$sybCatItem->url?>"><?=$sybCatItem->name?></a></li>
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>